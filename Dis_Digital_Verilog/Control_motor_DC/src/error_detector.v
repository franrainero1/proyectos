
module error_detector 
    #(
        parameter NB_COUNTER   = 10
    )
    (
        output [1:0]              o_updw_pwm     ,
        input  [NB_COUNTER -1: 0] i_updw_counter ,
        input  [NB_COUNTER -1: 0] i_ref_counter
    );

    /* Variables */
    reg [1:0] updw;

    /* Comportamiento */
    always @(*) begin
        
        /* Posicion pasada en sentido horario */
        if(i_updw_counter > i_ref_counter)
            updw <= 2'b01;
        else
            if(i_updw_counter < i_ref_counter)
                updw <= 2'b10;
            else
                updw <= 2'b00;
    end

    /* Asignacion de puertos */
    assign o_updw_pwm = updw;
    
endmodule