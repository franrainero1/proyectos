
/*   TP1:
 *
 * - Realizar la descripción de divisores de frecuencia para el barrido de los displays a una frec.de
 *   1 kHz utilizando un CLK de 15 kHz. 
 * - Describir un contador decimal de 000 a 999.
 * - Incorporar el concepto descripción jerárquico utilizando instanciación de componentes.
 * - Con una descripción de máquina de estado realizar la decodificación de los flancos de las
 *   señales A y B del encoder y transformarlas en pulsos Up/down según el sentido de giro (ej.
 *   en sentido horario sea pulsos up y antihorario, pulsos down).
 */

`define N_SEGMENTS  7
`define N_DISPLAYS  4
`define NB_COUNTER  10

 module top
    (
        output [`N_SEGMENTS - 1:0] o_sseg_data      ,
        output [`N_DISPLAYS - 1:0] o_sseg_enable    ,
        output                     o_pwm            ,
        output                     o_pwm_n          ,
        input  [`NB_COUNTER - 1:0] i_ref_counter    ,
        input  [1:0]               i_enc            ,
        input                      i_rst            ,
        input                      clock    
    );

    /* Parametros constantes */
    localparam MOD_div_clk_displays = 14 ;
    localparam NB_COUNTER           = `NB_COUNTER;

    localparam NB_PWM     = 12   ;    // Cantidad del bits del PWM (24.4kHz)
    localparam COM_MAX    = 3891 ;    // Cuenta correspondiente al 95% 
    localparam COM_MIN    = 204  ;    // Cuenta correspondiente al 05%

    /* Variables */
    wire [1 :0]                  connect_decoder_to_counter_dir      ;
    wire                         connect_decoder_to_counter_val      ;
    wire [NB_COUNTER -1     : 0] connect_counter_to_bin2bcd_decoder  ;
    wire [NB_COUNTER -1     : 0] connect_counter_to_error_detector   ;
    wire [4 * `N_DISPLAYS -1: 0] connect_bin2bcd_to_bcd2sseg_decoder ;
    wire [1 :0]                  connect_error_detector_to_pwm_generator; 

    wire                        clock_displays ;
    wire                        clock_pwm      ;
    
    wire [`N_SEGMENTS - 1 :0]   sseg_data      ;
    wire [`N_DISPLAYS - 1 :0]   sseg_enable    ;
    
    wire                        pwm   ;
    wire                        pwm_n ;


    /* Renombrado de cables */
    assign connect_counter_to_error_detector = connect_counter_to_bin2bcd_decoder;

    /* Decodificador de encoder */
    dec_encoder u_dec_encoder (
            .o_dir  (connect_decoder_to_counter_dir) ,
            .o_val  (connect_decoder_to_counter_val) ,
            .i_enc  (i_enc) ,
            .i_rst  (i_rst) ,
            .clock  (clock)
        );
    
    /* Contador Up-Down */
    updw_counter u_updw_counter (
            .o_count(connect_counter_to_bin2bcd_decoder ) ,
            .i_dir  (connect_decoder_to_counter_dir     ) ,
            .i_val  (connect_decoder_to_counter_val     ) ,
            .i_rst  (i_rst) ,
            .clock  (clock)
        );
    
    /* Decodificador Binario a BCD */
    bin2bcd u_bin2bcd (
            .o_bcd  (connect_bin2bcd_to_bcd2sseg_decoder ) ,
            .i_bin  (connect_counter_to_bin2bcd_decoder  ) ,
            .i_start(clock_displays      ),
            .i_rst  (i_rst) ,
            .clock  (clock)
        );
    
    /* Divisor de frecuencia para displays */
    div_frec #(
            .MOD(MOD_div_clk_displays)
        )
        u_div_frec
        (
            .clock_out(clock_displays ) ,
            .i_rst    (i_rst )          ,
            .clock    (clock )
        );
    
    /* Decodificador BCD a 4 displays de 7 segmentos */
    printer_sseg u_printer_sseg (
            .o_sseg_data   (sseg_data ),
            .o_sseg_enable (sseg_enable),
            .i_data_bcd    (connect_bin2bcd_to_bcd2sseg_decoder),
            .i_rst         (i_rst),
            .clock         (clock_displays )
        );
    
    /* Detector de error */
    error_detector #(
            .NB_COUNTER(NB_COUNTER)
        )
        u_error_detector
        (
            .o_updw_pwm     (connect_error_detector_to_pwm_generator),
            .i_updw_counter (connect_counter_to_error_detector),
            .i_ref_counter  (i_ref_counter )
        );

    /* Generador de PWM */
    pwm_generator #(
            .NB_PWM   (NB_PWM   ), 
            .COM_MAX  (COM_MAX  ),  
            .COM_MIN  (COM_MIN  )  
        )
        u_pwm_generator
        (
            .o_pwm      (pwm)       ,
            .o_pwm_n    (pwm_n)     ,
            .i_updw     (connect_error_detector_to_pwm_generator)          ,     
            .i_rst      (i_rst)     ,
            .clock      (clock)
        );

    /* Asignacion de puertos */
    assign o_sseg_data   = ~ sseg_data   ; // Diseñado para anodo comun
    assign o_sseg_enable =   sseg_enable ; 

    assign o_pwm   = pwm;
    assign o_pwm_n = pwm_n;

 endmodule