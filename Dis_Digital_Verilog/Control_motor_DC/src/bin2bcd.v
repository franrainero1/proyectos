
module bin2bcd (
        output [15 :0] o_bcd  ,
        input  [9  :0] i_bin   ,
        input         i_start ,
        input         i_rst   ,
        input         clock
    );
        
    /* Parametros constantes */
    localparam START  = 2'd0;
    localparam SHIFT  = 2'd1;
    localparam CK_END = 2'd2;
    localparam ADD    = 2'd3;

    /* Variables */
    reg [25:0] shift  ;
    reg [15:0] bcd    ;
    reg [3 :0] counter;
    reg [2 :0] state  ;

    /* Comportamiento */
    always @(posedge clock or negedge i_rst) begin
        if(!i_rst) begin
            state   <= START;
            counter <= 4'd0;
            shift   <= 26'd0;
            bcd     <= 16'd0;
        end
        else
            case (state)
                
                /* Inicio y Fin de la maquina */
                START: begin
                    counter <= 4'd0;
                    if(i_start) begin
                        shift <= {{16{1'b0}},i_bin};
                        state <= SHIFT;
                    end
                    else begin
                        state <= state;
                        bcd   <= shift[25:10];
                    end
                end

                /* Desplaza 1 bit */
                SHIFT: begin
                    shift   <= shift << 1;
                    counter <= counter + 1;
                    state   <= CK_END;
                end

                /* Chequea fin de la conversion */
                CK_END: begin
                    if (counter == 4'd10)  
                        state <= START;
                    else
                        state <= ADD;                    
                end

                ADD: begin
                    /* BCD 1 */
                    if      (shift[13:10] > 4'd4)
                        shift[13:10] <= shift[13:10] + 4'd3;
                    else 
                        shift[13:10] <= shift[13:10];
                    
                    /* BCD 2 */
                    if (shift[17:14] > 4'd4)
                        shift[17:14] <= shift[17:14] + 4'd3;
                    else
                        shift[17:14] <= shift[17:14];

                    /* BCD 3 */
                    if (shift[21:18] > 4'd4)
                        shift[21:18] <= shift[21:18] + 4'd3;
                    else
                        shift[21:18] <= shift[21:18];
                    
                    /* BCD 4 */
                    if (shift[25:22] > 4'd4)
                        shift[25:22] <= shift[25:22] + 4'd3;
                    else
                        shift[25:22] <= shift[25:22];

                    state <= SHIFT;
                end
                
                default:state <= START; 
            endcase

    end

    /* Asignacion de puertos */
    //assign o_bcd = shift[25:10];
    assign o_bcd = bcd;
    
endmodule