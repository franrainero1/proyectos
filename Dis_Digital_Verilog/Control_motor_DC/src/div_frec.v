module div_frec 
    #(
        parameter MOD = 18
    )
    (
        output clock_out,
        input  i_rst    ,
        input  clock
    );

    /* Variables */
    reg [MOD - 1 : 0] counter;

    /* Comportamiento */
    always @(posedge clock or negedge i_rst ) begin
        if(!i_rst)
            counter <= {MOD{1'b0}};
        else
            counter <= counter + 1;
    end

    /* Asignacionde puertos */
    assign clock_out = counter[MOD - 1];
    
endmodule