
module bcd2sseg (
        output [6:0] o_sseg ,
        input  [3:0] i_bcd  
    );

    /* Variables */
    reg [6:0] sseg;

    /* Comportamiento */
    always @(*) begin
        case (i_bcd)
            4'b0000 : sseg = 7'b0000001; // 0
            4'b0001 : sseg = 7'b1001111; // 1
            4'b0010 : sseg = 7'b0010010; // 2
            4'b0011 : sseg = 7'b0000110; // 3
            4'b0100 : sseg = 7'b1001100; // 4
            4'b0101 : sseg = 7'b0100100; // 5
            4'b0110 : sseg = 7'b0100000; // 6
            4'b0111 : sseg = 7'b0001110; // 7
            4'b1000 : sseg = 7'b0000000; // 8
            4'b1001 : sseg = 7'b0001100; // 9  
            default : sseg = 7'b1111111; // -
        endcase
    end

    /* Asignacion de puertos */
    assign o_sseg = sseg;
endmodule