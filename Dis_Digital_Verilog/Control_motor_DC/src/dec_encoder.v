
module dec_encoder
    (
        output [1:0] o_dir ,
        output       o_val ,
        input  [1:0] i_enc ,
        input        i_rst ,
        input        clock
    );

    /* Parametros constantes */
    localparam E0 = 2'b00;
    localparam E1 = 2'b01;
    localparam E2 = 2'b10;
    localparam E3 = 2'b11;

    /* Variables */
    reg [1:0] state;
    reg [1:0] new_state;
    reg       val;
    reg [1:0] dir;

    /* Comportamiento */
    always @(posedge clock) begin
        case (state)
            E0:
                case (i_enc)
                    2'b01:  begin
                                new_state <= E1 ;
                                dir <= 2'b10;
                            end 
                    2'b10:  begin
                                new_state <= E2 ;
                                dir <= 2'b01;
                            end 
                    default:begin
                                new_state <= state;
                                dir <= 2'b00;
                            end
                endcase
            E1:
                case (i_enc)
                    2'b00:  begin 
                                new_state <= E0 ;
                                dir <= 2'b01;
                            end
                    2'b11:  begin
                                new_state <= E3 ;
                                dir <= 2'b10;    
                            end
                    default:begin
                                new_state <= state;
                                dir <= 2'b00;
                            end
                endcase
            
            E2:
                case (i_enc)
                    2'b00:  begin 
                                new_state <= E0 ;
                                dir <= 2'b10;
                            end
                    2'b11:  begin 
                                new_state <= E3 ;
                                dir <= 2'b01;
                            end
                    default:begin 
                                new_state <= state;
                                dir <= 2'b00;
                            end
                endcase

            E3:
                case (i_enc)
                    2'b01:  begin
                                new_state <= E1 ;
                                dir <= 2'b01;
                            end
                    2'b10:  begin
                                new_state <= E2 ;
                                dir <= 2'b10;
                            end
                    default:begin
                                new_state <= state;
                                dir <= 2'b00;
                            end 
                endcase
        endcase

        if( new_state != state )
            val <= 1'b1;
        else
            val <= 1'b0;
    end

    always @(posedge clock or negedge i_rst) begin
        if(!i_rst) begin
            state     <= 2'b00; 
        end
        else
            state <= new_state;
    end

    /* Asignacion de puertos*/
    assign o_dir = dir;             
    assign o_val = val;                 // Eliminar esta linea, esta al re pedo

endmodule

