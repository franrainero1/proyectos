
module pwm_generator 
    #(
        parameter NB_PWM     = 18  ,    // Cantidad del bits del PWM
        parameter COM_MAX    = 249036,  // Cuenta correspondiente al 95% 
        parameter COM_MIN    = 13107    // Cuenta correspondiente al 05%
    )
    (
        output       o_pwm    ,
        output       o_pwm_n  ,
        input  [1:0] i_updw   ,     
        input        i_rst    ,
        input        clock
    );

    /* Parametros constantes */
    localparam INCREASE  = 2'b10;
    localparam DECREASE  = 2'b01;

    /* Variables */
    reg [NB_PWM -1 :0] counter;
    reg [NB_PWM -1 :0] top    ;
    reg                pwm    ;

    /* Comportamiento */
    always @(posedge clock or negedge i_rst) begin
        if(!i_rst) begin 
            counter <= {NB_PWM{1'b0}};
            pwm     <= 1'b1;

            /* Fija PWM al 50% */
            top     <= {NB_PWM{1'b0}} | (1'b1 << (NB_PWM-1));
            
        end
        else begin
            counter <= counter + 1'b1;

            if(counter < top)
                pwm <= 1'b1;
            else
                pwm <= 1'b0;

            if(counter == {NB_PWM{1'b1}}) begin
                case (i_updw)

                    INCREASE:
                        top <= COM_MAX;

                    DECREASE:
                        top <= COM_MIN;   
                                    
                    default : top <= {NB_PWM{1'b0}} | 1'b1 << (NB_PWM - 1);
                
                endcase
            end
            else
                top <= top;
        end

    end

    /* Asignacion de puertos */
    assign o_pwm   =  pwm;
    assign o_pwm_n = ~pwm;

endmodule