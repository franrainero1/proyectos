
module printer_sseg (
        output [6 :0] o_sseg_data   ,
        output [3 :0] o_sseg_enable ,
        input  [15:0] i_data_bcd    ,
        input         i_rst         ,
        input         clock
    );

    /* Parametros constantes */
    localparam DISPLAY_4 = 4'b0100;
    localparam DISPLAY_3 = 4'b0010;
    localparam DISPLAY_2 = 4'b0001;
    localparam DISPLAY_1 = 4'b1000;

    /* Variables */
    reg [3:0] cir_counter ;
    reg [3:0] data2write_bcd  ;

    /* Comportamiento */
    always @(posedge clock or negedge i_rst) begin
        if(!i_rst) begin
            cir_counter     <= 4'b0001;
            data2write_bcd  <= 4'b0000;
        end
        else begin
            
            case (cir_counter)
                DISPLAY_4 : data2write_bcd <= i_data_bcd [15:12]; 
                DISPLAY_3 : data2write_bcd <= i_data_bcd [11: 8];
                DISPLAY_2 : data2write_bcd <= i_data_bcd [7 : 4];
                DISPLAY_1 : data2write_bcd <= i_data_bcd [3 : 0];
                default   : data2write_bcd <= 4'b0000;
            endcase 

            cir_counter <= {cir_counter[2:0],cir_counter[3]};
        end
    end

    /* Instanciacion de conversor BCD a 7 Segmentos*/

    bcd2sseg u_bcd2sseg (
        .o_sseg(o_sseg_data   ) ,
        .i_bcd (data2write_bcd) 
    );

    /* Asignacion de puertos */
    assign o_sseg_enable = cir_counter; 

endmodule