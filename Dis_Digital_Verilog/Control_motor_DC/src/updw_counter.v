
module updw_counter 
    #(
        parameter NB_COUNTER   = 10      ,
        parameter COUNT_TOP    = 10'd999 ,
        parameter COUNT_BOTTOM = 10'd0  
    )
    (
        output [NB_COUNTER - 1 :0] o_count ,
        input  [1:0] i_dir   ,
        input        i_val   ,
        input        i_rst   ,
        input        clock
    );

    /* Parametros constantes */
    localparam MD  = 2'b01;         /* Motor hacia la derecha  */
    localparam MI  = 2'b10;         /* Motor hacia la izquierda */

    /* Variables */
    reg [NB_COUNTER - 1:0] counter;

    /* Comportamiento */
    always @(posedge clock or negedge i_rst) begin
        if(!i_rst)
            counter <= COUNT_BOTTOM;    
        else
            if(i_val)
                case (i_dir)
                    MD :
                        if (counter == COUNT_TOP) 
                            counter <= COUNT_BOTTOM ;
                        else
                            counter <= counter + 1 ;
                    MI : 
                        if (counter == COUNT_BOTTOM)
                            counter <= COUNT_BOTTOM ;
                        else
                            counter <= counter - 1 ;
                      
                    default: counter <= counter ; 
                endcase
            else
                counter <= counter;
    end

    /* Asignacion de puertos */
    assign o_count = counter;
    
endmodule