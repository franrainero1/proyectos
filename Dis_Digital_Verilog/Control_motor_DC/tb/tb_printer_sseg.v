`timescale 1ms/10ns

module tb_printer_sseg ();
    
    /* Entradas y salidas */
    wire [6 :0] o_sseg_data   ;
    wire [3 :0] o_sseg_enable ;
    reg  [15:0] i_data_bcd    ;
    reg         i_rst         ;
    reg         clock         ;

    /* Comportamiento */
    initial begin
        i_data_bcd = 16'd0;
        i_rst      =  1'b1;
        clock      =  1'b0;
    
        #1  i_rst  =  1'b0;
        #1  i_rst  =  1'b1;

        #5 i_data_bcd = 16'h0999;

        #5 i_data_bcd = 16'h0555;

        #5 i_data_bcd = 16'h0120;

        #5 $finish;

    end

    /* Clock de 2kHz */
    always begin
        #0.25 clock = ~clock;
    end

    /* Instancia del DUT */
    printer_sseg DUT(
        .o_sseg_data   (o_sseg_data   ),
        .o_sseg_enable (o_sseg_enable ),
        .i_data_bcd    (i_data_bcd    ),
        .i_rst         (i_rst         ),
        .clock         (clock         ) 
    );
    
endmodule