`timescale 10ns/100ps

module tb_top();

    /* Entradas y salidas */
    wire [6:0]  o_sseg_data ;
    wire [3:0]  o_sseg_enable;
    wire        o_pwm  ;
    wire        o_pwm_n;
    reg  [9 :0] i_ref_counter;
    reg  [1 :0] i_enc  ;
    reg         i_rst  ;
    reg         clock  ;

    /* Variables */
    integer i;
    wire [9 :0] tb_updw_counter;
    wire [15:0] tb_bcd_counter;

    /* Puntas de prueba */
    assign tb_updw_counter = tb_top.DUT.u_updw_counter.counter;
    assign tb_bcd_counter  = tb_top.DUT.u_bin2bcd.bcd;

    /* Generacion de estimulos */
    initial begin
        i_enc = 2'b00;
        i_rst = 1'b1 ;
        clock = 1'b0 ;
        i_ref_counter = 10'd500;
    
        #50  i_rst = 1'b0;
        #50  i_rst = 1'b1;

        /* Giro en sentido horario */
        for (i = 0; i < 125; i = i + 1) begin
            /* Encoder a 5kHz */
            #12500 i_enc = 2'b00;
            #12500 i_enc = 2'b10;
            #12500 i_enc = 2'b11;
            #12500 i_enc = 2'b01;
        end

        /* Encoder sin giro */
        for (i = 0; i < 125; i = i + 1) begin
            /* Encoder a 5kHz */
            #12500 i_enc = 2'b00;
        end

        #1 i_ref_counter = 10'd0;

        /* Giro en sentido anti-horario */
        for (i = 0; i < 125; i = i + 1) begin
            /* Encoder a 5kHz */
            #12500 i_enc = 2'b00;
            #12500 i_enc = 2'b01;
            #12500 i_enc = 2'b11;
            #12500 i_enc = 2'b10;
        end

        #12500 $finish;

    end

    always begin
        #0.5 clock = ~clock;      // Clock de 100MHz
    end

    /* Instanciacion del DUT */
    top DUT (
        .o_sseg_data   (o_sseg_data  ) ,
        .o_sseg_enable (o_sseg_enable) ,
        .o_pwm         (o_pwm         ),
        .o_pwm_n       (o_pwm_n       ),
        .i_ref_counter (i_ref_counter ),
        .i_enc         (i_enc  ) ,
        .i_rst         (i_rst  ) ,
        .clock         (clock  )   
    );


endmodule