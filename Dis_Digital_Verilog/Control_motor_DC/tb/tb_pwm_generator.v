`timescale 1ns/100ps

module tb_pwm_generator ();
    
    /* Entradas y salidas */
    wire       o_pwm     ;
    wire       o_pwm_n   ;
    reg  [1:0] i_updw    ;     
    reg        i_rst     ;
    reg        clock     ;

    /* Parametros constantes */
    localparam NB_PWM     = 18    ;
    localparam COM_MAX    = 249036; 
    localparam COM_MIN    = 13107 ;

    /* Variables */
    wire [NB_PWM -1 :0] tb_pwm_counter;
    wire [NB_PWM -1 :0] tb_pwm_top;


    /* Generacion de estimulos */
    initial begin   
        i_updw  = 2'b00  ;
        i_rst   = 1'b1   ;
        clock   = 1'b0   ;
    
        #2  i_rst = 1'b0 ;
        #2  i_rst = 1'b1 ;
            
        #10000000 i_updw = 2'b10;
        #10000000 i_updw = 2'b00;
        #10000000 i_updw = 2'b01;

        #10000000 $finish ;

    end

    always begin
        #5 clock = ~clock ;   // Clock de 100 kHz
    end

    /* Puntas de prueba */
    assign tb_pwm_counter = tb_pwm_generator.DUT.counter;
    assign tb_pwm_top     = tb_pwm_generator.DUT.top;

    /* Instanciacion del DUT */
    pwm_generator #(
        .NB_PWM  (NB_PWM)   ,  
        .COM_MAX (COM_MAX)  ,  
        .COM_MIN (COM_MIN)  
    )
    DUT
    (
        .o_pwm    (o_pwm)   ,  
        .o_pwm_n  (o_pwm_n) ,  
        .i_updw   (i_updw)  ,         
        .i_rst    (i_rst)   ,  
        .clock    (clock)         
    );

endmodule