`timescale 1ns/100ps

module tb_div_frec ();

    /* Entradas y salidas */
    wire clock_out;
    reg  i_rst    ;
    reg  clock    ;

    /* Parametros constantes */
    localparam MOD = 4;

    /* Generacion de estimulos */
    initial begin
        i_rst = 1'b1;
        clock = 1'b0;

        #50 i_rst = 1'b0;
        #50 i_rst = 1'b1;

        #1000 $finish;
    end

    always begin
        #5 clock = ~clock;
    end

    /* Instanciacion del DUT */
    div_frec #(
        .MOD(MOD)
    )
    DUT
    (
        .clock_out(clock_out),
        .i_rst(i_rst)        ,
        .clock(clock)
    );

endmodule