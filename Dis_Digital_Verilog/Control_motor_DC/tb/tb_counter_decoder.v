`timescale 1ns/100ps

module tb_counter_decoder ();
    
    /* Entradas y salidas */
    wire [9:0] o_count ;
    reg  [1:0] i_dir   ;
    reg        i_val   ;
    reg        i_rst   ;
    reg        clock   ;

    /* Parametros constantes */
    localparam MAX = 32'd5;
    localparam MP  = 2'b00;
    localparam MD  = 2'b01;
    localparam MI  = 2'b10;

    /* Variables */
    integer i;

    /* Generacion de estimulos */
    initial begin
        i_dir = 2'b00 ; 
        i_val = 1'b0  ;
        i_rst = 1'b0  ;
        clock = 1'b0  ;

        #10  i_rst = 1'b1 ;
        #10  i_rst = 1'b0 ;

        /* Cuenta ascendente - Giro Horario */
        #100 i_dir = MD ;
        for(i = 0; i <  MAX; i = i + 1) begin
            #100  i_val = 1'b1 ;
            #10   i_val = 0'b0 ;
        end

        /* Contador bloqueado - Motor parado */
        #10 i_dir = MP ;
        for(i = 0; i <  MAX - 4; i = i + 1) begin
            #100  i_val = 1'b1 ;
            #10   i_val = 0'b0 ;
        end

        /* Cuenta descendente - Giro Antihorario */
        #100 i_dir = MI ;
        for(i = 0; i <  MAX; i = i + 1) begin
            #100  i_val = 1'b1 ;
            #10   i_val = 0'b0 ;
        end

        #100 $finish;
    end

    always begin
        #5 clock = ~clock;
    end

    /* Instaciacion DUT */
    counter_decoder DUT (
        .o_count(o_count) ,
        .i_dir  (i_dir) ,
        .i_val  (i_val) ,
        .i_rst  (i_rst) ,
        .clock  (clock)
    );

endmodule