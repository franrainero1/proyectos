`timescale 1ns/100ps

module tb_dec_encoder();

    /* Entradas y salidas */
    wire [1:0] o_dir ;
    wire       o_val ;
    reg  [1:0] i_enc ;  // i_enc[1] = A & i_enc[0] = B
    reg        i_rst ;
    reg        clock ;

    /* Variables */
    integer i;
    wire [1:0] tb_state;

    assign tb_state = tb_dec_encoder.DUT.state;

    /* Generacion de estimulos */
    initial begin
        i_enc = 2'b00;
        i_rst = 1'b0 ;
        clock = 1'b0 ;

        #10  i_rst = 1'b1;
        #10  i_rst = 1'b0;
        #10  i_rst = 1'b1;

        /* Giro en sentido horario */
        for (i = 0; i < 50; i = i + 1) begin
            #50 i_enc = 2'b00;
            #50 i_enc = 2'b10;
            #50 i_enc = 2'b11;
            #50 i_enc = 2'b01;
        end

        //#50 i_enc = 2'b00;
        //#50 i_enc = 2'b11;

        /* Giro en sentido anti-horario */
        for (i = 0; i < 50; i = i + 1) begin
            #50 i_enc = 2'b00;
            #50 i_enc = 2'b01;
            #50 i_enc = 2'b11;
            #50 i_enc = 2'b10;
        end

        #50 $finish;

    end

    always begin
        #5 clock = ~clock;
    end

    /* Instanciacion del DUT */
    dec_encoder DUT (
        .o_dir (o_dir),
        .o_val (o_val), 
        .i_enc (i_enc),
        .i_rst (i_rst),
        .clock (clock)
    );


endmodule