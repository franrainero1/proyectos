`timescale 1ns/100ps

module tb_bin2bcd ();

    /* Entradas y salidas */
    wire [15 :0] o_bcd  ;
    reg  [9 :0]  i_bin  ;
    reg          i_start;
    reg          i_rst  ;
    reg          clock  ;
    
    /* Generacion de estimulos */
    initial begin
        i_bin   = 10'd243 ;
        i_start = 1'b0;
        i_rst   = 1'b0;
        clock   = 1'b0;

        #10 i_rst   = 1'b1;
        #10 i_rst   = 1'b0;
        #10 i_rst   = 1'b1;

        #10 i_start = 1'b1;
        #10 i_start = 1'b0;

        #300 i_bin   = 10'd520;

        #10 i_start = 1'b1;
        #10 i_start = 1'b0;

        #300 i_bin   = 10'd1023;
        
        #10 i_start = 1'b1;
        #10 i_start = 1'b0;

        #300 $finish;
    end

    always begin
        #5 clock = ~clock;
    end

    /* Instaciacion del DUT */
    bin2bcd DUT (
            .o_bcd   (o_bcd  ),
            .i_bin   (i_bin  ),
            .i_start (i_start),
            .i_rst   (i_rst  ),
            .clock   (clock  )
        );


endmodule