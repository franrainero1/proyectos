
module tb_error_detector ();
    
    /* Entradas y salidas */
    wire [1: 0] o_updw_pwm     ;
    reg  [9: 0] i_updw_counter ;
    reg  [9: 0] i_ref_counter  ;
    
    /* Generacion de estimulos */
    initial begin
        i_updw_counter = 10'd100   ;
        i_ref_counter  = 10'd512 ;

        #100 i_updw_counter = 10'd512;
        #100 i_updw_counter = 10'd600;
        #100 i_updw_counter = 10'd510;
        #100 i_updw_counter = 10'd514;
        #100 i_updw_counter = 10'd512;
        
        #100 $finish;
    end

    /* Instanciacion del DUT */
    error_detector u_error_detector (
        .o_updw_pwm     (o_updw_pwm    ),
        .i_updw_counter (i_updw_counter),
        .i_ref_counter  (i_ref_counter )
    );

endmodule