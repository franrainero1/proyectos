module my_top(
    output reg [3:0] o_led_r,   //Son tipo wire por defecto, son tipo reg si se aclara
    output reg [3:0] o_led_g,
    output reg [3:0] o_led_b,
    output reg [3:0] o_led,
    input [3:0] i_btn,
    input [3:0] i_sw,
    input i_reset,
    input clock
    );

wire enable;
wire [3:0] output_shift;
wire [3:0] output_flash;

reg  [3:0] output_mux;
reg  [3:0] btns_d;

reg mode;
reg red;
reg green;
reg blue;

my_count 
    u_count (
        .clock(clock),
        .i_reset(i_reset),
        .i_enable(i_sw[0]),
        .i_sel(i_sw[2:1]),
        .o_enable(enable)
    );

my_shiftreg 
    u_shitfreg (
        .clock(clock),
        .i_reset(i_reset),
        .i_enable(enable),
        .i_direction(i_sw[3]),
        .o_shift(output_shift)
    );

my_flash
    u_flash (
        .clock(clock),
        .i_reset(i_reset),
        .i_enable(enable),
        .o_flash(output_flash)
    );
  
always @(posedge clock) begin
    btns_d <= i_btn;           //Registro de i_btn
end

always @(posedge clock or negedge i_reset) begin

    if (~i_reset)
    begin
        mode <= 1'b0;
        red <= 1'b0;
        green <= 1'b0;
        blue <= 1'b0;
        o_led <= 4'b0;
    end
    else if(i_btn[0] && ~btns_d[0])  //Detector de flanco positivo
    begin    
        mode <= ~mode;
        o_led <= 4'b0001;
    end
    else if(i_btn[1] && ~btns_d[1])
    begin
        red <= ~red;
        o_led <= 4'b0010;
    end
    else if(i_btn[2] && ~btns_d[2])
    begin    
        green <= ~green;
        o_led <= 4'b0100;
    end
    else if(i_btn[3] && ~btns_d[3])
    begin
        blue <= ~blue;
        o_led <= 4'b1000;
    end
end

always @(*) begin

    if (mode == 0)
        output_mux = output_flash;
    else if (mode == 1)
        output_mux = output_shift;

    if (red == 0)
        o_led_r = 4'b0;
    else if (red == 1)
        o_led_r = output_mux;
        
    if (green == 0)
        o_led_g = 4'b0;
    else if (green == 1)
        o_led_g = output_mux;

    if (blue == 0)
        o_led_b = 4'b0;
    else if (blue == 1)
        o_led_b = output_mux;

end 

endmodule