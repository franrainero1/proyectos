`timescale 1ns/100ps

module tb_count();

  reg         clock;
  reg         i_reset;
  reg         i_enable;
  reg [1:0]   i_sel;
  wire        o_enable;

  wire [31:0] contador;
  wire [31:0] referencia;
  
  assign contador = u_count.counter;
  assign referencia = u_count.out_mux;  


 my_count
    u_count
    ( 
      .clock        (clock)     ,
      .i_reset      (i_reset)   ,
      .i_enable     (i_enable)  ,
      .i_sel        (i_sel) ,
      .o_enable     (o_enable)
    ) ;

  initial begin
    i_enable              = 1'b0    ;
    clock                 = 1'b0    ;
    i_reset               = 1'b1    ;
    i_sel[1:0]            = 2'b00   ;
    #10 i_reset           = 1'b0    ;
    #10 i_reset           = 1'b1    ; 
    #1 i_enable           = 1'b1    ; 
    #10000 i_sel[1:0]     = 2'b11   ;
    #20000 $finish                  ;

  end

  always #5 clock = ~clock ;
    
endmodule