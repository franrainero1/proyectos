`timescale 1ns/100ps

module tb_flash();

  reg         clock;
  reg         i_reset;
  reg         i_enable;
  wire [3:0]  o_flash;

  my_flash
    u_flash
    (
      .clock (clock),
      .i_reset (i_reset),
      .i_enable (i_enable),
      .o_flash (o_flash)
    ) ;

  initial begin
    i_enable      = 1'b1  ;
    clock         = 1'b0  ;
    i_reset       = 1'b1  ;
    #10 i_reset  = 1'b0   ;
    #10 i_reset  = 1'b1   ;
    #20000 $finish        ;
  end

  always #5   clock     = ~clock    ;
endmodule