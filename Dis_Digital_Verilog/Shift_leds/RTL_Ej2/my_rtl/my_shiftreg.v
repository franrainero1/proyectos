module my_shiftreg(
    input clock,
    input i_reset,
    input i_enable,
    input i_direction,
    output [3:0] o_shift
);

reg [3:0] salida;

always @(posedge clock or negedge i_reset) begin
    if(~i_reset)
    begin
        salida <= 4'b1000;
    end

    else
    begin
        if(i_enable)
        begin
            if(i_direction)
            begin
                case(o_shift)
                    4'b1000: salida <= 4'b0100;
                    4'b0100: salida <= 4'b0010;
                    4'b0010: salida <= 4'b0001;
                    4'b0001: salida <= 4'b1000;
                    default: salida <= 4'b1000;
                endcase
            end

            else
            begin
                case(o_shift)
                    4'b1000: salida <= 4'b0001;
                    4'b0100: salida <= 4'b1000;
                    4'b0010: salida <= 4'b0100;
                    4'b0001: salida <= 4'b0010;
                    default: salida <= 4'b1000;
                endcase
            end
        end
    end
end

assign o_shift = salida;

endmodule