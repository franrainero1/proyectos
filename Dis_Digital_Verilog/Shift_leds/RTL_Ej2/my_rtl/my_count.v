module my_count #(
    parameter R0 = 32'd100,
    parameter R1 = 32'd4000,
    parameter R2 = 32'd8000,
    parameter R3 = 32'd9000
)
(
    input clock,
    input i_reset,
    input i_enable,
    input [1:0] i_sel,
    output o_enable
);

reg salida;
reg [32-1:0] out_mux;
reg [32-1:0] counter;

always @(*) begin
    case(i_sel)
        2'b00 :  out_mux = R0;
        2'b01 :  out_mux = R1;
        2'b10 :  out_mux = R2;
        2'b11 :  out_mux = R3;
    endcase
end

always @(posedge clock or negedge i_reset) begin
    if(~i_reset)
        counter <= 32'b0;
    else
        if(counter == out_mux)
            counter <= 32'b0;
        else if(i_enable)
            counter <= counter + 1'b1;
        else
            counter <= counter;
end

always @(*) begin 
    if(counter == out_mux)
        salida = 1'b1;
    else
        salida = 1'b0;
end

assign o_enable = salida;

endmodule