module my_shift2leds(
    input clock,
    input i_reset,
    input i_enable,
    input i_direction,
    output [3:0] o_shift
);

reg [3:0] salida;

always @(posedge clock or negedge i_reset) begin
    if(~i_reset)
    begin
        salida <= 4'b0000;
    end

    else
    begin
        if(i_enable)
        begin
            if(i_direction)
            begin
                case(o_shift)
                    4'b0000: salida <= 4'b1001;
                    4'b1001: salida <= 4'b0110;
                    4'b0110: salida <= 4'b0000;
                    default: salida <= 4'b0000;
                endcase
            end

            else
            begin
                case(o_shift)
                    4'b0000: salida <= 4'b0110;
                    4'b0110: salida <= 4'b1001;
                    4'b1001: salida <= 4'b0000;
                    default: salida <= 4'b0000;
                endcase
            end
        end
    end
end

assign o_shift = salida;

endmodule