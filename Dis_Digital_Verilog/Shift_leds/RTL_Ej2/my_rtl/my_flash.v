module my_flash(
    input clock,
    input i_reset,
    input i_enable,
    output [3:0] o_flash
);

reg [3:0] salida;

always @(posedge clock or negedge i_reset) begin
    if(~i_reset)
        salida <= 4'b0;
    else
        if(i_enable)
            case(salida)
                4'b0: salida <= 4'b1111;
                4'b1111: salida <= 4'b0;
                default: salida <= 4'b0;
            endcase
end

assign o_flash = salida;

endmodule