`timescale 1ns/100ps

module tb_shif2leds();

  reg        clock;
  reg        i_reset;
  reg        i_enable;
  reg        i_direction;
  wire [3:0] o_shift;

  my_shift2leds
    u_shift2leds
    (
      .clock (clock),
      .i_reset (i_reset),
      .i_enable (i_enable),
      .i_direction (i_direction),
      .o_shift (o_shift)
    ) ;

  initial begin
    i_enable      = 1'b1;
    clock         = 1'b0;
    i_reset       = 1'b1;
    i_direction   = 1'b1;
    #10 i_reset  = 1'b0;
    #10 i_reset = 1'b1;
    #1000 i_direction = 1'b0;
    #20000 $finish      ;
  end

  always #5   clock     = ~clock    ;
    
endmodule