`timescale 1ns/100ps

module tb_top();

  wire [3:0] o_led_r;
  wire [3:0] o_led_g;
  wire [3:0] o_led_b;
  wire [3:0] o_led;
  reg [3:0]  i_btn;
  reg [3:0]  i_sw;
  reg        i_reset;
  reg        clock;

// ----------------------------------------

  wire count;
  wire [3:0] flash;
  wire [3:0] shift;
  wire [3:0] shift2leds;
  wire [3:0] ledbits;

  assign count    = u_top.enable;
  assign flash    = u_top.output_flash;
  assign shift    = u_top.output_shift;
  assign shift2leds  = u_top.output_shift2leds;
  assign ledbits  = u_top.output_mux;

  // ----------------------------------------

  my_top
    u_top
    ( .o_led_r      (o_led_r),
      .o_led_g      (o_led_g),
      .o_led_b      (o_led_b),
      .o_led        (o_led),
      .i_btn        (i_btn),
      .i_sw         (i_sw),
      .i_reset      (i_reset),
      .clock        (clock)
      
    ) ;

  initial begin
    i_btn               = 4'b0    ; //4 pulsadores en 0
    i_sw[0]             = 1'b1    ; //Activo contador
    clock               = 1'b0    ;
    i_reset             = 1'b1    ;
    i_sw[2:1]           = 2'b0    ; //Selecciono R0
    i_sw[3]             = 1'b1    ; //Direccion shif_reg

    #10 i_reset         = 1'b0    ; // Resetea
    #10 i_reset         = 1'b1    ;

    #50 i_btn[0]        = 1'b1    ; // Cambio de modo.
    #50 i_btn[0]        = 1'b0    ; 

    #10 i_btn[3:1]      = 3'b001  ; // Habilito r
    #10 i_btn[3:1]      = 3'b000  ;
    
    #10 i_btn[3:1]      = 3'b010  ; // Habilito g
    #10 i_btn[3:1]      = 3'b000  ;
    
    #10 i_btn[3:1]      = 3'b100  ; // Habilito b
    #10 i_btn[3:1]      = 3'b000  ;

    #1000 i_sw[3]       = 1'b0    ; // Cambia la direccion del shiftreg

    #3000 i_btn[0]     = 1'b1     ; // Cambio de modo.
    #10 i_btn[0]        = 1'b0    ;
    
    #3000 i_btn[0]     = 1'b1     ; // Cambio de modo.
    #10 i_btn[0]        = 1'b0    ;
        
    #100000 $finish               ;
  end

  always #5 clock = ~clock ;
    
endmodule