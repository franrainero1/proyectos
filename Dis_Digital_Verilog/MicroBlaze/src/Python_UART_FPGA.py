# TP6 Diseño Digital
# Francisco Gabriel Rainero. 
# franrainero1@gmail.com 

import time
import serial
import sys

#  Pasar tty por argumento al ejecutar python
if(len(sys.argv)>1):                                    #Verifica que se ejecute asignando el puerto
    puerto = sys.argv[1]                                #Asigna el argumento pasado 
else:
    print('Pasar el puerto como argumento "/dev/ttyUSBX"\n\r')
    sys.exit(0)                                         #Sale si no se pasa un argumento

ser = serial.Serial(
    port=puerto,	
    baudrate=115200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

ser.isOpen()
ser.timeout=None
print(ser.timeout)

print ('\r\nBienvenido\r\n')
print ('- Para prender/apagar un LED:\r\n N° de LED/LEDs [0(OFF) 1 2 3 4] + Letra de color [R-G-B-Y-C-M-W]:\r\n')
print ('- Para leer los switch:N° [55]\r\n')
print ('- Para salir: "exit"')

while 1 :
    inputData = input("<< ")
    if inputData == 'exit':
        ser.close()
        exit()
    elif(inputData == '55'):
        print ("Waiting Input Data")
        ser.write(inputData.encode())
        time.sleep(2)
        readData =ser.read(1)
        out = bin(int.from_bytes(readData,byteorder='big'))[2:].zfill(4)
        if out != '':
            print (">> " + out)
    else:
        print ("Sending Order")
        ser.write(inputData.encode())
        time.sleep(1)
