
#include <stdio.h>
#include <string.h>
#include "xparameters.h"
#include "xil_cache.h"
#include "xgpio.h"
#include "platform.h"
#include "xuartlite.h"
#include "microblaze_sleep.h"

#define PORT_IN	 		XPAR_AXI_GPIO_0_DEVICE_ID //XPAR_GPIO_0_DEVICE_ID
#define PORT_OUT 		XPAR_AXI_GPIO_0_DEVICE_ID //XPAR_GPIO_0_DEVICE_ID

//Device_ID Operaciones
#define def_SOFT_RST            0
#define def_ENABLE_MODULES      1
#define def_LOG_RUN             2
#define def_LOG_READ            3

XGpio GpioOutput;
XGpio GpioInput;
XUartLite uart_module;

int main()
{
	init_platform();
	int Status;

	unsigned char cabecera[2];

	//GPO_Value=0x00000000;
	//GPO_Param=0x00000000;

	/* Inicializacion de perifericos */

	XUartLite_Initialize(&uart_module, 0);

	Status=XGpio_Initialize(&GpioInput, PORT_IN);
	if(Status!=XST_SUCCESS){
        return XST_FAILURE;
    }
	Status=XGpio_Initialize(&GpioOutput, PORT_OUT);
	if(Status!=XST_SUCCESS){
		return XST_FAILURE;
	}
	XGpio_SetDataDirection(&GpioOutput, 1, 0x00000000); // GPIO de salida
	XGpio_SetDataDirection(&GpioInput, 1, 0xFFFFFFFF);  // GPIO de entrada

	u32 value;
  unsigned char datos;
  	  /* loop */
	while(1){
    read(stdin,&cabecera[0],2);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Funcionalidad
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    u32 led_mask;
    u32 colour;
    u32 colour_mask;
    u32 total_mask;

    if(cabecera[0]=='5' || cabecera[1]=='5'){
        XGpio_DiscreteWrite(&GpioOutput,1, (u32) 0x00000000);
        value = XGpio_DiscreteRead(&GpioInput, 1);
        datos=(char)(value&(0x0000000F));
        while(XUartLite_IsSending(&uart_module)){}
        XUartLite_Send(&uart_module, &(datos),1);
    }
    else{

        switch (cabecera[0])
        {
        case '0':
            led_mask = 0x0;
            break;
        case '1':
            led_mask = (0x0 << 9) + (0x0 << 6) + (0x0 << 3) + 0x7;  //Led 1
            break;
        case '2':
            led_mask = (0x0 << 9) + (0x0 << 6) + (0x7 << 3) + 0x0;  //Led 2
            break;
        case '3':
            led_mask = (0x0 << 9) + (0x7 << 6) + (0x0 << 3) + 0x0;  //Led 3
            break; 
        case '4':
            led_mask = (0x7 << 9) + (0x0 << 6) + (0x0 << 3) + 0x0;  //Led 4
            break; 
        default:
            led_mask = 0x0;
            break;
        }

        switch (cabecera[1])
        {
        case 'R': //Rojo
            colour = 0b001;
            break;
        case 'G': //Verde
            colour = 0b010;
            break;
        case 'B': //Azul
            colour = 0b100;
            break;
        case 'Y': //Amarillo
            colour = 0b011;
            break;
        case 'M': //Magenta
            colour = 0b101;
            break;
        case 'C': //Cyan
            colour = 0b110;
            break;
        case 'W': //Blanco
            colour = 0b111;
            break;  
        default:
            colour = 0b0;
            break;
        }
            
        colour_mask = (colour<<9) + (colour<<6) + (colour<<3) + colour;

        total_mask = led_mask & colour_mask; 

        XGpio_DiscreteWrite(&GpioOutput,1, (u32) total_mask);
    }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// FIN de  funcionalidad
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        }
	
	cleanup_platform();
	return 0;
}
