# TP4 Diseño Digital - Ejercicio 1
# Francisco Gabriel Rainero. 
# franrainero1@gmail.com 

import math
import numpy as np
import matplotlib.pyplot as plt
import tool.DSPtools as dsp
import tool._fixedInt as fix

##Inicialización
#Parámetros
BR = 1e9    #Símbolos/seg = Baudios
N = 8       #Tasa de Sobremuestreo
T = 1/BR    #Tiempo entre símbolos
fs = N*BR   #Frecuencia de muestreo
Ts = 1/fs   #Periodo de muestreo
L = 1e4     #Cantidad de símbolos a generar
M = 2       #Cantidad de niveles PAM-M

#Coseno realzado h
N_Baud = 16 #Cruces por cero de h
rolloff = [0.0,0.5,1.0]   #Lista rolloff

## Item (1)
#Generación de tres filtros

(t,h1) = dsp.rcosine(rolloff[0],T,N,N_Baud,False)
(t,h2) = dsp.rcosine(rolloff[1],T,N,N_Baud,False)
(t,h3) = dsp.rcosine(rolloff[2],T,N,N_Baud,False)

##Item (2)
#Gráfica de respuesta al impulso y en frecuencia

#Respuesta al impulso
f = plt.figure(figsize=(7,7))
plt.plot(t*1e9,h1,linewidth = 2, label = 'rolloff=0')
plt.plot(t*1e9,h2,linewidth = 2, label = 'rolloff=0,5')
plt.plot(t*1e9,h3,'-r',linewidth = 2, label = 'rolloff=1')
plt.grid(True)
plt.legend()
plt.title('Respuesta al impulso del coseno realzado')
plt.xlabel('Tiempo [ns]')
plt.ylabel('Amplitud')
plt.show()
f.savefig(f"imagenes/coseno_realzado_tiempo.pdf", bbox_inches='tight')

#Respuesta en frecuencia
[H1,A1,F1] = dsp.resp_freq(h1, Ts, 128) #H:Amplitud, A:Fase, F:Frecuencia
[H2,A2,F2] = dsp.resp_freq(h2, Ts, 128)
[H3,A3,F3] = dsp.resp_freq(h3, Ts, 128)

Fn = np.asarray(F1)/1e9 #Frecuencia normalizada a GHz

f = plt.figure(figsize=(7,7))
plt.plot(Fn, 20*np.log10(H1/H1[0]), linewidth = 2, label = 'rolloff = 0')
plt.plot(Fn, 20*np.log10(H2/H2[0]), linewidth = 2, label = 'rolloff = 0,5')
plt.plot(Fn, 20*np.log10(H3/H3[0]),'-r', linewidth = 1.5, label = 'rolloff = 1')
plt.legend()
plt.grid(True)
plt.title('Respuesta en frecuencia del coseno realzado')
plt.xlabel('Frequencia [GHz]')
plt.ylabel('Magnitud [dB]')
plt.show()
f.savefig(f"imagenes/coseno_realzado_frec.pdf", bbox_inches='tight')

##Item (3) 
#Convolución de los símbolos con h_RC y constelación

#Convoluciones
x = dsp.pamm(M,L)           #Generación de símbolos PAM-2
x_up = dsp.upsample(x,N)    #Sobremuestreo

y1 = np.convolve(h1,x_up)
y2 = np.convolve(h2,x_up)
y3 = np.convolve(h3,x_up)

f = plt.figure(figsize=(7,7))
plt.plot(y1, linewidth = 2, label = 'rolloff = 0')
plt.plot(y2, linewidth = 2, label = 'rolloff = 0,5')
plt.plot(y3,'-r', linewidth = 2, label = 'rolloff = 1')
plt.legend()
plt.title('Convolución de h-RC con x_up')
plt.grid(True)
plt.xlabel('Muestras')
plt.ylabel('Magnitud')
plt.xlim(2000,2300)
plt.show()
f.savefig(f"imagenes/convolucion.pdf", bbox_inches='tight')

#Constelaciones

r1 = dsp.downsample(y1,N,1)     #Diezmado (La fase se cambia a mano entre 0 y N-1)
r2 = dsp.downsample(y2,N,1)
r3 = dsp.downsample(y3,N,1)

f = plt.figure(figsize=(7,7))
plt.scatter(r1[100:1000],np.zeros(len(r1[100:1000])).tolist(),label = 'rolloff = 0')    
plt.scatter(r2[100:1000],np.zeros(len(r2[100:1000])).tolist(),label = 'rolloff = 0,5')    
plt.scatter(r3[100:1000],np.zeros(len(r3[100:1000])).tolist(),label = 'rolloff = 1', color='red')
plt.legend()
plt.title('Constelación PAM-2')
plt.grid(True)
plt.xlabel('I')
plt.ylabel('Q')    
plt.ylim(-1.1,1.1)
plt.show()
f.savefig(f"imagenes/constelacion.pdf", bbox_inches='tight')

##Item (4)  Item (5) Item (6)
#Cuantizaciones, gráficas y SNR

h1_o = h1.copy()     #Guardo respuestas al impulso originales
h2_o = h2.copy()
h3_o = h3.copy()

y1_o = y1.copy()    #Guardo salidas del filtrado
y2_o = y2.copy()
y3_o = y3.copy()

Py1 = np.sum(y1**2)  #Potencia de señal (Se debería dividir por su longitud pero se cancela en SNR)
Py2 = np.sum(y2**2)  
Py3 = np.sum(y3**2)  

#------------------S(8,7) truncado-----------------------

h1_8_7_t = fix.arrayFixedInt(8, 7, h1_o, signedMode='S', roundMode='trunc', saturateMode='saturate')
h2_8_7_t = fix.arrayFixedInt(8, 7, h2_o, signedMode='S', roundMode='trunc', saturateMode='saturate')
h3_8_7_t = fix.arrayFixedInt(8, 7, h3_o, signedMode='S', roundMode='trunc', saturateMode='saturate')

for i in range(len(h1_o)):
    h1[i] = h1_8_7_t[i].fValue
    h2[i] = h2_8_7_t[i].fValue
    h3[i] = h3_8_7_t[i].fValue

#Respuesta al impulso
f = plt.figure(figsize=(7,7))
plt.plot(t*1e9,h1,linewidth = 2, label = 'rolloff=0')
plt.plot(t*1e9,h2,linewidth = 2, label = 'rolloff=0,5')
plt.plot(t*1e9,h3,'-r',linewidth = 2, label = 'rolloff=1')
plt.grid(True)
plt.legend()
plt.title('Respuesta al impulso del coseno realzado. FixPoint S(8,7)T')
plt.xlabel('Tiempo [ns]')
plt.ylabel('Amplitud')
plt.show()
f.savefig(f"imagenes/coseno_realzado_tiempo_S_8_7_T.pdf", bbox_inches='tight')

#Respuesta en frecuencia
[H1,A1,F1] = dsp.resp_freq(h1, Ts, 128) #H:Amplitud, A:Fase, F:Frecuencia
[H2,A2,F2] = dsp.resp_freq(h2, Ts, 128)
[H3,A3,F3] = dsp.resp_freq(h3, Ts, 128)
Fn = np.asarray(F1)/1e9 #Frecuencia normalizada a GHz
f = plt.figure(figsize=(7,7))
plt.plot(Fn, 20*np.log10(H1/H1[0]), linewidth = 2, label = 'rolloff = 0')
plt.plot(Fn, 20*np.log10(H2/H2[0]), linewidth = 2, label = 'rolloff = 0,5')
plt.plot(Fn, 20*np.log10(H3/H3[0]),'-r', linewidth = 1.5, label = 'rolloff = 1')
plt.legend()
plt.grid(True)
plt.title('Respuesta en frecuencia del coseno realzado. FixPoint S(8,7)T')
plt.xlabel('Frequencia [GHz]')
plt.ylabel('Magnitud [dB]')
plt.show()
f.savefig(f"imagenes/coseno_realzado_frec_S_8_7_T.pdf", bbox_inches='tight')

#Convoluciones
y1 = np.convolve(h1,x_up)
y2 = np.convolve(h2,x_up)
y3 = np.convolve(h3,x_up)
f = plt.figure(figsize=(7,7))
plt.plot(y1, linewidth = 2, label = 'rolloff = 0')
plt.plot(y2, linewidth = 2, label = 'rolloff = 0,5')
plt.plot(y3,'-r', linewidth = 2, label = 'rolloff = 1')
plt.legend()
plt.title('Convolución de h-RC con x_up. FixPoint S(8,7)T')
plt.grid(True)
plt.xlabel('Muestras')
plt.ylabel('Magnitud')
plt.xlim(2000,2300)
plt.show()
f.savefig(f"imagenes/convolucion_S_8_7_T.pdf", bbox_inches='tight')

#Constelaciones
r1 = dsp.downsample(y1,N,1)     #Diezmado (La fase se cambia a mano entre 0 y N-1)
r2 = dsp.downsample(y2,N,1)
r3 = dsp.downsample(y3,N,1)
f = plt.figure(figsize=(7,7))
plt.scatter(r1[100:1000],np.zeros(len(r1[100:1000])).tolist(),label = 'rolloff = 0')    
plt.scatter(r2[100:1000],np.zeros(len(r2[100:1000])).tolist(),label = 'rolloff = 0,5')    
plt.scatter(r3[100:1000],np.zeros(len(r3[100:1000])).tolist(),label = 'rolloff = 1', color='red')
plt.legend()
plt.title('Constelación PAM-2. FixPoint S(8,7)T')
plt.grid(True)
plt.xlabel('I')
plt.ylabel('Q')    
plt.ylim(-1.1,1.1)
plt.show()
f.savefig(f"imagenes/constelacion_S_8_7_T.pdf", bbox_inches='tight')

#SNR
y1_n = y1 - y1_o        #Error de cuantización
y2_n = y2 - y2_o 
y3_n = y3 - y3_o 

Pn1 = np.sum(y1_n**2)   #Potencia de ruido (Se debería dividir por la longitud pero se cancela para SNR)
Pn2 = np.sum(y2_n**2)
Pn3 = np.sum(y3_n**2)

SNR1_dB = 10*np.log10(Py1/Pn1)
SNR2_dB = 10*np.log10(Py2/Pn2)
SNR3_dB = 10*np.log10(Py3/Pn3)

f = plt.figure(figsize = [7,20])
for i in range(3):
    plt.subplot(3,1,i+1)
    if i == 0:
        plt.plot(y1,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y1_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y1_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('S(8,7)T \n Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR1_dB))
    if i == 1:
        plt.plot(y2,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y2_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y2_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR2_dB))
    if i == 2:
        plt.plot(y3,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y3_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y3_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} .- SNR[dB] = {}'.format(rolloff[i],SNR3_dB))

    plt.xlim(1000,1300)
    plt.grid(True)
    plt.legend()
plt.xlabel('Muestras')
plt.show()
f.savefig(f"imagenes/SNR_S_8_7_T.pdf", bbox_inches='tight')


#---------------------S(8,7) redondeo------------------------

h1_8_7_r = fix.arrayFixedInt(8, 7, h1_o, signedMode='S', roundMode='round', saturateMode='saturate')
h2_8_7_r = fix.arrayFixedInt(8, 7, h2_o, signedMode='S', roundMode='round', saturateMode='saturate')
h3_8_7_r = fix.arrayFixedInt(8, 7, h3_o, signedMode='S', roundMode='round', saturateMode='saturate')

for i in range(len(h1_o)):
    h1[i] = h1_8_7_r[i].fValue
    h2[i] = h2_8_7_r[i].fValue
    h3[i] = h3_8_7_r[i].fValue

#Respuesta al impulso
f = plt.figure(figsize=(7,7))
plt.plot(t*1e9,h1,linewidth = 2, label = 'rolloff=0')
plt.plot(t*1e9,h2,linewidth = 2, label = 'rolloff=0,5')
plt.plot(t*1e9,h3,'-r',linewidth = 2, label = 'rolloff=1')
plt.grid(True)
plt.legend()
plt.title('Respuesta al impulso del coseno realzado. FixPoint S(8,7)R')
plt.xlabel('Tiempo [ns]')
plt.ylabel('Amplitud')
plt.show()
f.savefig(f"imagenes/coseno_realzado_tiempo_S_8_7_R.pdf", bbox_inches='tight')

#Respuesta en frecuencia
[H1,A1,F1] = dsp.resp_freq(h1, Ts, 128) #H:Amplitud, A:Fase, F:Frecuencia
[H2,A2,F2] = dsp.resp_freq(h2, Ts, 128)
[H3,A3,F3] = dsp.resp_freq(h3, Ts, 128)
Fn = np.asarray(F1)/1e9 #Frecuencia normalizada a GHz
f = plt.figure(figsize=(7,7))
plt.plot(Fn, 20*np.log10(H1/H1[0]), linewidth = 2, label = 'rolloff = 0')
plt.plot(Fn, 20*np.log10(H2/H2[0]), linewidth = 2, label = 'rolloff = 0,5')
plt.plot(Fn, 20*np.log10(H3/H3[0]),'-r', linewidth = 1.5, label = 'rolloff = 1')
plt.legend()
plt.grid(True)
plt.title('Respuesta en frecuencia del coseno realzado. FixPoint S(8,7)R')
plt.xlabel('Frequencia [GHz]')
plt.ylabel('Magnitud [dB]')
plt.show()
f.savefig(f"imagenes/coseno_realzado_frec_S_8_7_R.pdf", bbox_inches='tight')

#Convoluciones
y1 = np.convolve(h1,x_up)
y2 = np.convolve(h2,x_up)
y3 = np.convolve(h3,x_up)
f = plt.figure(figsize=(7,7))
plt.plot(y1, linewidth = 2, label = 'rolloff = 0')
plt.plot(y2, linewidth = 2, label = 'rolloff = 0,5')
plt.plot(y3,'-r', linewidth = 2, label = 'rolloff = 1')
plt.legend()
plt.title('Convolución de h-RC con x_up. FixPoint S(8,7)R')
plt.grid(True)
plt.xlabel('Muestras')
plt.ylabel('Magnitud')
plt.xlim(2000,2300)
plt.show()
f.savefig(f"imagenes/convolucion_S_8_7_R.pdf", bbox_inches='tight')

#Constelaciones
r1 = dsp.downsample(y1,N,1)     #Diezmado (La fase se cambia a mano entre 0 y N-1)
r2 = dsp.downsample(y2,N,1)
r3 = dsp.downsample(y3,N,1)
f = plt.figure(figsize=(7,7))
plt.scatter(r1[100:1000],np.zeros(len(r1[100:1000])).tolist(),label = 'rolloff = 0')    
plt.scatter(r2[100:1000],np.zeros(len(r2[100:1000])).tolist(),label = 'rolloff = 0,5')    
plt.scatter(r3[100:1000],np.zeros(len(r3[100:1000])).tolist(),label = 'rolloff = 1', color='red')
plt.legend()
plt.title('Constelación PAM-2. FixPoint S(8,7)R')
plt.grid(True)
plt.xlabel('I')
plt.ylabel('Q')    
plt.ylim(-1.1,1.1)
plt.show()
f.savefig(f"imagenes/constelacion_S_8_7_R.pdf", bbox_inches='tight')

#SNR
y1_n = y1 - y1_o        #Error de cuantización
y2_n = y2 - y2_o 
y3_n = y3 - y3_o 

Pn1 = np.sum(y1_n**2)   #Potencia de ruido (Se debería dividir por la longitud pero se cancela para SNR)
Pn2 = np.sum(y2_n**2)
Pn3 = np.sum(y3_n**2)

SNR1_dB = 10*np.log10(Py1/Pn1)
SNR2_dB = 10*np.log10(Py2/Pn2)
SNR3_dB = 10*np.log10(Py3/Pn3)

f = plt.figure(figsize = [7,20])
for i in range(3):
    plt.subplot(3,1,i+1)
    if i == 0:
        plt.plot(y1,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y1_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y1_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('S(8,7)R \n Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR1_dB))
    if i == 1:
        plt.plot(y2,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y2_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y2_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR2_dB))
    if i == 2:
        plt.plot(y3,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y3_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y3_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} .- SNR[dB] = {}'.format(rolloff[i],SNR3_dB))

    plt.xlim(1000,1300)
    plt.grid(True)
    plt.legend()
plt.xlabel('Muestras')
plt.show()
f.savefig(f"imagenes/SNR_S_8_7_R.pdf", bbox_inches='tight')

#-----------------S(3,2) truncado------------------------

h1_3_2_t = fix.arrayFixedInt(3, 2, h1_o, signedMode='S', roundMode='trunc', saturateMode='saturate')
h2_3_2_t = fix.arrayFixedInt(3, 2, h2_o, signedMode='S', roundMode='trunc', saturateMode='saturate')
h3_3_2_t = fix.arrayFixedInt(3, 2, h3_o, signedMode='S', roundMode='trunc', saturateMode='saturate')

for i in range(len(h1_o)):
    h1[i] = h1_3_2_t[i].fValue
    h2[i] = h2_3_2_t[i].fValue
    h3[i] = h3_3_2_t[i].fValue

#Respuesta al impulso
f = plt.figure(figsize=(7,7))
plt.plot(t*1e9,h1,linewidth = 2, label = 'rolloff=0')
plt.plot(t*1e9,h2,linewidth = 2, label = 'rolloff=0,5')
plt.plot(t*1e9,h3,'-r',linewidth = 2, label = 'rolloff=1')
plt.grid(True)
plt.legend()
plt.title('Respuesta al impulso del coseno realzado. FixPoint S(3,2)T')
plt.xlabel('Tiempo [ns]')
plt.ylabel('Amplitud')
plt.show()
f.savefig(f"imagenes/coseno_realzado_tiempo_S_3_2_T.pdf", bbox_inches='tight')

#Respuesta en frecuencia
[H1,A1,F1] = dsp.resp_freq(h1, Ts, 128) #H:Amplitud, A:Fase, F:Frecuencia
[H2,A2,F2] = dsp.resp_freq(h2, Ts, 128)
[H3,A3,F3] = dsp.resp_freq(h3, Ts, 128)
Fn = np.asarray(F1)/1e9 #Frecuencia normalizada a GHz
f = plt.figure(figsize=(7,7))
plt.plot(Fn, 20*np.log10(H1/H1[0]), linewidth = 2, label = 'rolloff = 0')
plt.plot(Fn, 20*np.log10(H2/H2[0]), linewidth = 2, label = 'rolloff = 0,5')
plt.plot(Fn, 20*np.log10(H3/H3[0]),'-r', linewidth = 1.5, label = 'rolloff = 1')
plt.legend()
plt.grid(True)
plt.title('Respuesta en frecuencia del coseno realzado. FixPoint S(3,2)T')
plt.xlabel('Frequencia [GHz]')
plt.ylabel('Magnitud [dB]')
plt.show()
f.savefig(f"imagenes/coseno_realzado_frec_S_3_2_T.pdf", bbox_inches='tight')

#Convoluciones
y1 = np.convolve(h1,x_up)
y2 = np.convolve(h2,x_up)
y3 = np.convolve(h3,x_up)
f = plt.figure(figsize=(7,7))
plt.plot(y1, linewidth = 2, label = 'rolloff = 0')
plt.plot(y2, linewidth = 2, label = 'rolloff = 0,5')
plt.plot(y3,'-r', linewidth = 2, label = 'rolloff = 1')
plt.legend()
plt.title('Convolución de h-RC con x_up. FixPoint S(3,2)T')
plt.grid(True)
plt.xlabel('Muestras')
plt.ylabel('Magnitud')
plt.xlim(2000,2300)
plt.show()
f.savefig(f"imagenes/convolucion_S_3_2_T.pdf", bbox_inches='tight')

#Constelaciones
r1 = dsp.downsample(y1,N,1)     #Diezmado (La fase se cambia a mano entre 0 y N-1)
r2 = dsp.downsample(y2,N,1)
r3 = dsp.downsample(y3,N,1)
f = plt.figure(figsize=(7,7))
plt.scatter(r1[100:1000],np.zeros(len(r1[100:1000])).tolist(),label = 'rolloff = 0')    
plt.scatter(r2[100:1000],np.zeros(len(r2[100:1000])).tolist(),label = 'rolloff = 0,5')    
plt.scatter(r3[100:1000],np.zeros(len(r3[100:1000])).tolist(),label = 'rolloff = 1', color='red')
plt.legend()
plt.title('Constelación PAM-2. FixPoint S(3,2)T')
plt.grid(True)
plt.xlabel('I')
plt.ylabel('Q')    
plt.ylim(-1.1,1.1)
plt.show()
f.savefig(f"imagenes/constelacion_S_3_2_T.pdf", bbox_inches='tight')

#SNR
y1_n = y1 - y1_o        #Error de cuantización
y2_n = y2 - y2_o 
y3_n = y3 - y3_o 

Pn1 = np.sum(y1_n**2)   #Potencia de ruido (Se debería dividir por la longitud pero se cancela para SNR)
Pn2 = np.sum(y2_n**2)
Pn3 = np.sum(y3_n**2)

SNR1_dB = 10*np.log10(Py1/Pn1)
SNR2_dB = 10*np.log10(Py2/Pn2)
SNR3_dB = 10*np.log10(Py3/Pn3)

f = plt.figure(figsize = [7,20])
for i in range(3):
    plt.subplot(3,1,i+1)
    if i == 0:
        plt.plot(y1,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y1_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y1_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('S(3,2)T \n Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR1_dB))
    if i == 1:
        plt.plot(y2,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y2_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y2_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR2_dB))
    if i == 2:
        plt.plot(y3,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y3_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y3_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} .- SNR[dB] = {}'.format(rolloff[i],SNR3_dB))

    plt.xlim(1000,1300)
    plt.grid(True)
    plt.legend()
plt.xlabel('Muestras')
plt.show()
f.savefig(f"imagenes/SNR_S_3_2_T.pdf", bbox_inches='tight')

#--------------------S(3,2) redondeo-----------------------

h1_3_2_r = fix.arrayFixedInt(3, 2, h1_o, signedMode='S', roundMode='round', saturateMode='saturate')
h2_3_2_r = fix.arrayFixedInt(3, 2, h2_o, signedMode='S', roundMode='round', saturateMode='saturate')
h3_3_2_r = fix.arrayFixedInt(3, 2, h3_o, signedMode='S', roundMode='round', saturateMode='saturate')

for i in range(len(h1_o)):
    h1[i] = h1_3_2_r[i].fValue
    h2[i] = h2_3_2_r[i].fValue
    h3[i] = h3_3_2_r[i].fValue

#Respuesta al impulso
f = plt.figure(figsize=(7,7))
plt.plot(t*1e9,h1,linewidth = 2, label = 'rolloff=0')
plt.plot(t*1e9,h2,linewidth = 2, label = 'rolloff=0,5')
plt.plot(t*1e9,h3,'-r',linewidth = 2, label = 'rolloff=1')
plt.grid(True)
plt.legend()
plt.title('Respuesta al impulso del coseno realzado. FixPoint S(3,2)R')
plt.xlabel('Tiempo [ns]')
plt.ylabel('Amplitud')
plt.show()
f.savefig(f"imagenes/coseno_realzado_tiempo_S_3_2_R.pdf", bbox_inches='tight')

#Respuesta en frecuencia
[H1,A1,F1] = dsp.resp_freq(h1, Ts, 128) #H:Amplitud, A:Fase, F:Frecuencia
[H2,A2,F2] = dsp.resp_freq(h2, Ts, 128)
[H3,A3,F3] = dsp.resp_freq(h3, Ts, 128)
Fn = np.asarray(F1)/1e9 #Frecuencia normalizada a GHz
f = plt.figure(figsize=(7,7))
plt.plot(Fn, 20*np.log10(H1/H1[0]), linewidth = 2, label = 'rolloff = 0')
plt.plot(Fn, 20*np.log10(H2/H2[0]), linewidth = 2, label = 'rolloff = 0,5')
plt.plot(Fn, 20*np.log10(H3/H3[0]),'-r', linewidth = 1.5, label = 'rolloff = 1')
plt.legend()
plt.grid(True)
plt.title('Respuesta en frecuencia del coseno realzado. FixPoint S(3,2)R')
plt.xlabel('Frequencia [GHz]')
plt.ylabel('Magnitud [dB]')
plt.show()
f.savefig(f"imagenes/coseno_realzado_frec_S_3_2_R.pdf", bbox_inches='tight')

#Convoluciones
y1 = np.convolve(h1,x_up)
y2 = np.convolve(h2,x_up)
y3 = np.convolve(h3,x_up)
f = plt.figure(figsize=(7,7))
plt.plot(y1, linewidth = 2, label = 'rolloff = 0')
plt.plot(y2, linewidth = 2, label = 'rolloff = 0,5')
plt.plot(y3,'-r', linewidth = 2, label = 'rolloff = 1')
plt.legend()
plt.title('Convolución de h-RC con x_up. FixPoint S(3,2)R')
plt.grid(True)
plt.xlabel('Muestras')
plt.ylabel('Magnitud')
plt.xlim(2000,2300)
plt.show()
f.savefig(f"imagenes/convolucion_S_3_2_R.pdf", bbox_inches='tight')

#Constelaciones
r1 = dsp.downsample(y1,N,1)     #Diezmado (La fase se cambia a mano entre 0 y N-1)
r2 = dsp.downsample(y2,N,1)
r3 = dsp.downsample(y3,N,1)
f = plt.figure(figsize=(7,7))
plt.scatter(r1[100:1000],np.zeros(len(r1[100:1000])).tolist(),label = 'rolloff = 0')    
plt.scatter(r2[100:1000],np.zeros(len(r2[100:1000])).tolist(),label = 'rolloff = 0,5')    
plt.scatter(r3[100:1000],np.zeros(len(r3[100:1000])).tolist(),label = 'rolloff = 1', color='red')
plt.legend()
plt.title('Constelación PAM-2. FixPoint S(3,2)R')
plt.grid(True)
plt.xlabel('I')
plt.ylabel('Q')    
plt.ylim(-1.1,1.1)
plt.show()
f.savefig(f"imagenes/constelacion_S_3_2_R.pdf", bbox_inches='tight')

#SNR
y1_n = y1 - y1_o        #Error de cuantización
y2_n = y2 - y2_o 
y3_n = y3 - y3_o 

Pn1 = np.sum(y1_n**2)   #Potencia de ruido (Se debería dividir por la longitud pero se cancela para SNR)
Pn2 = np.sum(y2_n**2)
Pn3 = np.sum(y3_n**2)

SNR1_dB = 10*np.log10(Py1/Pn1)
SNR2_dB = 10*np.log10(Py2/Pn2)
SNR3_dB = 10*np.log10(Py3/Pn3)

f = plt.figure(figsize = [7,20])
for i in range(3):
    plt.subplot(3,1,i+1)
    if i == 0:
        plt.plot(y1,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y1_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y1_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('S(3,2)R \n Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR1_dB))
    if i == 1:
        plt.plot(y2,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y2_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y2_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR2_dB))
    if i == 2:
        plt.plot(y3,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y3_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y3_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} .- SNR[dB] = {}'.format(rolloff[i],SNR3_dB))

    plt.xlim(1000,1300)
    plt.grid(True)
    plt.legend()
plt.xlabel('Muestras')
plt.show()
f.savefig(f"imagenes/SNR_S_3_2_R.pdf", bbox_inches='tight')

#--------------------S(6,4) truncado-------------------------

h1_6_4_t = fix.arrayFixedInt(6, 4, h1_o, signedMode='S', roundMode='trunc', saturateMode='saturate')
h2_6_4_t = fix.arrayFixedInt(6, 4, h2_o, signedMode='S', roundMode='trunc', saturateMode='saturate')
h3_6_4_t = fix.arrayFixedInt(6, 4, h3_o, signedMode='S', roundMode='trunc', saturateMode='saturate')

for i in range(len(h1_o)):
    h1[i] = h1_6_4_t[i].fValue
    h2[i] = h2_6_4_t[i].fValue
    h3[i] = h3_6_4_t[i].fValue

#Respuesta al impulso
f = plt.figure(figsize=(7,7))
plt.plot(t*1e9,h1,linewidth = 2, label = 'rolloff=0')
plt.plot(t*1e9,h2,linewidth = 2, label = 'rolloff=0,5')
plt.plot(t*1e9,h3,'-r',linewidth = 2, label = 'rolloff=1')
plt.grid(True)
plt.legend()
plt.title('Respuesta al impulso del coseno realzado. FixPoint S(6,4)T')
plt.xlabel('Tiempo [ns]')
plt.ylabel('Amplitud')
plt.show()
f.savefig(f"imagenes/coseno_realzado_tiempo_S_6_4_T.pdf", bbox_inches='tight')

#Respuesta en frecuencia
[H1,A1,F1] = dsp.resp_freq(h1, Ts, 128) #H:Amplitud, A:Fase, F:Frecuencia
[H2,A2,F2] = dsp.resp_freq(h2, Ts, 128)
[H3,A3,F3] = dsp.resp_freq(h3, Ts, 128)
Fn = np.asarray(F1)/1e9 #Frecuencia normalizada a GHz
f = plt.figure(figsize=(7,7))
plt.plot(Fn, 20*np.log10(H1/H1[0]), linewidth = 2, label = 'rolloff = 0')
plt.plot(Fn, 20*np.log10(H2/H2[0]), linewidth = 2, label = 'rolloff = 0,5')
plt.plot(Fn, 20*np.log10(H3/H3[0]),'-r', linewidth = 1.5, label = 'rolloff = 1')
plt.legend()
plt.grid(True)
plt.title('Respuesta en frecuencia del coseno realzado. FixPoint S(6,4)T')
plt.xlabel('Frequencia [GHz]')
plt.ylabel('Magnitud [dB]')
plt.show()
f.savefig(f"imagenes/coseno_realzado_frec_S_6_4_T.pdf", bbox_inches='tight')

#Convoluciones
y1 = np.convolve(h1,x_up)
y2 = np.convolve(h2,x_up)
y3 = np.convolve(h3,x_up)
f = plt.figure(figsize=(7,7))
plt.plot(y1, linewidth = 2, label = 'rolloff = 0')
plt.plot(y2, linewidth = 2, label = 'rolloff = 0,5')
plt.plot(y3,'-r', linewidth = 2, label = 'rolloff = 1')
plt.legend()
plt.title('Convolución de h-RC con x_up. FixPoint S(6,4)T')
plt.grid(True)
plt.xlabel('Muestras')
plt.ylabel('Magnitud')
plt.xlim(2000,2300)
plt.show()
f.savefig(f"imagenes/convolucion_S_6_4_T.pdf", bbox_inches='tight')

#Constelaciones
r1 = dsp.downsample(y1,N,1)     #Diezmado (La fase se cambia a mano entre 0 y N-1)
r2 = dsp.downsample(y2,N,1)
r3 = dsp.downsample(y3,N,1)
f = plt.figure(figsize=(7,7))
plt.scatter(r1[100:1000],np.zeros(len(r1[100:1000])).tolist(),label = 'rolloff = 0')    
plt.scatter(r2[100:1000],np.zeros(len(r2[100:1000])).tolist(),label = 'rolloff = 0,5')    
plt.scatter(r3[100:1000],np.zeros(len(r3[100:1000])).tolist(),label = 'rolloff = 1', color='red')
plt.legend()
plt.title('Constelación PAM-2. FixPoint S(6,4)T')
plt.grid(True)
plt.xlabel('I')
plt.ylabel('Q')    
plt.ylim(-1.1,1.1)
plt.show()
f.savefig(f"imagenes/constelacion_S_6_4_T.pdf", bbox_inches='tight')

#SNR
y1_n = y1 - y1_o        #Error de cuantización
y2_n = y2 - y2_o 
y3_n = y3 - y3_o 

Pn1 = np.sum(y1_n**2)   #Potencia de ruido (Se debería dividir por la longitud pero se cancela para SNR)
Pn2 = np.sum(y2_n**2)
Pn3 = np.sum(y3_n**2)

SNR1_dB = 10*np.log10(Py1/Pn1)
SNR2_dB = 10*np.log10(Py2/Pn2)
SNR3_dB = 10*np.log10(Py3/Pn3)

f = plt.figure(figsize = [7,20])
for i in range(3):
    plt.subplot(3,1,i+1)
    if i == 0:
        plt.plot(y1,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y1_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y1_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('S(6,4)T \n Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR1_dB))
    if i == 1:
        plt.plot(y2,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y2_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y2_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR2_dB))
    if i == 2:
        plt.plot(y3,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y3_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y3_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} .- SNR[dB] = {}'.format(rolloff[i],SNR3_dB))

    plt.xlim(1000,1300)
    plt.grid(True)
    plt.legend()
plt.xlabel('Muestras')
plt.show()
f.savefig(f"imagenes/SNR_S_6_4_T.pdf", bbox_inches='tight')

#---------------------S(6,4) redondeo------------------------

h1_6_4_r = fix.arrayFixedInt(6, 4, h1_o, signedMode='S', roundMode='round', saturateMode='saturate')
h2_6_4_r = fix.arrayFixedInt(6, 4, h2_o, signedMode='S', roundMode='round', saturateMode='saturate')
h3_6_4_r = fix.arrayFixedInt(6, 4, h3_o, signedMode='S', roundMode='round', saturateMode='saturate')

for i in range(len(h1_o)):
    h1[i] = h1_6_4_r[i].fValue
    h2[i] = h2_6_4_r[i].fValue
    h3[i] = h3_6_4_r[i].fValue

#Respuesta al impulso
f = plt.figure(figsize=(7,7))
plt.plot(t*1e9,h1,linewidth = 2, label = 'rolloff=0')
plt.plot(t*1e9,h2,linewidth = 2, label = 'rolloff=0,5')
plt.plot(t*1e9,h3,'-r',linewidth = 2, label = 'rolloff=1')
plt.grid(True)
plt.legend()
plt.title('Respuesta al impulso del coseno realzado. FixPoint S(6,4)R')
plt.xlabel('Tiempo [ns]')
plt.ylabel('Amplitud')
plt.show()
f.savefig(f"imagenes/coseno_realzado_tiempo_S_6_4_R.pdf", bbox_inches='tight')

#Respuesta en frecuencia
[H1,A1,F1] = dsp.resp_freq(h1, Ts, 128) #H:Amplitud, A:Fase, F:Frecuencia
[H2,A2,F2] = dsp.resp_freq(h2, Ts, 128)
[H3,A3,F3] = dsp.resp_freq(h3, Ts, 128)
Fn = np.asarray(F1)/1e9 #Frecuencia normalizada a GHz
f = plt.figure(figsize=(7,7))
plt.plot(Fn, 20*np.log10(H1/H1[0]), linewidth = 2, label = 'rolloff = 0')
plt.plot(Fn, 20*np.log10(H2/H2[0]), linewidth = 2, label = 'rolloff = 0,5')
plt.plot(Fn, 20*np.log10(H3/H3[0]),'-r', linewidth = 1.5, label = 'rolloff = 1')
plt.legend()
plt.grid(True)
plt.title('Respuesta en frecuencia del coseno realzado. FixPoint S(6,4)R')
plt.xlabel('Frequencia [GHz]')
plt.ylabel('Magnitud [dB]')
plt.show()
f.savefig(f"imagenes/coseno_realzado_frec_S_6_4_R.pdf", bbox_inches='tight')

#Convoluciones
y1 = np.convolve(h1,x_up)
y2 = np.convolve(h2,x_up)
y3 = np.convolve(h3,x_up)
f = plt.figure(figsize=(7,7))
plt.plot(y1, linewidth = 2, label = 'rolloff = 0')
plt.plot(y2, linewidth = 2, label = 'rolloff = 0,5')
plt.plot(y3,'-r', linewidth = 2, label = 'rolloff = 1')
plt.legend()
plt.title('Convolución de h-RC con x_up. FixPoint S(6,4)R')
plt.grid(True)
plt.xlabel('Muestras')
plt.ylabel('Magnitud')
plt.xlim(2000,2300)
plt.show()
f.savefig(f"imagenes/convolucion_S_6_4_R.pdf", bbox_inches='tight')

#Constelaciones
r1 = dsp.downsample(y1,N,1)     #Diezmado (La fase se cambia a mano entre 0 y N-1)
r2 = dsp.downsample(y2,N,1)
r3 = dsp.downsample(y3,N,1)
f = plt.figure(figsize=(7,7))
plt.scatter(r1[100:1000],np.zeros(len(r1[100:1000])).tolist(),label = 'rolloff = 0')    
plt.scatter(r2[100:1000],np.zeros(len(r2[100:1000])).tolist(),label = 'rolloff = 0,5')    
plt.scatter(r3[100:1000],np.zeros(len(r3[100:1000])).tolist(),label = 'rolloff = 1', color='red')
plt.legend()
plt.title('Constelación PAM-2. FixPoint S(6,4)R')
plt.grid(True)
plt.xlabel('I')
plt.ylabel('Q')    
plt.ylim(-1.1,1.1)
plt.show()
f.savefig(f"imagenes/constelacion_S_6_4_R.pdf", bbox_inches='tight')

#SNR
y1_n = y1 - y1_o        #Error de cuantización
y2_n = y2 - y2_o 
y3_n = y3 - y3_o 

Pn1 = np.sum(y1_n**2)   #Potencia de ruido (Se debería dividir por la longitud pero se cancela para SNR)
Pn2 = np.sum(y2_n**2)
Pn3 = np.sum(y3_n**2)

SNR1_dB = 10*np.log10(Py1/Pn1)
SNR2_dB = 10*np.log10(Py2/Pn2)
SNR3_dB = 10*np.log10(Py3/Pn3)

f = plt.figure(figsize = [7,20])
for i in range(3):
    plt.subplot(3,1,i+1)
    if i == 0:
        plt.plot(y1,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y1_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y1_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('S(6,4)R \n Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR1_dB))
    if i == 1:
        plt.plot(y2,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y2_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y2_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} - SNR[dB] = {}'.format(rolloff[i],SNR2_dB))
    if i == 2:
        plt.plot(y3,linewidth = 2,label = 'Punto Fijo')
        plt.plot(y3_o,linewidth = 2,label = 'Punto Flotante')
        plt.plot(y3_n , '-r',linewidth = 2,label = 'Error')
        plt.title ('Error de cuantizacion. Rolloff = {} .- SNR[dB] = {}'.format(rolloff[i],SNR3_dB))

    plt.xlim(1000,1300)
    plt.grid(True)
    plt.legend()
plt.xlabel('Muestras')
plt.show()
f.savefig(f"imagenes/SNR_S_6_4_R.pdf", bbox_inches='tight')