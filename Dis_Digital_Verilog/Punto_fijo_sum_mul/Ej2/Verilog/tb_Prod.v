// TP4 Diseño Digital -TP4- Ejercicio 2
// Testbench producto en punto fijo
// Francisco Gabriel Rainero. 
// franrainero1@gmail.com

`timescale 1ns/100ps

module tb_Prod ();

    reg  [8-1:0] i_A  ;
    reg  [12-1:0] i_B  ;
    
    wire [20  - 1:0] o_C_FR ; 
    wire [12  - 1:0] o_C_OF  ; 
    wire [12  - 1:0] o_C_SAT  ; 
    wire [10  - 1:0] o_C_RED  ;   
    
    reg     clock ;

    integer fileA  ;
    integer fileB  ;
    integer fileCa ;
    integer fileCb ;
    integer fileCc ;
    integer fileCd ;

    integer  errorA;
    integer  errorB;

    
    initial begin
        clock = 1'b0;
        i_A   = 16'b0 ;
        i_B   = 12'b0 ;

        // ARCHIVOS DE ENTRADA 
        
        /* Entrada i_A */
        fileA = $fopen("D:/Fulgor/Dis_Digital/TP4/Ej2/Python/mul_vectors/A.in","r");
        if(fileA == 0) begin
           $display("Error abriendo el archivo de A");
           $stop; 
        end       

        /* Entrada i_B */
        fileB = $fopen("D:/Fulgor/Dis_Digital/TP4/Ej2/Python/mul_vectors/B.in","r");
        if(fileB == 0) begin
           $display("Error abriendo el archivo de B");
           $stop; 
        end  

        /* ARCHIVOS DE SALIDA */
        
        /* Salida o_c_full_res */
        fileCa = $fopen("D:/Fulgor/Dis_Digital/TP4/Ej2/Python/mul_vectors/Ca_tb.out","w");
        if(fileCa == 0) begin
           $display("Error abriendo el archivo de Ca");
           $stop; 
        end

        /* Salida o_c_trn_ovf */
        fileCb = $fopen("D:/Fulgor/Dis_Digital/TP4/Ej2/Python/mul_vectors/Cb_tb.out","w");
        if(fileCb == 0) begin
           $display("Error abriendo el archivo de Cb");
           $stop; 
        end

        /* Salida o_c_trn_sat */
        fileCc = $fopen("D:/Fulgor/Dis_Digital/TP4/Ej2/Python/mul_vectors/Cc_tb.out","w");
        if(fileCc == 0) begin
           $display("Error abriendo el archivo de Cc");
           $stop; 
        end

        /* Salida o_c_rnd_sat */
        fileCd = $fopen("D:/Fulgor/Dis_Digital/TP4/Ej2/Python/mul_vectors/Cd_tb.out","w");
        if(fileCd == 0) begin
           $display("Error abriendo el archivo de Cd");
           $stop; 
        end

    end

    always @(posedge clock) begin
        /* Levanta nuevo valor de i_a*/
        errorA <= $fscanf(fileA,"%b", i_A);
        
        /* Levanta nuevo valor de i_b*/
        errorB <= $fscanf(fileB,"%b", i_B);

        /* Cierra archivos por error o EOF */
        if( errorA != 1 || errorB != 1 ) begin
            $fclose(fileA );
            $fclose(fileB );
            $fclose(fileCa);
            $fclose(fileCb);
            $fclose(fileCc);
            $fclose(fileCd);
            $finish;
        end  
    end

    always @(negedge clock) begin
        /* Guarda valor de o_c */
        $fdisplay( fileCa,"%b", o_C_FR  );
        $fdisplay( fileCb,"%b", o_C_OF  );
        $fdisplay( fileCc,"%b", o_C_SAT );
        $fdisplay( fileCd,"%b", o_C_RED );
    end

    /* Generacion de clock */
    always begin
        #10 clock = ~clock ;
    end

    /* Instanciacion del DUT en modo FULL RESOLUTION */
    Prod_Punto_Fijo #(
    )
    DUT
    (
        .i_A( i_A          ),
        .i_B( i_B          ),        
        .o_C_FR (o_C_FR  ),
        .o_C_OF (o_C_OF  ),
        .o_C_SAT(o_C_SAT ),
        .o_C_RED(o_C_RED )
    );

endmodule