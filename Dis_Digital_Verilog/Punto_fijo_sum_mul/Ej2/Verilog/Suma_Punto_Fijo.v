// TP4 Diseño Digital -TP4- Ejercicio 2
// Módulo Suma en punto fijo
// Francisco Gabriel Rainero. 
// franrainero1@gmail.com

module Suma_Punto_Fijo #(

    // Parámetros de entrada 1 [S(16,14)]
    parameter NB_IN_A  = 16,               //Bits totales
    parameter NBF_IN_A = 14,               //Bits fraccionales
    parameter NBI_IN_A = NB_IN_A-NBF_IN_A, //Bits enteros = Totales - Fraccionales

    // Parámetros de entrada 2 [S(12,11)]
    parameter NB_IN_B  = 12,               //Bits totales
    parameter NBF_IN_B = 11,               //Bits fraccionales
    parameter NBI_IN_B = NB_IN_B-NBF_IN_B, //Bits enteros = Totales - Fraccionales

    // Parámetros de salida Full resolution

    parameter NB_OUT_FR = ( NB_IN_A  > NB_IN_B  )? NB_IN_A + 1 : NB_IN_B + 1 ,  //1 bit más que el mayor
    parameter NBF_OUT_FR = ( NBF_IN_A  > NBF_IN_B  )? NBF_IN_A : NB_IN_B ,      //Igual que el mayor
    parameter NBI_OUT_FR = NB_OUT_FR-NBF_OUT_FR,                                //Bits enteros = Totales - Fraccionales

    // Parámetros de salida S(11,10)

    parameter NB_OUT_S_11_10 = 11,
    parameter NBF_OUT_S_11_10 = 10,
    parameter NBI_OUT_S_11_10 = NB_OUT_S_11_10-NBF_OUT_S_11_10,

    // Parámetros de salida S(9,8)

    parameter NB_OUT_S_9_8 = 9,
    parameter NBF_OUT_S_9_8 = 8,
    parameter NBI_OUT_S_9_8 = NB_OUT_S_9_8-NBF_OUT_S_9_8

)
(
    //Entradas
    input [NB_IN_A-1:0] i_A,
    input [NB_IN_B-1:0] i_B,
    //Salida
    output [NB_OUT_FR-1:0]      o_C_FR,     //Salida full resolution
    output [NB_OUT_S_11_10-1:0] o_C_OF,     //Salida S(11,10) con overflow y truncado
    output [NB_OUT_S_11_10-1:0] o_C_SAT,    //Salida S(11,10) con saturación y truncado
    output [NB_OUT_S_9_8-1:0]   o_C_RED    //Salida S(9,8) con saturación y redondeo
);

// Variables signadas para considerar S (por defecto Verilog las hace sin signo)

wire signed [NB_IN_A-1:0] A;
wire signed [NB_IN_B-1:0] B;

reg signed [NB_OUT_FR-1:0]      C_FR;          
reg signed [NB_OUT_S_11_10-1:0] C_OF;    
reg signed [NB_OUT_S_11_10-1:0] C_SAT;    
reg signed [NB_OUT_S_9_8-1:0]   C_RED;

assign A = i_A;
assign B = i_B;

localparam NB_RED = (1+NBI_OUT_FR + NBF_OUT_S_9_8 + 1); //Variable auxiliar usada en redondeo

reg signed [NB_RED-1:0] RED;

always @(*) begin
    //Full resolution
    C_FR = A + $signed({B,{NBF_IN_A-NBF_IN_B {1'b0}}}); //Agrega los ceros necesarios a B y suma. 
                                                        //Esto es porque S(16,14) [A] tiene más decimales que S(12,11) [B]

    //Overflow Truncado 
    C_OF = C_FR [(NB_OUT_FR -1) - (NBI_OUT_FR - NBI_OUT_S_11_10) -: NB_OUT_S_11_10];    

    //Saturación Truncado 
    if ( &C_FR[(NB_OUT_FR -1) -: (NBI_OUT_FR - NBI_OUT_S_11_10) + 1] || ~|C_FR[(NB_OUT_FR -1) -: (NBI_OUT_FR - NBI_OUT_S_11_10) + 1] )    
        C_SAT = C_FR[ (NB_OUT_FR -1) - (NBI_OUT_FR - NBI_OUT_S_11_10) -: NB_OUT_S_11_10 ];
    else if ( C_FR[(NB_OUT_FR -1)] )
        C_SAT = { 1'b1 , { NB_OUT_S_11_10-1 {1'b0} } } ;
    else
        C_SAT = { 1'b0 , { NB_OUT_S_11_10-1 {1'b1} } } ;

    //Saturación Redondeo 
    RED = $signed(C_FR[(NB_OUT_FR -1) -: (NB_RED -1)]) + $signed(2'b01);
        
    if ( &RED[(NB_RED -1) -: (NBI_OUT_FR - NBI_OUT_S_9_8) + 2] || ~|RED[(NB_RED -1) -: (NBI_OUT_FR - NBI_OUT_S_9_8) + 2] )
        C_RED = RED[ (NB_RED -1) - (NBI_OUT_FR - NBI_OUT_S_9_8) -1 -: NB_OUT_S_9_8 ];
    else if ( RED[(NB_RED -1)] )
        C_RED = { 1'b1 , { NB_OUT_S_9_8-1 {1'b0} } } ;
    else
        C_RED = { 1'b0 , { NB_OUT_S_9_8-1 {1'b1} } } ;

end

//Asignación de puertos
assign o_C_FR  = C_FR; 
assign o_C_OF  = C_OF; 
assign o_C_SAT = C_SAT;
assign o_C_RED = C_RED;

endmodule

    