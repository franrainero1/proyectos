// TP4 Diseño Digital -TP4- Ejercicio 2
// Módulo Producto en punto fijo
// Francisco Gabriel Rainero. 
// franrainero1@gmail.com

module Prod_Punto_Fijo #(

    // Parámetros de entrada 1 [S(8,6)]
    parameter NB_IN_A  = 8,                 //Bits totales
    parameter NBF_IN_A = 6,                 //Bits fraccionales
    parameter NBI_IN_A = NB_IN_A-NBF_IN_A, //Bits enteros = Totales - Fraccionales

    // Parámetros de entrada 2 [S(12,11)]
    parameter NB_IN_B  = 12,               //Bits totales
    parameter NBF_IN_B = 11,               //Bits fraccionales
    parameter NBI_IN_B = NB_IN_B-NBF_IN_B, //Bits enteros = Totales - Fraccionales

    // Parámetros de salida Full resolution

    parameter NB_OUT_FR =  NB_IN_A  + NB_IN_B ,
    parameter NBF_OUT_FR = NBF_IN_A + NBF_IN_B ,
    parameter NBI_OUT_FR = NB_OUT_FR - NBF_OUT_FR,


    // Parámetros de salida S(12,11)

    parameter NB_OUT_S_12_11 = 12,
    parameter NBF_OUT_S_12_11 = 11,
    parameter NBI_OUT_S_12_11 = NB_OUT_S_12_11-NBF_OUT_S_12_11,

    // Parámetros de salida S(10,9)

    parameter NB_OUT_S_10_9 = 10,
    parameter NBF_OUT_S_10_9 = 9,
    parameter NBI_OUT_S_10_9 = NB_OUT_S_10_9-NBF_OUT_S_10_9

)
(
    //Entradas
    input [NB_IN_A-1:0] i_A,
    input [NB_IN_B-1:0] i_B,
    //Salida
    output [NB_OUT_FR-1:0]      o_C_FR,     //Salida full resolution
    output [NB_OUT_S_12_11-1:0] o_C_OF,     //Salida S(12,11) con overflow y truncado
    output [NB_OUT_S_12_11-1:0] o_C_SAT,    //Salida S(12,11) con saturación y truncado
    output [NB_OUT_S_10_9-1:0]  o_C_RED    //Salida S(10,9) con saturación y redondeo
);

// Variables signadas para considerar S (por defecto Verilog las hace sin signo)

wire signed [NB_IN_A-1:0] A;
wire signed [NB_IN_B-1:0] B;

wire signed [NB_OUT_FR-1:0]      C_FR;          
reg signed [NB_OUT_S_12_11-1:0] C_OF;    
reg signed [NB_OUT_S_12_11-1:0] C_SAT;    
reg signed [NB_OUT_S_10_9-1:0]  C_RED;

assign A = i_A;
assign B = i_B;

//Full resolution
assign C_FR = $signed(i_A) * $signed(i_B)  ;  

localparam NB_RED = NBI_OUT_FR + NBF_OUT_S_10_9 + 2; //Variable auxiliar usada en redondeo
reg signed [NB_RED-1:0] RED;

always @(*) begin
    
    //Overflow Truncado 
    C_OF = C_FR [(NB_OUT_FR -1) - (NBI_OUT_FR - NBI_OUT_S_12_11) -: NB_OUT_S_12_11];    

    //Saturación Truncado 
    if ( &C_FR[(NB_OUT_FR -1) -: (NBI_OUT_FR - NBI_OUT_S_12_11) + 1] || ~|C_FR[(NB_OUT_FR -1) -: (NBI_OUT_FR - NBI_OUT_S_12_11) + 1] )    
        C_SAT = C_FR[ (NB_OUT_FR -1) - (NBI_OUT_FR - NBI_OUT_S_12_11) -: NB_OUT_S_12_11 ];
    else if ( C_FR[(NB_OUT_FR -1)] )
        C_SAT = { 1'b1 , { NB_OUT_S_12_11-1 {1'b0} } } ;
    else
        C_SAT = { 1'b0 , { NB_OUT_S_12_11-1 {1'b1} } } ;

    //Saturación Redondeo 
    RED = $signed(C_FR[(NB_OUT_FR -1) -: (NB_RED -1)]) + $signed(2'b01);
        
    if ( &RED[(NB_RED -1) -: (NBI_OUT_FR - NBI_OUT_S_10_9) + 2] || ~|RED[(NB_RED -1) -: (NBI_OUT_FR - NBI_OUT_S_10_9) + 2] )
        C_RED = RED[ (NB_RED -1) - (NBI_OUT_FR - NBI_OUT_S_10_9) -1 -: NB_OUT_S_10_9 ];
    else if ( RED[(NB_RED -1)] )
        C_RED = { 1'b1 , { NB_OUT_S_10_9-1 {1'b0} } } ;
    else
        C_RED = { 1'b0 , { NB_OUT_S_10_9-1 {1'b1} } } ;


end

//Asignación de puertos
assign o_C_FR  = C_FR; 
assign o_C_OF  = C_OF; 
assign o_C_SAT = C_SAT;
assign o_C_RED = C_RED;

endmodule