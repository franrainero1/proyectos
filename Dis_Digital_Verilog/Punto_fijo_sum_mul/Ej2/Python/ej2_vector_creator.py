# TP4 Diseño Digital 
# Francisco Gabriel Rainero. 
# franrainero1@gmail.com 

######################################################################
#                               MODULES
######################################################################

import os
import numpy as np
import matplotlib.pyplot as plt

import tool._fixedInt as fxd
import tool.DSPtools as dsp

######################################################################
#                               FUNCTIONS
######################################################################

######################################################################
#                           formatForTestbenchFile()
#
# Description   : ...
# Arguments     : ...
# Returns       : ...
######################################################################

def formatForTestbenchFile(intValue, nBits, flip = False , separator = '\t', end = '\n'):
    '''
    Converts a number (int) to its binary representation (str) and
    formats it  for the  testbench. The input  integer  must be in 
    equivalent integer format.
    
    @type   intValue    : integer
    @param  intValue    : integer to convert to binary

    @type   nBits       : integer
    @param  nBits       : number of bits at the output

    @type   flip        : bool 
    @param  flip        : true to read from LSB to MSB in file

    @type   separator   : str
    @param  separator   : separator to add between bits

    @rtype              : str
    @return             : bin formatted to write to testbench file

    Example of positive number in fixed point:    

    >>> a = fixedInt(6,5)                    # fixed point format S(6,5)
    >>> a.value =  0.5                       # a = 0b10000
    >>> print(a.intValue)                    
        16                                   # int equivalent to convert
    >>> formatForTestbenchFile(a.intValue,6) # add format
        '1\t0\t0\t0\t0\t0'                   
 
    Example of negative number in fixed point:

    >>> a = fixedInt(6,5)                    # fixed point format S(6,5)
    >>> a.value = -0.5                       # a = 0b110000
    >>> print(a.intValue)                    
        48                                   # int equivalent to convert
    >>> formatForTestbenchFile(a.intValue,6) # add format
        '1\t1\t0\t0\t0\t0'
    '''
    # Chequea entrada
    if intValue < 0:
        raise (ValueError, 'The input integer must be positive')
    # Convierte int a bin (str)
    data = bin(intValue)[2:]
    # Extiende a nBits
    data = data.zfill(nBits)
    # Espeja
    if flip:
        data = data [::-1]
    # Agrega separador entre bits
    if separator != '':
        data = list(data)
        for i in range(1, 2*len(data)-1, 2):
            data.insert(i,separator)
        data = ''.join(data) 
    # Retorna intValue convertido a binario en string formateado
    return data + end

######################################################################
#                               MAIN
######################################################################

def main():
    ''' Main function '''

    ### ------------- Configurables -------------------------
    
    # Puntos de simulacion
    lenght         = 10000       # Cantidad de puntos de simulacion 

    # Punto fijo
    operation      = 'S'        # (S) suma : (P) producto

    # Sumando A  (entrada)
    A_type         = 'round'    # (trunc) truncado : (round) redondeo
    A_sign         = 'S'        # (U) no signado : (S) signado
    A_totalWidth   = 16          # Cantidad de bits totales
    A_fractWidth   = 14          # Cantidad de bits fraccionales
    A_waveform     = 'rand'     # (rand) aleatoria : (sin) seno : (cos) coseno 
    A_waveform_T   = 255        # Cantidad de muestras por periodo para sin y cos
    
    # Sumando B  (entrada)
    B_type         = 'round'    # (trunc) truncado : (round) redondeo
    B_sign         = 'S'        # (U) no signado : (S) signado
    B_totalWidth   = 12         # Cantidad de bits totales
    B_fractWidth   = 11         # Cantidad de bits fraccionales
    B_waveform     = 'rand'     # (rand) aleatoria : (sin) seno : (cos) coseno 
    B_waveform_T   = 255        # Cantidad de muestras por periodo para sin y cos

    # Resultado C (punto b)
    Cb_type         = 'trunc'   # (trunc) truncado : (round) redondeo
    Cb_sign         = 'S'       # (U) no signado : (S) signado
    Cb_totalWidth   = 11        # Cantidad de bits totales
    Cb_fractWidth   = 10        # Cantidad de bits fraccionales
    Cb_saturateMode = 'wrap'    # (wrap) overflow : (saturate) satura 

    # Resultado C (punto c)
    Cc_type         = 'trunc'   # (trunc) truncado : (round) redondeo
    Cc_sign         = 'S'       # (U) no signado : (S) signado
    Cc_totalWidth   = 11        # Cantidad de bits totales
    Cc_fractWidth   = 10        # Cantidad de bits fraccionales
    Cc_saturateMode = 'saturate'# (wrap) overflow : (saturate) satura 

    # Resultado C (punto d)
    Cd_type         = 'round'   # (trunc) truncado : (round) redondeo
    Cd_sign         = 'S'       # (U) no signado : (S) signado
    Cd_totalWidth   = 9        # Cantidad de bits totales
    Cd_fractWidth   = 8         # Cantidad de bits fraccionales
    Cd_saturateMode = 'saturate'# (wrap) overflow : (saturate) satura 
    ### -----------------------------------------------------

    # Selecciona directorio de archivos
    if operation == 'S':
        file_path = 'sum_vectors/'
    else:
        file_path = 'mul_vectors/'


    # Abre/Crea un archivo por cada señal
    fileA  = open(file_path + 'A.in'  ,'w')
    fileB  = open(file_path + 'B.in'  ,'w')
    fileCa = open(file_path + 'Ca_py.out','w')
    fileCb = open(file_path + 'Cb_py.out','w')
    fileCc = open(file_path + 'Cc_py.out','w')
    fileCd = open(file_path + 'Cd_py.out','w')

    # Variables
    A = fxd.DeFixedInt( roundMode    = A_type          ,
                        signedMode   = A_sign          ,
                        totalWidth   = A_totalWidth    ,
                        fractWidth   = A_fractWidth    ,
                        saturateMode = 'saturate'      )

    B = fxd.DeFixedInt( roundMode    = B_type          ,
                        signedMode   = B_sign          ,
                        totalWidth   = B_totalWidth    ,
                        fractWidth   = B_fractWidth    ,
                        saturateMode = 'saturate'      )

    Cb = fxd.DeFixedInt( roundMode   = Cb_type         ,
                        signedMode   = Cb_sign         ,
                        totalWidth   = Cb_totalWidth   ,
                        fractWidth   = Cb_fractWidth   ,
                        saturateMode = Cb_saturateMode )

    Cc = fxd.DeFixedInt( roundMode   = Cc_type         ,
                        signedMode   = Cc_sign         ,
                        totalWidth   = Cc_totalWidth   ,
                        fractWidth   = Cc_fractWidth   ,
                        saturateMode = Cc_saturateMode )

    Cd = fxd.DeFixedInt( roundMode   = Cd_type         ,
                        signedMode   = Cd_sign         ,
                        totalWidth   = Cd_totalWidth   ,
                        fractWidth   = Cd_fractWidth   ,
                        saturateMode = Cd_saturateMode )

    # Calcula rango de entradas
    A_range = A.showRange(returnList = True )
    B_range = B.showRange(returnList = True )
 
    # Indice para entradas seno y coseno
    A_index = 0
    B_index = 0

    for i in range(lenght):

        # GENERA ENTRADA A
        if A_waveform == 'rand':
            A.value = np.random.uniform(min(A_range),max(A_range))
        else:
            if A_index >= A_waveform_T:
                A_index = 0
            if A_waveform == 'sin':
                A.value = abs(min(A_range)) * np.sin(A_index * 2 * np.pi / A_waveform_T)
            else:
                A.value = abs(min(A_range)) * np.cos(A_index * 2 * np.pi / A_waveform_T)
            A_index += 1;
        
        # GENERA ENTRADA B
        if B_waveform == 'rand':
            B.value = np.random.uniform(min(B_range),max(B_range))
        else:
            if B_index >= B_waveform_T:
                B_index = 0
            if B_waveform == 'sin':
                B.value = abs(min(B_range)) * np.sin(B_index * 2 * np.pi / B_waveform_T)
            else:
                B.value = abs(min(B_range)) * np.cos(B_index * 2 * np.pi / B_waveform_T)
            B_index += 1;

        # Operacion
        if operation == 'S':
            Ca = A + B
        else:
            Ca = A * B

        Cb.value = Ca.fValue  
        Cc.value = Ca.fValue
        Cd.value = Ca.fValue

        fileA.write (formatForTestbenchFile( A.intvalue, A.width,separator=''))
        fileB.write (formatForTestbenchFile( B.intvalue, B.width,separator=''))
        fileCa.write(formatForTestbenchFile(Ca.intvalue,Ca.width,separator=''))
        fileCb.write(formatForTestbenchFile(Cb.intvalue,Cb.width,separator=''))
        fileCc.write(formatForTestbenchFile(Cc.intvalue,Cc.width,separator=''))
        fileCd.write(formatForTestbenchFile(Cd.intvalue,Cd.width,separator=''))

    # Cierra archivos
    fileA.close()
    fileB.close()
    fileCa.close()
    fileCb.close()
    fileCc.close()
    fileCd.close()

    print(f'Archivos exportados al directorio "{file_path}"')

######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    main()