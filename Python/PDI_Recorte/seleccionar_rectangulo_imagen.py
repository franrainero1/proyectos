# UTN-FRC. Ing. Electrónica.
# Práctico 4: Manipulación de imágenes
# Visión por computadora
# Francisco Gabriel Rainero. Legajo 71967.

import cv2                                                      #Importa módulo de OpenCV
import numpy as np                                              #Importa módulo numpy

blue = (255, 0, 0); green = (0, 255, 0); red = (0, 0, 255);     #Define colores
drawing = False                                                 #Bandera dibujando rectángulo
xybutton_inicio = -1, -1                                        #Punto x,y de inicio del rectángulo                           
xybutton_final = -1, -1                                         #Punto x,y final del rectángulo

img = cv2.imread('hojas.jpg')                                   #Leo imagen y la respaldo con una auxiliar
img_aux = cv2.imread('hojas.jpg')
H = len(img)                                                    #Leo altura (height)
W = len(img[0])                                                 #Leo ancho (width)

def dibuja_cuadrado (event, x, y, flags, param):                #Función que grafica el rectángulo
    global xybutton_inicio, drawing, img_aux, xybutton_final    #Variables globales para no perder el valor
    if event == cv2.EVENT_LBUTTONDOWN:                          #Click izquierdo abajo
        drawing = True                                          #Bandera de arranque
        xybutton_inicio = x, y                                  #Guardo ubicación de arranque
    elif event == cv2.EVENT_MOUSEMOVE:                          #Evento de movimiento del mouse
        if drawing is True:
            img[:] = img_aux[:]                                 #Esto borra las líneas no deseadas
            cv2.rectangle(img, xybutton_inicio, (x,y), red, 0)  #Grafico rectángulo
    elif event == cv2.EVENT_LBUTTONUP:                          #Click derecho arriba
        drawing = False                                         #Bandera de finalización
        xybutton_final = x, y                                   #Guardo ubicación de final

cv2.namedWindow('image')
cv2.setMouseCallback('image', dibuja_cuadrado)                  #Cuando hay un evento del mouse se llama
                                                                #a la función
while(1):                                      

    cv2.imshow('image', img)                                        
    k = cv2.waitKey(1) & 0xFF
    if k == ord('q'):                                           #Sale al presionar 'q'
        break
    if k == ord('r'):                                           #Con 'r' restauramos la imagen
        img = cv2.imread('hojas.jpg')           
    if k == ord('g'):                                           #Con 'g' guarda los cambios y sale
        img_output = np.zeros((H,W,3), np.uint8)
        img_output[:] = 255
        for i in range (xybutton_inicio[1],xybutton_final[1]+1):  #Asigno a output la parte seleccionada
            for j in range(xybutton_inicio[0],xybutton_final[0]+1):
                    img_output[i][j]=img[i][j]
        cv2.imwrite('output.jpg',img_output)
        break

cv2.destroyAllWindows()                                         #Cierra las ventanas