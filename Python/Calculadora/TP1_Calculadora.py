#!/usr/bin/env python
# coding: utf-8

#  ## Graficar en la misma página.

# In[1]:


# Grafica en la misma pagina, sino abre una ventana con qt5
#get_ipython().run_line_magic('matplotlib', 'inline')
#%matplotlib qt5


# ## Importamos librerias

# In[2]:


import numpy as np
import matplotlib.pyplot as pl

#define las dimensiones de las figuras por defecto (en pulgadas)
pl.rcParams['figure.figsize']= [10.0,6.0]


# ## 1) Suma

# In[3]:


def suma(a=0,b=0):
    return a+b


# ## 2) Resta

# In[4]:


def resta(a=0,b=0):
    return a-b


# ## 3) Producto

# In[5]:


def producto(a=0,b=0):
    return a*b


# ## 4) División

# In[6]:


def cociente(a=0,b=0):
    return a/b


# ## 5) Iteración

# In[7]:


def iterar(a,b):
    c = [1,0,0]
    while b!=0:
        c[2] = a + c[2]
        c[1] = c[1] - a
        c[0] = a*c[0]
        b = b -1
    return c


# ## 6) Producto punto

# In[8]:


def cargar_matriz():
    f = int(input("N° de Filas   : "))
    c = int(input("N° de Columnas: "))
    
    array = np.zeros((f,c))
    for i in range (f):
        for j in range (c):
            array[i,j] = int(input('-Elemento [{},{}]:'.format(i,j)))
    return array

def producto_punto():
    while True:
        print("Primera matriz/vector:")
        A = cargar_matriz()
        np.savetxt('matrizA.log', A, delimiter = ',')
        print("\nSegunda matriz/vector:")
        B = cargar_matriz()
        np.savetxt('matrizB.log', B, delimiter = ',')
        if( np.size(A, 0) == np.size(B, 0) and np.size(A, 1) == np.size(B, 1)):
            np.savetxt('A_pp_B.log', np.dot(A, B), delimiter = ',')
            return np.dot(A, B)
            break
        else:
            print("Dimensiones incompatibles, ingrese nuevamente")


# ## 7) Cargar arreglo

# In[9]:


def cargar_vector():
    N = int(input("N° de elementos: "))
    
    array = np.zeros(N)
    for i in range (N):
        array[i] = int(input('-Elemento [{}]:'.format(i)))
    return array


# ## 7) a) Plot

# In[10]:


def ploteo(v):
    N = np.shape(v)
    n  = np.arange(0.,N[0],1)
    pl.plot(n,v,linewidth=1.0,label='Plot del vector v')
    pl.ylabel('Amplitud')
    pl.xlabel('N° de muestra')
    pl.title('Vector ingresado')
    pl.legend()
    pl.grid()
    pl.show()


# ## 7) b) Stem

# In[11]:


def stemeo(v):
    N = np.shape(v)
    n  = np.arange(0.,N[0],1)
    pl.stem(n,v,label='Plot del vector v')
    pl.ylabel('Amplitud')
    pl.xlabel('N° de muestra')
    pl.title('Vector ingresado')
    pl.legend()
    pl.grid()
    pl.show()


# ## 7) c) Histograma

# In[12]:


def histograma(v):
    
    x_min = np.amin(v)
    x_max = np.amax(v)
    
    pl.figure()
    pl.hist(v,20,label='Histograma del vector v')
    pl.legend()
    pl.xlim(x_min-1,x_max+1)
    #pl.ylim(0,10000)
    pl.grid()
    pl.show()


# ## 7) d) FFT

# In[13]:


def fft(v):
    V = np.fft.fft(v,1023)
    N = np.shape(V)
    w  = np.arange(0.,6.277049384,0.006135923152)
    pl.plot(w,np.abs(V),linewidth=1.0,label='Módulo V(e^jw)')
    pl.plot(w,np.angle(V),'-r',linewidth=1.0,label='Fase V(e^jw)')
    pl.ylabel('Amplitud')
    pl.xlabel('w')
    pl.title('fft() de vector ingresado')
    pl.legend()
    pl.grid()
    pl.show()


# ## 7) e) File Plot

# In[14]:


def file_plot(file):
    data = np.genfromtxt(file, dtype = int, delimiter = ",")
    pl.plot(np.arange(np.size(data)), data,'o-', linewidth=1.0)
    pl.ylabel('Amplitud')
    pl.xlabel('Muestras')
    pl.title('Plot del archivo cargado')
    pl.grid()
    pl.show()


# ## Menú Calculadora

# In[ ]:


print("Bienvenido a la calculadora 3000.")
print("Elija una opción para continuar:")
print("0) Salir")
print("1) Sumar")
print("2) Restar")
print("3) Multiplicar")
print("4) Dividir")
print("5) Iterativo: suma, resta y multiplicación.")
print("6) Producto punto")
print("7) Graficar")
    
while True:
     
    op = int(input("\nIngrese una opción:"))
    
    if op == 0:
        print("Gracias, vuelva pronto.")
        break
        
    elif op == 1:
        print("Usted va a sumar")
        a = float(input("Ingrese el primer sumando: "))
        b = float(input("Ingrese el segundo sumando: "))
        print("El resultado es ", suma(a,b))
        
    elif op == 2:
        print("Usted va a restar")
        a = float(input("Ingrese el minuendo: "))
        b = float(input("Ingrese el sustraendo: "))
        print("El resultado es ", resta(a,b))
        
    elif op == 3:
        print("Usted va a multiplicar")
        a = float(input("Ingrese el primer factor: "))
        b = float(input("Ingrese el segundo factor: "))
        print("El resultado es ", producto(a,b))
        
    elif op == 4:
        print("Usted va a dividir")
        a = float(input("Ingrese el dividendo: "))
        b = float(input("Ingrese el dividor: "))
        if b == 0:
            print("División no válida")
        else:
            print("El resultado es ", cociente(a,b))
            
    elif op == 5:
        print("Usted va a iterar")
        a = float(input("Ingrese el paso: "))
        b = float(input("Ingrese las veces: "))
        c = iterar(a,b)
        print("a)Suma: ",c[2],"b)Resta: ",c[1],"c)Producto",c[0])
        
    elif op == 6:
        prod_punt = producto_punto()
        print("El resultado es: ")
        for i in prod_punt:
            print(i)
            
    elif op == 7:
        print("Ingrese el tipo de gráfico que quiere implementar:")
        print("a) Plot")
        print("b) Stem")
        print("c) Histograma")
        print("d) FFT plot")
        print("e) File plot")
        
        gr = input("\nTipo de gráfico:")
        
        if gr == 'a':
            v = cargar_vector()
            ploteo(v)
            print("Graficado")
        elif gr == 'b':
            v = cargar_vector()
            stemeo(v)
            print("Graficado")
        elif gr == 'c':
            v = cargar_vector()
            histograma(v)
            print("Graficado")
        elif gr == 'd':
            v = cargar_vector()
            fft(v)
            print("Graficado")
        elif gr == 'e':
            print("Ingrese la ruta del archivo a graficar (Ej: sine.log):")
            file = input('File: ')
            file_plot(file)
            print("Graficado")
            
        


# In[ ]:




