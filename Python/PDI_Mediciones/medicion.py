# UTN-FRC. Ing. Electrónica.
# Práctico 9: Medición de objetos
# Visión por computadora
# Francisco Gabriel Rainero. Legajo 71967.

import cv2
import numpy as np
import math

blue = (255, 0, 0); green = (0, 255, 0); red = (0, 0, 255); yellow = (0, 255, 255);    #Define colores

puntos = 4                                              #Variable que controla los puntos a tomar de image

drawing = False
ix, iy = 0, 0

def homografia(image, origen, destino):                 #Mod Homografia: recibe 4 pares de puntos.
    (al, an) = image.shape[:2]

    M = cv2.getPerspectiveTransform(origen, destino)    #Matriz homografia

    img_out = cv2.warpPerspective(image, M, (an,al))    #Transformacion homográfica: mapea 4 puntos en puntos
                                                        #Mapea rectas en rectas 
    return img_out

def dibuja_punto (event, x, y, flags, param):           #Función que grafica el rectángulo
    global x1, y1, x2, y2, x3, y3, x4, y4, puntos       #Variables globales para no perder el valor
    if event == cv2.EVENT_LBUTTONDOWN:                  #Click izquierdo abajo
        if puntos == 4:
            x1, y1 = x, y                               #Guardo el punto 1
            cv2.circle(image, (x,y),2,red,-1)           #Grafico un círculo rojo en punto 1
            puntos = 3
        elif puntos == 3:
            x2, y2 = x, y                               #Guardo el punto 2
            cv2.circle(image, (x,y),2,blue,-1)          #Grafico un círculo azul en punto 2 
            puntos = 2
        elif puntos == 2:
            x3, y3 = x, y                               #Guardo el punto 3
            cv2.circle(image, (x,y),2,green,-1)         #Grafico un círculo verde en punto 3
            puntos = 1
        elif puntos == 1:
            x4, y4 = x, y                               #Guardo el punto 3
            cv2.circle(image, (x,y),2,yellow,-1)        #Grafico un círculo amarillo en punto 4
            puntos = 0

def medicion(event, x, y, flags, param):                #Medición e impresión de mediciones

    global ix, iy, drawing, img_rect, img_rec_copia, img_rect_medida
    
    if event == cv2 .EVENT_LBUTTONDOWN:                 #Pulsando botón izq empiezo a medir
        ix, iy = x, y
        drawing = True

    elif event == cv2.EVENT_MOUSEMOVE:                  #Grafica a medida que se mueve el mouse
        if drawing is True:
            img_rect[:] = img_rect_medida[:]                                    
            cv2.line(img_rect, (ix, iy), (x, y), yellow, 2)
            medida = (np.sqrt((ix-x)**2+(iy-y)**2))/100        #Cálculo de longitud en metros (Distancia entre dos puntos)
            medida = medida/2                                  #Debo dividir por dos porque dupliqué el tamaño de imagen                              
            x_y_text = int((ix+x)/2+10), int((iy+y)/2-10)      #Coordenadas del texto                                
            cv2.putText(img_rect, "{:.2f} m" .format(medida), (x_y_text), cv2.FONT_HERSHEY_PLAIN, 1, red, 1) #Imprime

    elif event == cv2.EVENT_LBUTTONUP:                  #Termina de graficar soltando botón izquierdo
        img_rect_medida[:] = img_rect[:]
        drawing = False

image = cv2.imread('fachada.jpg')                       #Imagen a rectificar
(h,w) = (438, 488)                                      #2*Tamaño de la puerta en cm(El doble para crear una imagen mas grande)
                                                        #Cada pixel equivaldrá 2cm
mostrar_rectificada = False                             #Bandera para mostrar rectificada

while True:

    cv2.imshow('Presione h para seleccionar la puerta y luego g', image) #Muestro imagen original

    if mostrar_rectificada:
        cv2.imshow('Rectificada. Realice sus mediciones', img_rect)      #Muestro imagen rectificada si la bandera es True
    
    k = cv2.waitKey(1) & 0xFF

    if k == ord('q'):                                   #Sale al presionar 'q'
        break

    if k == ord('h'):                                   #Selecciona cuatro puntos con 'h'

        cv2.namedWindow('Presione h para seleccionar la puerta y luego g')
        cv2.setMouseCallback('Presione h para seleccionar la puerta y luego g', dibuja_punto)   #Cuando hay un evento del mouse se llama a f
    
    if k == ord('g'):                                   #Guardo al presionar 'g'
        
        destino = np.float32([[x1,y1],[x1+w,y1],[x1,y1+h],[x1+w,y1+h]])    #4 vertices con offset para ver toda la imagen
        origen = np.float32([[x1,y1],[x2,y2],[x3,y3],[x4,y4]])             #Puntos seleccionados 
        
        img_rect = homografia(image,origen,destino)         #Transformación homográfica de la imagen y dos copias
        img_rect_copia = img_rect.copy()
        img_rect_medida = img_rect.copy()
        cv2.imwrite('Imagen_rectificada.jpg',img_rect)      #Guardo la imagen rectificada
        
        cv2.namedWindow('Rectificada. Realice sus mediciones')
        cv2.setMouseCallback('Rectificada. Realice sus mediciones',medicion)     #Cuando hay un evento del mouse llama a esta función
        mostrar_rectificada = True

    if k == ord('r'):                                       #Con r restauramos la medición
        img_rect = img_rect_copia.copy()                        
        img_rect_medida = img_rect.copy()
        
cv2.imwrite('Imagen_con_mediciones.jpg',img_rect)          #Guardo la imagen rectificada

cv2.destroyAllWindows()                                 #Cierra las ventanas