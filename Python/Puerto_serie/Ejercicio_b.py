import time
import serial
import os
import numpy as np
import matplotlib.pyplot as pl

def ploteo(v):
    N = np.shape(v)
    n  = np.arange(0.,N[0],1)
    pl.plot(n,v,linewidth=1.0,label='Plot del vector v')
    pl.ylabel('Amplitud')
    pl.xlabel('N° de muestra')
    pl.title('Vector random')
    pl.legend()
    pl.grid()
    pl.show()

def encode_trama_corta(data): #0xA + size + 0x00 + 0x00 + Device (0x01) + Data + 0x4 + size
    
     ## Estructura de la trama

    data = data.encode()    #Pasa a byte

    long = len(data)        #Calcula longitud

    # Byte 1
    frame_head   = int('0xA0',base = 16) # 10100000
    frame_ssize  = int(long)             # longitud  

    # Byte 2
    frame_sizeh  = int('0x00',base = 16) # 00000000

    # Byte 3
    frame_sizel  = int('0x00',base = 16) # 00000000

    # Byte 4
    frame_device = int('0x01',base = 16) # 00000001

    # Byte n (último)
    frame_tail   = int('0x40',base=16)   # 01000000

    # Construye la cabecera
    head = []
    head.append(frame_head | frame_ssize)
    head.append(frame_sizeh)
    head.append(frame_sizel)
    head.append(frame_device)

    head = bytes(list(head))

    # Agrega la cabecera
    data = head + data
    
    # Agrega el tail
    data = data + bytes([frame_tail | long])

    # Devuelve trama completa
    return data
    

def encode_trama_larga(data): #0xB0 + size + Device (0x01) + Data + 0x50
    ## Estructura de la trama

    data = data.encode()    #Pasa a byte

    long = len(data)        #Calcula longitud

    # Byte 1
    frame_head   = int('0xB0',base = 16) 
             
    # Byte 2 y 3

    frame_lsizeh  = int(0xFF00 & long) >> 8
    frame_lsizel  = int(0x00FF & long) 

    # Byte 4
    frame_device = int('0x01',base = 16) # 00000001

    # Byte n (último)
    frame_tail   = int('0x50',base=16)   

    # Construye la cabecera
    head = []
    head.append(frame_head)
    head.append(frame_lsizeh)
    head.append(frame_lsizel)
    head.append(frame_device)

    head = bytes(list(head))

    # Agrega la cabecera
    data = head + data
    
    # Agrega el tail
    data = data + bytes([frame_tail])

    # Devuelve trama completa
    return data

# Aquí comienza el programa:

ser = serial.serial_for_url('loop://', timeout=1)   # Puentea RX con TX

ser.isOpen()
ser.timeout=None
ser.flushInput()
ser.flushOutput()
print(ser.timeout)

while True :

#Transimisión:

    print('\n>> Ingrese una opción y presione Enter:\n')
    print('"Calculadora", "Graficar" o "Salir"\n')
    data = input("Opción: ")

    if data == 'Calculadora' or data == 'Salir':
        data = encode_trama_corta(data)
        ser.write(data)            #Transmite data

    elif data == 'Graficar':
        v = str(np.random.randint(0,10,size = 10, dtype=int))
        data = data + v
        data = encode_trama_larga(data)
        ser.write(data)            #Transmite data
    
    else: 
        print("Opción incorrecta")

#Recepción:

    trama_rx = bytes(0)

    while ser.inWaiting() > 0:
        read_data = ser.read(1)
        trama_rx += read_data       #Recibe datos del puerto en out

    print("Bytes recibidos: {}" .format(trama_rx))

    if bytes(trama_rx[4:15] == bytes('Calculadora', 'utf-8')):
        os.system('py Calculadora.py')

    if bytes(trama_rx[4:9] == bytes('Salir', 'utf-8')):
        if ser.isOpen():
            ser.close()
        print("Saliendo...")
        break

    if bytes(trama_rx[4:12] == bytes('Graficar', 'utf-8')):
        vector = trama_rx[12:33].decode()
        v = []
        for i in range(10):
            v.append(int(vector[1+2*i]))
        ploteo(v)
