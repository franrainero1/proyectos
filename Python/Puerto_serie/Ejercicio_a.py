import time
import serial
import os
import numpy as np
import matplotlib.pyplot as pl

def ploteo():
    v = np.random.rand(10)
    N = np.shape(v)
    n  = np.arange(0.,N[0],1)
    pl.plot(n,v,linewidth=1.0,label='Plot del vector v')
    pl.ylabel('Amplitud')
    pl.xlabel('N° de muestra')
    pl.title('Vector random')
    pl.legend()
    pl.grid()
    pl.show()

ser = serial.serial_for_url('loop://', timeout=1)   # Puentea RX con TX

ser.isOpen()
ser.timeout=None
ser.flushInput()
ser.flushOutput()
print(ser.timeout)

while True :

    print('\n>> Ingrese una opción y presione Enter:\n')
    print('"Calculadora", "Graficar" o "Salir"\n')
    data = input("Opción: ")

    ser.write(data.encode())            #Transmite data
    out = ''

    while ser.inWaiting() > 0:
        read_data = ser.read(1)
        out += read_data.decode()       #Recibe datos del puerto en out

    if out != '':

        if (out == 'Salir'): 
            if ser.isOpen():
                ser.close()
            print(">> " + out)
            break

        elif (out == 'Calculadora'):
            print(">> " + out)
            os.system('py Calculadora.py')

        elif (out == 'Graficar'):
            ploteo()
            print(">> " + out)

        else: 
            print("Opción no válida, ingrese nuevamente")


    