% Filtro de Kalman aplicado a un balanc�n
clear
clc
close all
%% Modelado
% x(k+1) = F * x(k)+ G * u(k)  + L*w(k)
% z(k) = H * x(k)   + v(k) 

ts = 0.01312;                   % Tiempo entre muestras
N  = 50000;                     % Numero de muestras
x  = zeros(2,N);                % Inicializacion de estados
u  = zeros(1,N);                % Inicializacion del vector de entrada
z  = zeros(1,N);                % Inicializacion de salidas

%Matrices:
F = [1 -ts;0 1];
G = [ts;0];
L = ts;
H = [1 0]; 

%% Matrices de covarianza Q y R
Q = ts^2 * [8.75 0;0 1];           %Covariancia de proceso
R = 398.62;                        %Covariancia de medici�n

%% Generaci�n de ruidos
% Ruido de proceso:
Var_PNoise = .1;                   % varianza del ruido del proceso
Mu_PNoise = 0;                     % media del ruido del proceso
Std_PNoise = sqrt(Var_PNoise)';    % desviacion standar del ruido
PNoise = Std_PNoise * randn(2,N) + Mu_PNoise*ones(2,N);

% Ruido de medici�n:
Var_ONoise = 2;                    % varianza del ruido de medicion
Mu_ONoise = 0;                     % media del ruido de medicion
Std_ONoise = sqrt(Var_ONoise)';    % desviacion standar del ruido
ONoise = Std_ONoise * randn(1,N) + Mu_ONoise*ones(1,N);

%% Valores iniciales para el modelo
x(:,1) = [0 0]';                            %Estado inicial nulo
z(1) = H * x(:,1) + ONoise(:,1);            %Observacion inicial

%% Transicion de estados
for i = 2 : N
    x(:,i) = F * x(:,i-1) + G*u(:,i-1) + L*PNoise(:,i-1);   %Estados
    z(:,i-1) = H * x(:,i-1)  + ONoise(:,i-1);               %Observadores reales
end

%% Filtro de Kalman
xh(:,1) = ts*randn(2,1);                    %Estado inicial
Px = eye(2);                                %Matriz de covariancia inicial

for i = 2 : size(z,2)
    %---------- Prediccion ----------
    xh_(:,i) = F * xh(:,i-1) + G*u(1,i-1);  %Estimacion de los estados
    Px_ = F*Px*F' + Q;                      %Estimacion de la matriz de covarianza
   
    %---------- Correccion ----------
    K = Px_ * H' * inv(H*Px_*H' + R);       %Cte de filtro de Kalman
    zh_(:,i) = H * xh_(:,i) + R;            %Estimacion del observador
    resid(:,i) = z(:,i) - zh_(:,i);         %Error residual de medici�n
    xh(:,i+1) = xh_(:,i) + K * resid(:,i);  %Estimacion de los estados actualizados
    Px = Px_ - K*H*Px_;                     %Estimacion de la matriz de covarianza actualizada
   
end
   
%% Gr�fica observacion Real VS Observacion Estimada   
figure 
plot(z,'-r','LineWidth', 2)
hold on
plot(zh_,'--k', 'LineWidth', 2)
grid on
legend({'Posicion angular real','Posicion angular de Kalman'},'interpreter','latex')  
title('Implementacion de Kalman','interpreter','latex');
xlabel('Numero de muestra','interpreter','latex');
ylabel('Posicion angular','interpreter','latex');
ylim([-200,100])
set(gcf, 'Position', [100 100 600 600],'Color', 'w');
saveas(gcf,'Kalman_simu.svg','svg');

%Zoom
figure 
plot(z,'-r','LineWidth', 2)
hold on
plot(zh_,'--k', 'LineWidth', 2)
grid on
legend({'Posicion angular real','Posicion angular de Kalman'},'interpreter','latex')  
title('Zoom Implementacion de Kalman','interpreter','latex');
xlabel('Numero de muestra','interpreter','latex');
ylabel('Posicion angular','interpreter','latex');
xlim([1e4,1.5e4])
set(gcf, 'Position', [100 100 600 600],'Color', 'w');
saveas(gcf,'zoom_Kalman_simu.svg','svg');