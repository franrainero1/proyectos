clear; close all; clc;

%% Parametros de simulacion.
J = 100e-3;      % Momento de inercia
T = 10;          % Tiempo de simulacion
dt = 0.0001;     % Intervalo de muestreo
N = T/dt;        % Indice maximo para estados discretos
ts = 0:dt:T-dt;  % Vector de tiempos discretos

%% Vector de estado inicial.
% q1 = theta; q2 = theta_punto = omega
q0 = [0; 0];

%% Vector de estados e inicializacion.
q = zeros(2, N);
q(:, 1) = q0;

%% Vector de acciones de control. (Lazo abierto)
u = zeros(1, N);
%u = ones(1, N);

%% Matrices del sistema de estados discretizado.
A = [1, dt; 0, 1];
B = [0; dt/J];

%% Bucle para calculo de los estados. 
%Comentar para lazo cerrado
%for i = 1 : (N-1)
%	q(:, i+1) = A*q(:, i) + B*u(:, i);
%end

%% Sistema de lazo cerrado
Kp = 1;                              %Ganancia proporcional
Td = 0.632;                          %Tiempo derivativo 
q_r = ones(2, N);                    %Escalon unitario en theta_r
q_r(1,:) = zeros(1, N);              %Escalon unitario en omega_r
PD = [Kp, Kp*Td];                    %Matriz controlador PD

fn = 0.0;                            %Factor ruido

for i = 1 : (N-1)
    e(:,i) = (q_r(:,i)-q(:,i));         
    u(:,i) = PD*e(:,i);         
    q(:, i+1) = A*q(:, i) + B*u(:, i);
    q(1,i+1) = q(1,i+1) + fn*(rand-0.5)*q(1,i+1);
end

%% Graficos
figure;
subplot(2, 1, 1); plot(ts, q(1, :),'-r', 'LineWidth', 2.5); 
grid on; xlabel('$t [s]$','interpreter','latex'); ylabel('$q_1 (\theta)$','interpreter','latex');
legend({'Posicion angular'},'Interpreter','latex')
title({'$Kp = 1$, $Td = 0.632$, $fn = 0.05$'},'Interpreter','latex')
subplot(2, 1, 2); plot(ts, q(2, :),'-b', 'LineWidth', 2.5); 
grid on; xlabel('$t [s]$','interpreter','latex'); ylabel('$q_2 (\omega)$','interpreter','latex');
legend({'Velocidad angular'},'Interpreter','latex')

set(gcf, 'Position', [100 100 600 600],'Color', 'w');
%saveas(gcf,'Noise_05.svg','svg');


