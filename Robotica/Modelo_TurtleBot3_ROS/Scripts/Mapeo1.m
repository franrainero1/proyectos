clc
clear 
close all

%Script que genera 3 gr�ficos de 3 barridos l�ser diferentes

A = load('scan.txt');                               %Leo el archivo en una matriz A                      

time = A(1:end,1);                                  %Vector tiempo
laser = A(1:end,2:end);                             %Matriz laser

for i = 1:100:300                                   %Tres valores distintos de tiempo (aleatorios)
    figure();
    polarplot(laser(i,(1:end)),'.r');               %Gr�fico polar color rojo
    x = sprintf("Barrido l�ser en t=%.2f seg",time(i));
    title(x);
end
