clc
clear 
close all

%Script que realiza y grafica el mapa del robot

scan = load('scan.txt');                          %Leo scan en una matriz A
odom = load('odom.txt');                          %Leo odom en una matriz B
time_scan = scan(1:end,1);                        %Vector tiempo scan
time_odom = odom(1:end,1);                        %Vector tiempo odom

step = length(time_odom)/length(time_scan);       %Paso entre muestras
step = round(step);                               %Redondeo a entero

odom_short = zeros(length(scan),length(odom(1,:)));                       
for i = 1:step:(length(time_odom)-3*step)         %Elimina muestras cada step
   odom_short(i,:) = odom(i,:);
end
odom_short(sum(abs(odom_short),2)==0,:)=[ ];      %Elimina las filas en 0
odom_short(:,1) = time_scan;

%Conversi�n de polar a rectangular

scan_x = zeros(length(scan(:,1)),length(scan(1,:))-1);
scan_y = zeros(length(scan(:,1)),length(scan(1,:))-1);

for i = 1:360
    for j = 2:length(scan(1,:))
       if scan(i,j) == Inf 
           scan_x(i,j-1)=Inf;
           scan_y(i,j-1)=Inf;
       else
           scan_x(i,j-1)=scan(i,j)*cos((j-1)*pi/180 + odom_short(i,4)) + odom_short(i,2);
           % scan_x = scan*cos(w*t + theta) + x_i
           scan_y(i,j-1)=scan(i,j)*sin((j-1)*pi/180 + odom_short(i,4)) + odom_short(i,3);
           % scan_x = scan*sen(w*t + theta) + x_i
       end
    end
end

%Gr�ficos
figure();
hold on;
grid on;
title("Mapa generado por el sensor l�ser.");
%Gr�fico del camino
plot(odom(:,2),odom(:,3),'--b');
%Gr�fico del inicio
plot(odom(1,2),odom(1,3),'ok');
%Gr�fico del final
plot(odom(end,2),odom(end,3),'>k');
%Gr�fico del mapa
for i = 1:60:length(scan)
    plot(scan_x(i,:),scan_y(i,:),'.r');
end
xlabel(" x [m] ");
ylabel(" y [m] ");
legend('Camino','Inicio','Final','Mapa');
