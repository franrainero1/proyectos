% Script graficador
% -----------------
% Este script lee los datos del txt y grafica las curvas correspondientes

clear;
clc;
close all;

%Abertura del archivo
fileID = fopen('log1.txt','r'); %Abre el archivo 'nombre.txt'; r->READ
formatSpec = '%f';              %Formato de los datos que se leer�n. %f para punto flotante
A = fscanf(fileID,formatSpec);  %Copia los datos a A en el formato especificado
                                %Se copian N datos (N/6 grupos de 6)
                                %En una sola columna y en orden (t,x,y,o,v,w)
fclose(fileID);                 %Cierra el archivo

%Indexado de arreglos
N = length(A);                  %Cantidad de datos del .txt
t = zeros(N/6,1);              %Arreglo para el tiempo
x = zeros(N/6,1);              %Arreglo para x
y = zeros(N/6,1);              %Arreglo para y
o = zeros(N/6,1);              %Arreglo para orientaci�n (theta)
v = zeros(N/6,1);              %Arreglo para v
w = zeros(N/6,1);              %Arreglo para w

c = 1;                          %Variable para seleccionar columna
j = 1;                          %Variable para indexar arreglos

for i = 1:N                     %Recorrido total 
    if c==1
        t(j) = A(i);
    end
    if c==2
        x(j) = A(i);
    end
    if c==3
        y(j) = A(i);
    end
    if c==4
        o(j) = A(i);
    end
    if c==5
        v(j) = A(i);
    end
    if c==6 
        w(j) = A(i);
        c = 0;
        j = j + 1;
    end 
    c = c + 1;
end

% Gr�ficas solicitadas

plot(x,y);              %Gr�fico del camino realizado
xlabel('x [m]');
ylabel('y [m]');
title('Camino realizado');
grid on;

figure();               %Gr�fico de pose (x,y,o vs t)
hold on;
plot(t,x);              
plot(t,y);              
plot(t,o);              
xlabel('t [seg]');
ylabel('Pose');
title('Pose con respecto al tiempo');
legend({'x [m]','y [m]','o [rad]'})
grid on;

figure();               %Gr�fico de velocidades
hold on;
plot(t,v);              
plot(t,w);              
xlabel('t [seg]');
ylabel('Velocidades');
title('Velocidades con respecto al tiempo');
legend({'v [m/s]','w [rad/s]'})
grid on;

