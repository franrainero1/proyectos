clc
clear 
close all

%Script que regula las frecuencias de muestreo de odom.txt y de scan.txt

scan = load('scan.txt');                          %Leo scan en una matriz A
odom = load('odom.txt');                          %Leo odom en una matriz B

time_scan = scan(1:end,1);                        %Vector tiempo scan
time_odom = odom(1:end,1);                        %Vector tiempo odom

%Como time_odom > time_scan, elimino muestras de odom para igualar tama�os

step = length(time_odom)/length(time_scan);       %Paso entre muestras
step = round(step);                               %Redondeo a entero

odom_short = zeros(length(scan(:,1)),length(odom(1,:)));                       
for i = 1:step:(length(time_odom)-3*step)         %Elimina muestras cada step
   odom_short(i,:) = odom(i,:);
end

odom_short(sum(abs(odom_short),2)==0,:)=[ ];      %Elimina las filas en 0

time_odom_short = odom_short(1:end,1);            %Vector tiempo odom_short

figure();
hold all;
plot(time_scan,'-r', 'LineWidth', 2.5);           %Gr�fico tiempo de scan
plot(time_odom_short,'--b', 'LineWidth', 2.5);    %Gr�fico tiempo de odom_short
plot(time_odom,'-k', 'LineWidth', 2.5);           %Gr�fico tiempo de odom
grid on;
xlabel('N� de muestra');
ylabel('Valor de tiempo');
legend(["Tiempo scan","Tiempo odom-short","Tiempo odom"]);
title("Distintas escalas de tiempo");
