// Fuente: read_write.c
#include <avr/io.h>

#define BAUD_RATE 0x0010					// Valor de UBRR0 para 57600 bps

int main(void){
	uint8_t car;

	UBRR0H  = (uint8_t)(BAUD_RATE >> 8);	// Configura BR (byte mas significativo)
	UBRR0L  = (uint8_t) BAUD_RATE; 			// Configura BR (byte menos significativo)
	UCSR0C  = _BV(UCSZ00) | _BV(UCSZ01);	// Define trama 8N1
	UCSR0B |= _BV(TXEN0);	 				// Habilita el transmisor
	UCSR0B |= _BV(RXEN0);	 				// Habilita el receptor

	while (1){
		while (!( UCSR0A & _BV(RXC0)));		// Espera recibir un dato
		car = UDR0; 						// Lee caracter recibido

		if( (car >='a') && (car <='z'))		// Convierte a mayuscula
			car -= 32;

		while(!(UCSR0A & _BV(UDRE0)));		// Espera bufer de transmision vacio
			UDR0 = car;						// Escribe el bufer de transmision
	}
 	return 0;
}
