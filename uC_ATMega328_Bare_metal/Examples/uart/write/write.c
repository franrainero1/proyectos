// Fuente: write.c

#include <avr/io.h>
#include <util/delay.h>

#define BAUD_RATE 0x0008					// Define macro con BR de 115200
#define DELAY_MS 500

int main(void){
	int i;
	uint8_t dato[] = "Hola mundo :D\n\r";		// Cadena a transmitir

	UBRR0H  = (uint8_t)(BAUD_RATE >> 8); 	// Configura BR (byte mas significativo)
	UBRR0L  = (uint8_t) BAUD_RATE;			// Configura BR (byte menos significativo)

	UCSR0C  = _BV(UCSZ00) | _BV(UCSZ01); 	// Define trama 8N1
	UCSR0B |= _BV(TXEN0); 					// Habilita el transmisor
	
	while(1){
		for(i=0; dato[i] != '\0'; i++){
			while(!(UCSR0A & _BV(UDRE0)));	// Espera bufer de transmision vacío
			UDR0 = dato[i];					// Escribe el bufer de transmisión
		}
		_delay_ms(DELAY_MS);
	}
	return 0;
}
