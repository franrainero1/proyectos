// eeprom_escritura.c
#include <avr/io.h>
#include <util/delay.h>

int main(void){

	int adress = 0;			// Dirección de memoria a escribir/leer
	char data = 'f';		// Caracter a escribir

	//Escritura en EEPROM
	while(EECR & _BV(EEPE));	// Verifica que EEPE esté en 0
	EEAR = adress;			// Dirección de memoria a escribir
	EEDR = data;			// Dato a escribir
	EECR |= _BV(EEMPE);		// Pone en alto EEMPE
	EECR |= _BV(EEPE);		// Pone en bajo EEPE

	while(1){
	}

	return 0;
}
