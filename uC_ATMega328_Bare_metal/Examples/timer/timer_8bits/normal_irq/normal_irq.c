// timer_normal.c
#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

#define CICLOS 12									// Ciclos para retardo de 200ms (aprox)

int main(void){	
	DDRC   |= _BV(DDC0);      						// Configura PC0 como salida	
	TIMSK0 |= _BV(TOIE0);							// Habilita interrupciones por desborde
	sei();											// Habilita interrupciones en general
    TCCR0B |= _BV(CS02) | _BV(CS00);				// Define prescaler de 1024

	while (1){
	}
}

ISR (TIMER0_OVF_vect){
	static uint8_t cont;							// Define contador de ciclos

	cli();											// Dehabilita interrupciones
	cont ++;
	if (cont == CICLOS){
		cont = 0;									// Reinicia el contador
		PINC |= _BV(PINC0);							// Toggle del bit PC0	
	}
	sei();											// Habilita interrupciones
}
