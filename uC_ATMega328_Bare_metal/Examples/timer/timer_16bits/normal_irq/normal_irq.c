// timer_normal.c
#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

#define CICLOS 2				// Ciclos para retardo de 8,3s (aprox)

int main(void){	
	DDRC   |= _BV(DDC0);      		// Configura PC0 como salida	
	TIMSK1 |= _BV(TOIE1);			// Habilita interrupciones por desborde
	sei();					// Habilita interrupciones en general
    TCCR1B |= _BV(CS12) | _BV(CS10);		// Define prescaler de 1024

	while (1){
	}
}

ISR (TIMER1_OVF_vect){
	static uint8_t cont;			// Define contador de ciclos

	cli();					// Dehabilita interrupciones
	cont ++;
	if (cont == CICLOS){
		cont = 0;			// Reinicia el contador
		PINC |= _BV(PINC0);		// Toggle del bit PC0	
	}
	sei();					// Habilita interrupciones
}
