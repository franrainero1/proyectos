// timer_CTC.c
#include <avr/io.h>

int main(void){

    /* Generador de señal cuadrada de 10Hz en PC0 */

	TCCR1A |= _BV(WGM13) | _BV(WGM12);	// Configura modo CTC (WGM[3:0]=12)

	ICR1H 	= 0x03; 			// Define TOP en ICR1 en 780 (p/ 10Hz)
	ICR1L   = 0x0C;

	TCCR1B |= _BV(CS12) | _BV(CS10);	// Configura prescaler de 1024

	DDRC |= _BV(DDC0);			//Define a PC0 como salida

	while (1){
		if(TCNT1==ICR1){		//Comparo cuentas con top
			PINC |= _BV(PINC0);	//Invierto PC0

			TCNT1H = 0x00;		//Reinicio contador
			TCNT1L = 0x00;		//Por alguna razón no se reinicia solo
		}
	}
}
