// Fuente: slave_uart.c
#include <avr/io.h>

#define BAUD_RATE 0X0067				//Valor de UBRR0 para 9600bps

int main(void){

	//Configuración de UART
	UBRR0H = (uint8_t)(BAUD_RATE >> 8);	//Configura BR (MSB)
	UBRR0L = (uint8_t) BAUD_RATE;		//Configura BR (LSB)
	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);	//Define trama 8N1
	UCSR0B |= _BV(TXEN0);			//Habilita transmisor

	//Configuración de SPI Slave
	DDRB |= _BV(DDB4); 			// Define MISO (PB4) como salida
	SPCR |= _BV(SPE);			// Habilita el SPI

	uint8_t dato;

	while(1){
		while(! bit_is_set(SPSR,SPIF));		 // Espera fin de recepcion (SPI)
		dato = SPDR;				 // Lee dato (SPI)
		while(!( UCSR0A & _BV(UDRE0)));		 //Espera bufer de transmision vacio (UART)
		UDR0 = dato;				 // Realiza la transmisión (UART)
		}
	return 0;
}


