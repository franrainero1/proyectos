// Fuente: master_uart.c
#include <avr/io.h>
#include <util/delay.h>

#define DELAY_MS 100
#define BAUD_RATE 0x0067			//Valor de UBRR0 para 9600 bps
 
int main(void){

	//Configuración UART
	UBRR0H = (uint8_t)(BAUD_RATE >> 8);	//Configura BR (MSB)
	UBRR0L = (uint8_t) BAUD_RATE;		//Configura BR (LSB)
	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);	//Define trama 8N1
	UCSR0B |= _BV(RXEN0);			//Habilita receptor

	// Configuración SPI Master
	DDRB  |= _BV(DDB3) | _BV(DDB5);  	//Define MOSI (PB3) y SCK (PB5) como salida
	DDRB  |= _BV(DDB2);					// Define SS (PB2) como salida
	//SPCR  |= _BV(SPR1) | _BV(SPR0);		//Define SCK = 125kHz
	SPCR  |= _BV(MSTR) | _BV(SPE);		//Define como MASTER y habilita el SPI

	PORTB &= ~_BV(PORTB2);			//Activa SS (logica negada) 

	while(1){
		while(!( UCSR0A & _BV(RXC0)));	//Espera dato en bufer de recepción UART
		SPDR = UDR0;			//Transmite en SPI lo recibido por UART
		while(! bit_is_set(SPSR,SPIF));	//Espera fin de recepción SPI	
	}
	return 0;
}


