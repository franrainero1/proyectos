// Fuente: master.c
#include <avr/io.h>
#include <util/delay.h>

#define DELAY_MS 100
 
int main(void){
	DDRB  |= _BV(DDB3) | _BV(DDB5);  	// Define MOSI (PB3) y SCK (PB5) como salida
	DDRB  |= _BV(DDB2);					// Define SS (PB2) como salida
	SPCR  |= _BV(SPR1) | _BV(SPR0);		// Define SCK = 125kHz
	SPCR  |= _BV(MSTR) | _BV(SPE);		// Define como MASTER y habilita el SPI
	
	PORTB &= ~_BV(PORTB2);				// Activa SS (logica negada) 

	while(1){
		SPDR = 'A';						// Transmite 'A'
		while(! bit_is_set(SPSR,SPIF));	// Espera fin de transmision
		_delay_ms(DELAY_MS);
		
		SPDR = 'B';						// Transmite 'B'
		while(! bit_is_set(SPSR,SPIF));	// Espera fin de recepcion
		_delay_ms(DELAY_MS);
	}
	return 0;
}


