// int1.c

#include <avr/interrupt.h>		// Tambien incluye io.h

int main(void){
	DDRC  |=  _BV(DDC0);		// Define PC0 como salida
	EICRA |=  _BV(ISC11);		// Configura evento = flanco descendente
	EIMSK |=  _BV(INT1);		// Habilita interrupción externa INT1   
	sei();						// Habilita interrupciones en general
	
	while(1){

	}
	return 0;
}

ISR (INT1_vect){
	PINC |= _BV(PINC0);			// Invierte el estado de PC0
}

