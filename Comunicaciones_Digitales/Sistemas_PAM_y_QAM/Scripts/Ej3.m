%% Ejercicio 3

clc
clear all
close all

% -------------------------------- Item a ---------------------------------

BR	= 32e9;     % Bd
L	= 100;      % Simulation Length
N	= 16;       % Oversampling rate
fs	= N*BR;     % Sampling rate to emulate analog domain
T	= 1/BR;     % Time interval between two consecutive symbols
Ts	= 1/fs;     % Time between 2 conseutive samples at Tx output

% Generacion de simbolos aleatorios (+1,-1)

x = 2*randi([0,1],L,1)-1;

% Upsampling to change sampling rate

xup = upsample(x,N);

% Filtro de interpolacion y pulse-shaping

h = rsdcos(BR, fs, .1, 501);    % h cumple la func de conformador espectral

yup = filter(h,1,xup);

% -- Creacion de ejes temporales --
t_symbols	= (0:L-1).*T ;              % Eje temporal de los simbolos
t_samples	= (0:length(yup)-1).*Ts ;	% Eje temporal de la salida interpolada
delay       = (length(h)-1)/2 ;         % Delay del filtro
extra_time	= delay*Ts ;                % Delay en cantidad de muestras
% ---------------------------------

LPLOT = 25*N; % Total symbols to plot

figure
plot(t_samples, yup,'-b', 'Linewidth',1)
hold all

stem(t_symbols+extra_time, x,'filled', '--r','Linewidth', 1)

ylabel('Amplitude', 'Interpreter', 'latex')
xlabel('Continuous Time [sec]', 'Interpreter', 'latex')
grid on
xlim([0, t_samples(LPLOT)])

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/3a.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------

% -------------------------------- Item b ---------------------------------

h = rootrsdcos(BR, fs, .1, 501) ; % h cumple la func de conformador espectral

yup = filter(h,1,xup);

figure
plot(t_samples, yup,'-k', 'Linewidth',1)
hold all

stem(t_symbols+extra_time, x, 'filled', '--r','Linewidth', 1)

ylabel('Amplitude', 'Interpreter', 'latex')
xlabel('Continuous Time [sec]', 'Interpreter', 'latex')
grid on
xlim([0, t_samples(LPLOT)])

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/3b.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------