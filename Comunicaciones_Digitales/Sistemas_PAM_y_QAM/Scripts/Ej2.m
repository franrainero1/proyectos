%% Ejercicio 2

clc
clear all
close all

BR      = 32e9; % Bd
N       = 8;    % Oversampling rate
fs      = N*BR; % Sampling rate
Ntaps	= 501;  % Desired number of taps

rolloff = 0.4 ; 

% -------------------------------- Item b ---------------------------------

h_rc	= rsdcos(BR, fs, rolloff, Ntaps) ;
h_rrc	= rootrsdcos(BR, fs, rolloff, Ntaps) ;
h_rrc	= h_rrc ./ h_rrc(round(Ntaps/2)+1) ;

% -- Creacion del eje temporal --
if mod(Ntaps, 2) == 1
	Ntaps = Ntaps + 1 ;
end
plot_time = 0:1:Ntaps ;  % Creo un eje temporal igual al de la funcion
% -------------------------------

figure('Name', 'Raised cosine and Root-raised cosine (Time)')
hold all
plot(plot_time, h_rc, '-r', 'Linewidth', 1.5, 'DisplayName', '$h_{rc}(t)$')
plot(plot_time, h_rrc, '-b', 'Linewidth', 1.5, 'DisplayName', '$h_{rrc}(t)$')
xlabel('Samples', 'Interpreter', 'latex')
ylabel('Amplitude', 'Interpreter', 'latex')
xlim([200 300]) ;
grid on
legend('Interpreter', 'latex')
set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/2b.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------

% -------------------------------- Item c ---------------------------------
NFFT = 8*1024 ;

H_RC	= abs(fft(h_rc, NFFT)) ;
H_RC	= H_RC ./ H_RC(1) ;
H_RRC	= abs(fft(h_rrc, NFFT)) ;
H_RRC	= H_RRC ./ H_RRC(1) ;

% -- Creacion del eje de freq --
freq_step	= 2*pi/NFFT ;
omega       = -pi:freq_step:pi-freq_step ;
% ------------------------------

figure('Name', 'Raised cosine (freq)')
plot(omega, fftshift(H_RC), '-r', 'Linewidth', 1.5, 'DisplayName', '$H_{rc}(\omega)$')
hold on
plot(omega, fftshift(H_RRC), '-b', 'Linewidth', 1.5, 'DisplayName', '$H_{rrc}(\omega)$')
xlabel('Discrete frequency [rad/sec]','Interpreter', 'latex')
ylabel('Amplitude','Interpreter', 'latex')
xlim([-1 1]) ;
grid on
legend('Interpreter', 'latex')
set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/2c.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------

% -------------------------------- Item d ---------------------------------

h_rrc	= rootrsdcos(BR, fs, rolloff, Ntaps) ;

hrrc_conv_hrrc = conv(h_rrc, h_rrc) ;

hrrc_conv_hrrc	= hrrc_conv_hrrc ./ hrrc_conv_hrrc(round(2*Ntaps/2)+1) ;

% -- Creacion del eje temporal --
plot_time2 = 0:1:2*Ntaps ;  % Por la convolucion cambia el nro de taps
% -------------------------------

figure('Name', 'Raised cosine and Root-raised cosine*Root-raised cosine (Time)')
plot(plot_time+Ntaps/2, h_rc, '-r', 'Linewidth', 1.5, 'DisplayName', '$h_{rc}(t)$')
hold on
plot(plot_time2, hrrc_conv_hrrc,'--b', 'Linewidth', 1.5, 'DisplayName', '$h_{rc}(t)\ast h_{rrc}(t)$')
xlabel('Samples', 'Interpreter', 'latex')
ylabel('Amplitude', 'Interpreter', 'latex')
xlim([450 550])
xticks(450:25:550)
grid on
legend('Interpreter', 'latex')
set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/2d.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------