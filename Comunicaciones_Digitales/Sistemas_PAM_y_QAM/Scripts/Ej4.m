%% Ejercicio 4

clc
clear all
close all

BR	= 32e9;     % Bd
L	= 10000;    % Simulation Length
N	= 16;       % Oversampling rate
fs	= N*BR;     % Sampling rate to emulate analog domain
T	= 1/BR;     % Time interval between two consecutive symbols
Ts	= 1/fs;     % Time between 2 conseutive samples at Tx output

% Generacion de simbolos aleatorios (+1,-1)

x = 2*randi([0,1],L,1)-1;

% Upsampling para cambiar la tasa de muestreo

xup = upsample(x,N);

% Filtro de interpolacion y pulse-shaping

h = rsdcos(BR, fs, .1, 501) ;    % h cumple la func de conformador espectral

yup = filter(h,1,xup) ;

% -------------------------------------------------------------------------

NFFT = 1024*10;
WELCH_OVERLAP = 0.*NFFT;

% PSD de los simbolos

[Pxx, fxx] = pwelch(xup, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Pxx = Pxx ./ Pxx(1) ;
Pxx_dB = 10*log10(Pxx) ;
Pxx_dB = Pxx_dB - Pxx_dB(1) ;

Pxx_dB = [Pxx_dB(1:(end));flip(Pxx_dB)] ;	% Flip para mostrar la parte de frecuencia negativa
Pxx_dB = fftshift(Pxx_dB) ;
fxx    = [fxx(1:(end));-flip(fxx)];
fxx    = fftshift(fxx) ;
fxx = fxx ./ 1e9 ;                          % Convierto a GHz

% PSD de la salida

[Pyy, fyy] = pwelch(yup, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Pyy = Pyy ./ Pyy(1) ;
Pyy_dB = 10*log10(Pyy) ;
Pyy_dB = Pyy_dB - Pyy_dB(1) ;

Pyy_dB = [Pyy_dB(1:(end));flip(Pyy_dB)] ; % Flip para mostrar la parte de frecuencia negativa
Pyy_dB = fftshift(Pyy_dB) ;
fyy    = [fyy(1:(end));-flip(fyy)];
fyy    = fftshift(fyy) ;
fyy = fyy ./ 1e9 ;                          % Convierto a GHz

% Respuesta en frecuencia del filtro

H = abs(fft(h, NFFT)) ;
H = H ./ H(1) ;
H = 20*log10(H) ;
H = fftshift(H) ;

% Plots

figure

plot(fxx, Pxx_dB, 'b', 'Linewidth', 1, 'DisplayName', '$a_k\ PSD$')
hold all
plot(fyy, Pyy_dB, 'r', 'Linewidth', 1, 'DisplayName', '$Filter\ Output\ PSD$')
plot(fyy(1:end-2), H(1:length(fyy(1:end-2))), '--k', 'Linewidth', 1, 'DisplayName', '$Filter\ Response\ H(\omega)$')

xlabel('$Frequency\ [GHz]$','Interpreter','latex')
ylabel('$PSD\ Magnitude\ [dB]$','Interpreter','latex')
xticks(-50:10:50)
xlim([-50 50])
ylim([-120 10])
grid on
rect = [0.25, 0.25, .25, .25];
legend('Interpreter','latex','Location','best')

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/4.svg', 'svg') ;


% CONCLUSIONES: 
% -------------------------------------------------------------------------