%% Ejercicio 8

clc
clear 
close all

BR	= 16e9 ;	% Bd
L	= 10000 ;	% Simulation Length
N	= 4 ;   	% Oversampling rate
fs	= N*BR ;	% Sampling rate to emulate analog domain
T	= 1/BR ;	% Time interval between two consecutive symbols
Ts	= 1/fs ;	% Time between 2 conseutive samples at Tx output

% Generacion de simbolos aleatorios (+1+j, -1+j, -1-j, +1-j)

x_i = 2*randi([0,1],L,1)-1 ;
x_q = 2*randi([0,1],L,1)-1 ; 
x_total = x_i + 1j*x_q ;

% Upsampling to change sampling rate

xup = upsample(x_total, N) ;

% Filtro de interpolacion y pulse-shaping
rolloff = 0.2 ;
h = rsdcos(BR, fs, rolloff, 500) ;
                                    
m = filter(h,1,xup) ;      % Se�al interpolada


% -- Modelo de un transceptor QAM 16 --

fc = (1+rolloff)*BR/2*1.5 ;          % Carrier frequency
t = [0:length(m)-1].' .* Ts ;
p = sqrt(2) .* exp(1j*2*pi*fc.*t) ;
s = m .* p ;                        

x = real(s) ;

r = x ;

i = imag(hilbert(r)) ;
delay = finddelay(i, r);
f = r + 1j*i ;

g = conj(p) .* f ./ 2;

T0 = 2;
gD = g(T0+1:N:end) ;

mD = m(T0+1:N:end) ;

%% -------------------------------- Item a --------------------------------
scatterplot(mD(500:end),1,0,'*r')
title('')
grid on

xlabel('$In-Phase$','interpreter','latex')
ylabel('$Quadrature$','interpreter','latex')

legend('$TX\ Constellation$', 'Location', 'north' ,'Interpreter','latex') ;

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/8a.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------

%% -------------------------------- Item b --------------------------------
scatterplot(gD(500:end), 1, 0, '*b')
title('')
grid on

xlabel('$In-Phase$','interpreter','latex')
ylabel('$Quadrature$','interpreter','latex')

legend('$RX\ Constellation$', 'Location', 'north' ,'Interpreter','latex') ;

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/8b.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------

%% -------------------------------- Item c --------------------------------
phase_error = [0 pi/8 pi/4] ;
T0 = 2;

figure

g_with_error = g .* exp(phase_error(1)) ;
g_with_error_D = g_with_error(T0+1:N:end) ;
scatter(real(g_with_error_D(100:end)),imag(g_with_error_D(100:end)), '*r')
hold on

g_with_error = g .* exp(phase_error(2)) ;
g_with_error_D = g_with_error(T0+1:N:end) ;
scatter(real(g_with_error_D(100:end)),imag(g_with_error_D(100:end)), '*b')

g_with_error = g .* exp(phase_error(3)) ;
g_with_error_D = g_with_error(T0+1:N:end) ;
scatter(real(g_with_error_D(100:end)),imag(g_with_error_D(100:end)), '*k')

% CONCLUSIONES: Cambia la amplitud. Se puede detectar obtener el error de
% fase aproximado a partir de los simbolos recibidos ya que el cambio de
% amplitud es proporcional a ellos.
% -------------------------------------------------------------------------

% -------------------------------- Item d ---------------------------------
phase_error = [0 pi/2] ;
T0 = 2;

g_with_error = g .* exp(phase_error(2)) ;
g_with_error_D = g_with_error(T0+1:N:end) ;
scatter(real(g_with_error_D(100:end)),imag(g_with_error_D(100:end)), '*m')

legend('$\theta = 0$', '$\theta = \pi/8$', '$\theta = \pi/4$', '$\theta = \pi/2$', 'Location', 'north', 'Interpreter','latex') ;

xlabel('$In-Phase$','interpreter','latex')
ylabel('$Quadrature$','interpreter','latex')
grid on

xlim([-6,6])
ylim([-6,6])

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/8c_8d.svg', 'svg') ;

% CONCLUSIONES: se pueden distinguir los simbolos correspondientes a cada
% angulo.
% -------------------------------------------------------------------------

%% -------------------------------- Item e --------------------------------
freq_error = 2*pi*0.1*fc ;
T0 = 2;

g_with_error = g .* exp(-1j*freq_error.*t) ;
g_with_error_D = g_with_error(T0+1:N:end) ;

scatterplot(g_with_error_D(100:end),1,0,'*r')
title('')
grid on
xlabel('$In-Phase$','interpreter','latex')
ylabel('$Quadrature$','interpreter','latex')

legend('$\Delta fc = 10\%$', 'Interpreter','latex') ;

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/8e.svg', 'svg') ;

% CONCLUSIONES: Los simbolos son indistinguibles
% -------------------------------------------------------------------------