%% Ejercicio 7

clc
clear all
close all

BR	= 16e9 ;	% Bd
L	= 10000 ;	% Simulation Length
N	= 4 ;   	% Oversampling rate
fs	= N*BR ;	% Sampling rate to emulate analog domain
T	= 1/BR ;	% Time interval between two consecutive symbols
Ts	= 1/fs ;	% Time between 2 conseutive samples at Tx output

% Generacion de simbolos aleatorios (+1+j, -1+j, -1-j, +1-j)

x_i = 2*randi([0,1],L,1)-1 ;
x_q = 2*randi([0,1],L,1)-1 ; 
x_total = x_i + 1j*x_q ;

% Upsampling para cambiar la tasa de muestreo

xup = upsample(x_total, N) ;

% Filtro de interpolacion y pulse-shaping

rolloff = 0.2 ;
h = rsdcos(BR, fs, rolloff, 500) ;	% h cumple la func de conformador 
                                    % espectral
                                    
m = filter(h,1,xup) ;               % Salida del transmisor


% Upconversion

fc = (1+rolloff)*BR/2*1.5 ;          % Carrier frequency
t = [0:length(m)-1].' .* Ts ;
p = sqrt(2) .* exp(1j*2*pi*fc.*t) ;
s = m .* p ;                        

x = real(s) ;

% Downconversion

r = x ;

i = imag(hilbert(r)) ;
delay = finddelay(i, r);
f = r + 1j*i ;

g = conj(p) .* f ./ (2);

%% -------------------------------- Item a ---------------------------------

NFFT = 1024*8;
WELCH_OVERLAP = 0.*NFFT;

[Pm, f_Pm] = pwelch(m, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Pm_dB = 10*log10(Pm) ;
Pm_dB = Pm_dB - Pm_dB(10) ;             % Normalizacion (a 0dB)

Pm_dB = fftshift(Pm_dB) ;               % En este caso pwelch ya nos devolvio las freq negativas
f_Pm  = f_Pm - f_Pm(end/2);
f_Pm  = f_Pm ./ 1e9 ;                   % Convierto a GHz

[Pm_real, f_Pm_real] = pwelch(real(m), hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Pm_real_dB = 10*log10(Pm_real) ;
Pm_real_dB = Pm_real_dB - Pm_real_dB(10) ;              % Normalizacion (a 0dB)

Pm_real_dB = [Pm_real_dB(1:(end));flip(Pm_real_dB)] ;	% Flip para mostrar la parte de frecuencia negativa
Pm_real_dB = fftshift(Pm_real_dB) ;
f_Pm_real  = [f_Pm_real(1:(end));-flip(f_Pm_real)];
f_Pm_real  = fftshift(f_Pm_real) ;
f_Pm_real  = f_Pm_real ./ 1e9 ;                         % Convierto a GHz

[Pm_imag, f_Pm_imag] = pwelch(imag(m), hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Pm_imag_dB = 10*log10(Pm_imag) ;
Pm_imag_dB = Pm_imag_dB - Pm_imag_dB(10) ;              % Normalizacion (a 0dB)

Pm_imag_dB = [Pm_imag_dB(1:(end));flip(Pm_imag_dB)] ;	% Flip para mostrar la parte de frecuencia negativa
Pm_imag_dB = fftshift(Pm_imag_dB) ;
f_Pm_imag  = [f_Pm_imag(1:(end));-flip(f_Pm_imag)];
f_Pm_imag  = fftshift(f_Pm_imag) ;
f_Pm_imag  = f_Pm_imag ./ 1e9 ;                         % Convierto a GHz

figure
plot(f_Pm, Pm_dB, '-k', 'DisplayName', '$m(t)\ PSD$', 'LineWidth', 1)
hold on
plot(f_Pm_real, Pm_real_dB, '--b', 'DisplayName', '$Real[m(t)]\ PSD$', 'LineWidth', 1)
plot(f_Pm_imag, Pm_imag_dB, '--r', 'DisplayName', '$Imag[m(t)]\ PSD$', 'LineWidth', 1)

xlabel('$Frequency\ [GHz]$','Interpreter','latex')
ylabel('$PSD\ Magnitude\ [dB]$','Interpreter','latex')
xlim([-30 30])
ylim([-140 10])
grid on
legend('Interpreter','latex','Location','southeast')

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/7a.svg', 'svg') ;

% CONCLUSIONES: m(t) ocupa el mismo ancho de banda que Real[m(t)] y
% e Imag[m(t)]. 
% -------------------------------------------------------------------------

%% -------------------------------- Item b --------------------------------
[Ps, f_Ps] = pwelch(s, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Ps_dB = 10*log10(Ps) ;
Ps_dB = Ps_dB + 100 ;                   % Normalizacion (a 0dB) 

Ps_dB = [Ps_dB(1:(end));Ps_dB] ;        % Sin flip. Se repite por la 
Ps_dB = fftshift(Ps_dB) ;               % periodicidad del espectro.
f_Ps  = [f_Ps(1:(end));-flip(f_Ps)];
f_Ps  = fftshift(f_Ps) ;
f_Ps  = f_Ps ./ 1e9 ;                   % Convierto a GHz

figure
plot(f_Ps, Ps_dB, '-b', 'DisplayName', '$x(t)\ PSD$', 'LineWidth', 1)
xlabel('$Frequency\ [GHz]$','Interpreter','latex')
ylabel('$PSD\ Magnitude\ [dB]$','Interpreter','latex')
xlim([-30 30])
ylim([-140 10])
grid on
legend('Interpreter','latex','Location','northwest')

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/7b.svg', 'svg') ;

% CONCLUSIONES: s(t) con m(t) complejo ocupa el mismo BW que s(t) para m(t)
% real del ejercicio anterior.
% -------------------------------------------------------------------------

%% -------------------------------- Item c --------------------------------
[Pm, f_Pm] = pwelch(m, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Pm_dB = 10*log10(Pm) ;
Pm_dB = Pm_dB - Pm_dB(10) ;             % Normalizacion (a 0dB)

Pm_dB = fftshift(Pm_dB) ;               % En este caso pwelch ya nos devolvio las freq negativas
f_Pm  = f_Pm - f_Pm(end/2);
f_Pm  = f_Pm ./ 1e9 ;                   % Convierto a GHz

[Pg, f_Pg] = pwelch(g, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Pg_dB = 10*log10(Pg) ;
Pg_dB = Pg_dB - Pg_dB(10) ;             % Normalizacion (a 0dB)

Pg_dB = fftshift(Pg_dB) ;               % En este caso pwelch ya nos devolvio las freq negativas
f_Pg  = f_Pg - f_Pg(end/2);
f_Pg  = f_Pg ./ 1e9 ;                   % Convierto a GHz

figure
plot(f_Pm, Pm_dB, '-b', 'DisplayName', '$m(t)\ PSD$', 'LineWidth', 1)
hold on
plot(f_Pg, Pg_dB, '--r', 'DisplayName', '$g(t)\ PSD$', 'LineWidth', 1)
xlabel('$Frequency\ [GHz]$','Interpreter','latex')
ylabel('$PSD\ Magnitude\ [dB]$','Interpreter','latex')
xlim([-30 30])
ylim([-140 10])
grid on
legend('Interpreter','latex')

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/7c1.svg', 'svg') ;

figure
plot(real(m), '-b', 'DisplayName', '$Real[m(t)]$', 'LineWidth', 1) ;
hold on
plot(real(g), '--r', 'DisplayName', '$Real[g(t)]$', 'LineWidth', 1) ;
xlabel('$Time\ [samples]$','Interpreter','latex')
ylabel('$Amplitude$', 'Interpreter','latex')
xlim([350 500])
ylim([-2.5 2.5])
grid on
legend('Interpreter','latex', 'Location', 'north')

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/7c2.svg', 'svg') ;

figure
plot(imag(m), '-b', 'DisplayName', '$Imag[m(t)]$', 'LineWidth', 1) ;
hold on
plot(imag(g), '--r', 'DisplayName', '$Imag[g(t)]$', 'LineWidth', 1) ;
xlabel('$Time\ [samples]$','Interpreter','latex')
ylabel('$Amplitude$', 'Interpreter','latex')
xlim([350 500])
ylim([-2.5 2.5])
grid on
legend('Interpreter','latex', 'Location', 'north')

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/7c3.svg', 'svg') ;

% CONCLUSIONES: la se�al recuperada g(t) es igual que la se�al m(t)
% -------------------------------------------------------------------------