%% Raiz coseno realzado
% Esta funcion recibe el symbol-rate, la frecuencia de muestreo del filtro,
% el rolloff y la cantidad de taps deseados.
function [taps] = rootrsdcos(fc, fs, rolloff, n_taps)

    rolloff = rolloff + 0.01 ;  % Evita los puntos para los que h(t) no esta definido
    Ts      = 1/fs ;            % Periodo de muestreo del filtro
    T       = 1/fc ;            % Periodo de los simbolos

    if mod(n_taps, 2) == 1      % Forzando cantidad de taps impar
        n_taps = n_taps + 1 ;   % Suma 1 si es impar, porque en la creacion de t ya se suma 1 de por si
    end

    t = (-n_taps/2:1:n_taps/2) .* Ts ;
    
    t_norm = t./T ;
    
    %taps = 4*rolloff/(pi*sqrt(T)) .* ( ( cos(pi.*(1+rolloff).*t_norm) + sin(pi.*(1-rolloff).*t_norm)./(4*rolloff.*t_norm) ) ./ (1-(4*rolloff*t_norm).^2) ) ;
    taps = ( sin(pi.*(1-rolloff).*t_norm) + 4*rolloff .*t_norm .*cos(pi.*(1+rolloff).*t_norm) ) ./ (pi.*t_norm.*(1-(4*rolloff*t_norm).^2)) ;
    taps(round(n_taps/2)+1) = (1-rolloff)+4*rolloff/pi ;
    
end