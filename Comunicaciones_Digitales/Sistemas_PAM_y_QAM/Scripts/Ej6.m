%% Ejercicio 6

clc
clear all
close all

BR	= 16e9;     % Bd
L	= 10000;    % Simulation Length
N	= 4;        % Oversampling rate
fs	= N*BR;     % Sampling rate to emulate analog domain
T	= 1/BR;     % Time interval between two consecutive symbols
Ts	= 1/fs;     % Time between 2 conseutive samples at Tx output

% Generacion de simbolos aleatorios (+1,-1)

xi = 2*randi([0,1],L,1)-1;

% Upsampling para cambiar la tasa de muestreo

xup = upsample(xi,N);

% Filtro de interpolacion y pulse-shaping

rolloff = 0.2 ;

h = rsdcos(BR, fs, rolloff, 500) ;	% h cumple la func de conformador 
                                    % espectral
m = filter(h,1,xup) ;               % Salida del transmisor PAM2

% Upconversion

fc = (1+rolloff)*BR/2*1.2;          % Carrier frequency
t = [0:length(m)-1].' .* Ts ;
p = sqrt(2) .* exp(1j*2*pi*fc.*t) ;
s = m .* p ;                        

x = real(s) ;

% Downconversion

r = x ;

i = imag(hilbert(r)) ;
delay = finddelay(i, r);
f = r + 1j*i ;

g = conj(p) .* f ./(2);

g = real(g);

%% -------------------------------- Item a --------------------------------

NFFT = 1024*8;
WELCH_OVERLAP = 0.*NFFT;

[Pm, f_Pm] = pwelch(m, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Pm_dB = 10*log10(Pm) ;
Pm_dB = Pm_dB - Pm_dB(10) ;             % Normalizacion (a 0dB)

Pm_dB = [Pm_dB(1:(end));flip(Pm_dB)] ;	% Flip para mostrar la parte de frecuencia negativa
Pm_dB = fftshift(Pm_dB) ;
f_Pm  = [f_Pm(1:(end));-flip(f_Pm)];
f_Pm  = fftshift(f_Pm) ;
f_Pm  = f_Pm ./ 1e9 ;                   % Convierto a GHz

[Ps, f_Ps] = pwelch(s, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Ps_dB = 10*log10(Ps) ;
Ps_dB = Ps_dB + 100 ;                   % Normalizacion (a 0dB) 

Ps_dB = [Ps_dB(1:(end));Ps_dB] ;        % Sin flip. Se repite por la 
Ps_dB = fftshift(Ps_dB) ;               % periodicidad del espectro.
f_Ps  = [f_Ps(1:(end));-flip(f_Ps)];
f_Ps  = fftshift(f_Ps) ;
f_Ps  = f_Ps ./ 1e9 ;                   % Convierto a GHz

figure
plot(f_Pm, Pm_dB, '-b', 'DisplayName', '$m(t)\ PSD$')
hold on
plot(f_Ps, Ps_dB, '-r', 'DisplayName', '$s(t)\ PSD$')
xlabel('$Frequency\ [GHz]$','Interpreter','latex')
ylabel('$PSD\ Magnitude\ [dB]$','Interpreter','latex')
xlim([-30 30])
ylim([-140 10])
grid on
legend('Interpreter','latex','Location','northwest')

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/6a.svg', 'svg') ;

% CONCLUSIONES: s(t) es analitica porque su PSD esta compuesta solo de
% frecuencias positivas.
% El BW de s(t) es el doble del BW de m(t).
% -------------------------------------------------------------------------

%% -------------------------------- Item b --------------------------------
[Pm, f_Pm] = pwelch(m, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Pm_dB = 10*log10(Pm) ;
Pm_dB = Pm_dB - Pm_dB(10) ;             % Normalizacion (a 0dB) 


Pm_dB = [Pm_dB(1:(end));flip(Pm_dB)] ;	% Flip para mostrar la parte de frecuencia negativa
Pm_dB = fftshift(Pm_dB) ;
f_Pm  = [f_Pm(1:(end));-flip(f_Pm)];
f_Pm  = fftshift(f_Pm) ;
f_Pm  = f_Pm ./ 1e9 ;                   % Convierto a GHz

[Px, f_Px] = pwelch(x, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Px_dB = 10*log10(Px) ;
Px_dB = Px_dB + 100 ;                   % Normalizacion (a 0dB)

Px_dB = [Px_dB(1:(end));flip(Px_dB)] ;	% Flip para mostrar la parte de frecuencia negativa
Px_dB = fftshift(Px_dB) ;
f_Px  = [f_Px(1:(end));-flip(f_Px)];
f_Px  = fftshift(f_Px) ;
f_Px  = f_Px ./ 1e9 ;                   % Convierto a GHz

figure
plot(f_Pm, Pm_dB, '-b', 'DisplayName', '$m(t)\ PSD$')
hold on
plot(f_Px, Px_dB, '-r', 'DisplayName', '$x(t)\ PSD$')
xlabel('$Frequency\ [GHz]$','Interpreter','latex')
ylabel('$PSD\ Magnitude\ [dB]$','Interpreter','latex')
xlim([-30 30])
ylim([-140 10])
grid on
legend('Interpreter','latex','Location','northeast')


set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/6b.svg', 'svg') ;

% CONCLUSIONES: x(t) ocupa el doble de BW que m(t)
% -------------------------------------------------------------------------

%% -------------------------------- Item c --------------------------------
figure
hold on

plot(i,'-b','Linewidth', 1, 'DisplayName', '$i(t)$');
plot(imag(s),'--r','Linewidth', 1, 'DisplayName', '$Imag[s(t)]$');

xlabel('$Time\ [samples]$','Interpreter','latex')
ylabel('$Amplitude$', 'Interpreter','latex')
xlim([350 500])
ylim([-2.5 2.5])
grid on
legend('Interpreter','latex', 'Location', 'north')

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/6c.svg', 'svg') ;


% CONCLUSIONES: Se comprobo que las senales son identicas realizando la
% superposicion entre las mismas.
% -------------------------------------------------------------------------

%% -------------------------------- Item d ---------------------------------
[Pm, f_Pm] = pwelch(m, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Pm_dB = 10*log10(Pm) ;
Pm_dB = Pm_dB - Pm_dB(10) ;             % Normalizacion (a 0dB) 

Pm_dB = [Pm_dB(1:(end));flip(Pm_dB)] ;	% Flip para mostrar la parte de frecuencia negativa
Pm_dB = fftshift(Pm_dB) ;
f_Pm  = [f_Pm(1:(end));-flip(f_Pm)];
f_Pm  = fftshift(f_Pm) ;
f_Pm  = f_Pm ./ 1e9 ;                   % Convierto a GHz

[Pg, f_Pg] = pwelch(g, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs) ;
Pg_dB = 10*log10(Pg) ;
Pg_dB = Pg_dB - Pg_dB(10) ;             % Normalizacion (a 0dB) 


Pg_dB = [Pg_dB(1:(end));flip(Pg_dB)] ;	% Flip para mostrar la parte de frecuencia negativa
Pg_dB = fftshift(Pg_dB) ;
f_Pg  = [f_Pg(1:(end));-flip(f_Pg)];
f_Pg  = fftshift(f_Pg) ;
f_Pg  = f_Pg ./ 1e9 ;                   % Convierto a GHz


figure
plot(f_Pm, Pm_dB, '-b', 'DisplayName', '$m(t)\ PSD$')
hold on
plot(f_Pg, Pg_dB, '--r', 'DisplayName', '$g(t)\ PSD$')
xlabel('$Frequency\ [GHz]$','Interpreter','latex')
ylabel('$PSD\ Magnitude\ [dB]$','Interpreter','latex')
xlim([-30 30])
ylim([-140 10])
grid on
legend('Interpreter','latex')

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/6d1.svg', 'svg') ;

figure
plot(m,'-b','Linewidth', 1, 'DisplayName', '$m(t)$');
hold on
plot(g,'--r','Linewidth', 1, 'DisplayName', '$g(t)$');
xlabel('$Time\ [samples]$','Interpreter','latex')
ylabel('$Amplitude$','Interpreter','latex')
xlim([350 500])
ylim([-2.5 2.5])
grid on
legend('Interpreter','latex', 'Location', 'north')

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/6d2.svg', 'svg') ;

% CONCLUSIONES: la se�al recuperada g(t) es igual que la se�al m(t)
% -------------------------------------------------------------------------