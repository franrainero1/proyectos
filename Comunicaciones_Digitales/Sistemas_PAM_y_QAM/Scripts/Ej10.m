%% Ejercicio 10 QAM - 16

clc
clear 
close all

BR	= 16e9 ;	% Bd
L	= 10000 ;	% Simulation Length
N	= 4 ;   	% Oversampling rate
fs	= N*BR ;	% Sampling rate to emulate analog domain
T	= 1/BR ;	% Time interval between two consecutive symbols
Ts	= 1/fs ;	% Time between 2 conseutive samples at Tx output

% Filtro de interpolacion y pulse-shaping

rolloff = 0.2 ;
h = rsdcos(BR, fs, rolloff, 500) ;

% Generacion de simbolos

M	= 16 ;
BPS = log2(M) ;

data = randi([0,1],L,BPS) ;
data_Symbols = bi2de(data) ;

mapped_data = qammod(data_Symbols, M, 'gray') ;

% Upsampling to change sampling rate

xup = upsample(mapped_data, N) ;
                                
m = filter(h,1,xup) ;      % Se�al interpolada

% -- Modelo equivalente de un transceptor QAM 16 --

t = [0:length(m)-1].' .* Ts ;

phase_error = 0 ;   % Error de fase en rad
freq_error	= 0 ;	% Error de frecuencia en Hz

ep = exp(1j*2*pi*freq_error.*t + phase_error) ;
g = m .* ep ;

T0 = 2;
gD = g(T0+1:N:end) ;

%% -------------------------------- Item a --------------------------------
phase_error = pi/12 ;   % Error de fase en rad
freq_error	= 0 ;       % Error de frecuencia en Hz

ep = exp(1j*2*pi*freq_error.*t + phase_error) ;
g = m .* ep ;

T0 = 2;
gD = g(T0+1:N:end) ;

scatterplot(gD(500:end),1,0,'*r') ;  % Constelacion de simbolos recibidos
grid on
title('')

xlabel('$In-Phase$','interpreter','latex')
ylabel('$Quadrature$','interpreter','latex')

legend('QAM-16 RX. $\theta = \pi/12 = 15\ deg$', 'Location', 'best', 'Interpreter','latex') ;

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/10a.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------

% -------------------------------- Item b ---------------------------------
phase_error = 0 ;       % Error de fase en rad
freq_error	= 200e6 ;	% Error de frecuencia en Hz

ep = exp(1j*2*pi*freq_error.*t + phase_error) ;
g = m .* ep ;

T0 = 2;
gD = g(T0+1:N:end) ;

scatterplot(gD(500:end),1,0,'*r') ;  % Constelacion de simbolos recibidos
grid on
title('')

xlabel('$In-Phase$','interpreter','latex')
ylabel('$Quadrature$','interpreter','latex')

h = legend('$QAM-16 RX.\ \Delta f = 200MHz$', 'Location', 'north', 'Interpreter','latex') ;

xlim([-6,6])
ylim([-6,6])

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/10b.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------

%% Ejercicio 10 QAM - 64

clc
clear all

BR	= 16e9 ;	% Bd
L	= 10000 ;	% Simulation Length
N	= 4 ;   	% Oversampling rate
fs	= N*BR ;	% Sampling rate to emulate analog domain
T	= 1/BR ;	% Time interval between two consecutive symbols
Ts	= 1/fs ;	% Time between 2 conseutive samples at Tx output

% Filtro de interpolacion y pulse-shaping

rolloff = 0.2 ;
h = rsdcos(BR, fs, rolloff, 500) ;

% Generacion de simbolos

M	= 64 ;
BPS = log2(M) ;

data = randi([0,1],L,BPS) ;
data_Symbols = bi2de(data) ;

mapped_data = qammod(data_Symbols, M, 'gray') ;

% Upsampling to change sampling rate

xup = upsample(mapped_data, N) ;
                                
m = filter(h,1,xup) ;      % Se�al interpolada

% -- Modelo equivalente de un transceptor QAM 64 --

t = [0:length(m)-1].' .* Ts ;

phase_error = 0 ;   % Error de fase en rad
freq_error	= 0 ;	% Error de frecuencia en Hz

ep = exp(1j*2*pi*freq_error.*t + phase_error) ;
g = m .* ep ;

T0 = 2;
gD = g(T0+1:N:end) ;

% -------------------------------- Item c ---------------------------------
phase_error = pi/12 ;   % Error de fase en rad
freq_error	= 0 ;       % Error de frecuencia en Hz

ep = exp(1j*2*pi*freq_error.*t + phase_error) ;
g = m .* ep ;

T0 = 2;
gD = g(T0+1:N:end) ;

scatterplot(gD(500:end),1,0,'*r') ;  % Constelacion de simbolos recibidos
grid on
title('')

xlabel('$In-Phase$','interpreter','latex')
ylabel('$Quadrature$','interpreter','latex')

legend('$QAM-64 RX.\ \theta = \pi/12 = 15deg$', 'Location', 'north','Interpreter','latex') ;

xlim([-12,12])
ylim([-12,12])

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/10c.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------

% -------------------------------- Item d ---------------------------------
phase_error = 0 ;       % Error de fase en rad
freq_error	= 200e6 ;	% Error de frecuencia en Hz

ep = exp(1j*2*pi*freq_error.*t + phase_error) ;
g = m .* ep ;

T0 = 2;
gD = g(T0+1:N:end) ;

scatterplot(gD(500:end),1,0,'*r') ;  % Constelacion de simbolos recibidos
grid on
title('')

xlabel('$In-Phase$','interpreter','latex')
ylabel('$Quadrature$','interpreter','latex')

legend('$QAM-64 RX.\ \Delta f = 200MHz$', 'Location', 'north','Interpreter','latex') ;

xlim([-12,12])
ylim([-12,12])

set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/10d.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------