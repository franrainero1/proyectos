%% Ejercicio 1

clc
clear all
close all

BR      = 32e9; % Bd
N       = 8;    % Oversampling rate
fs      = N*BR; % Sampling rate
Ntaps	= 501;  % Desired number of taps

rolloff = [0.7 0.5 0.3 0.1] ; 

% -------------------------------- Item b ---------------------------------

% -- Creacion del eje temporal --
if mod(Ntaps, 2) == 1
	Ntaps = Ntaps + 1 ;
end
plot_time = [0:1:Ntaps] ;  % Creo un eje temporal igual al de la funcion
% -------------------------------

figure('Name', 'Raised cosine (time)')
for i = 1:length(rolloff)
    h = rsdcos(BR, fs, rolloff(i), Ntaps);
    plot(plot_time, h, 'Linewidth', 1.5, 'DisplayName', '$\beta$ = '+ string(rolloff(i)))
    hold on
end
xlabel('Samples','Interpreter', 'latex')
ylabel('Amplitude','Interpreter', 'latex')
grid on
xlim([200 300]) ;
legend('Interpreter', 'latex')
set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/1b.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------

% -------------------------------- Item c ---------------------------------
NFFT = 8*1024 ;

% -- Creacion del eje de freq --
freq_step	= 2*pi/NFFT ;
omega       = -pi:freq_step:pi-freq_step ;
% ------------------------------

figure('Name', 'Raised cosine (freq)')

for i = 1:length(rolloff)
    h = rsdcos(BR, fs, rolloff(i), Ntaps);
    H = abs(fft(h, NFFT)) ;
    H = H ./ H(1) ;
    %H = 20*log10(H) ;
    plot(omega, fftshift(H), 'Linewidth', 1.5, 'DisplayName', '$\beta$ = '+ string(rolloff(i)))
    hold on
end

xlabel('Discrete frequency [rad/sec]','Interpreter', 'latex')
ylabel('Amplitude','Interpreter', 'latex')
xlim([-1 1]) ;
grid on
legend('Interpreter', 'latex')
set(gcf, 'Position', [100 100 400 400]) ;
saveas(gcf, 'Plots/1c.svg', 'svg') ;

% CONCLUSIONES: 
% -------------------------------------------------------------------------