%---------------------------------------------------------------------%
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Programmers: Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
%---------------------------------------------------------------------%

% Ejercicio 6 - PLL simulacion custom (bode)

clear; clc; close all;

% Parametros
fs = 16e9;               % Frecuencia de muestreo
Ts = 1/fs;               % Periodo de muestreo

Kp = 0.01;               % Ganancia proporcional
Ki = Kp/100;             % Ganancia integral

L = 10;                  % Latencia en la rama de realimentación
Length = 5e4;            % Longitud de cada simulacion
BodePoints = 1000;        % Cantidad de puntos del Bode

%% Modelado

error       = zeros(Length,1);   % Salida del detector de fase 
error_prop  = zeros(Length,1);   % Salida del controlador proporcional
error_int   = zeros(Length,1);   % Salida del controlador integral
nco_in      = zeros(Length,1);   % Entrada del NCO
tita_out    = zeros(Length,1);   % Fase de salida
tita_delay  = zeros(Length,1);   % Fase de salida retardada con latencia

DELAY_LINE  = zeros(L+1,1)   ;   % FIFO para modelar la latencia

ampIn  = 0                   ;   % Amplitud de entrada del Bode
ampOut = zeros(BodePoints,1) ;   % Amplitud de salida del Bode

contador = 0                 ;   % Contador para ciclos del Bode


for f=0:(fs/2)/BodePoints:fs/2-(fs/2)/BodePoints
    
    contador = contador + 1;
    
    fn = f;
    wn = 2*pi*fn;
    titaMax = 45; %Grados
    t = (0:Length-1).*Ts;
    tita_in = titaMax/180*pi * sin(wn.*t);
    ampIn = titaMax/180*pi;
    
    for n=2:Length
        % Detector de fase
        error(n) = tita_in(n) - tita_delay(n-1);

        % Filtro de lazo (Controlador PI)
        error_prop(n) = Kp*error(n);
        error_int(n) = error_int(n-1)+Ki*error(n);
        nco_in(n) = error_prop(n)+error_int(n);

        % NCO (integrador)
        tita_out(n) = tita_out(n-1)+nco_in(n);

        % Modelado del retardo
        % Asumo que los datos entran por el final y salen por el frente
        % Primero roto, luego cargo el nuevo valor al final
        DELAY_LINE = [DELAY_LINE(2:end); tita_out(n)]; 
        % Finalmente, logueo la senial retardada
        tita_delay(n) = DELAY_LINE(1); 
    end
    
    ampOut(contador) = max(tita_out(1000:end));
    
end

%% Modelado anterior

z = tf('z',Ts);

G_z = (Kp+Ki*(z/(z-1)))*(z/(z-1));
H_z = z^(-L);
F_z = feedback(G_z,H_z);

[F_z_mag,~,wout] = bode(F_z);             % Rta en frecuencia

%% Plot

f=(0:(fs/2)/BodePoints:fs/2-(fs/2)/BodePoints);
Mag_dB = 20*log10(ampOut/ampIn);

figure
semilogx(f,Mag_dB,'-r', 'Linewidth',1.5);
hold on
semilogx(wout./(2*pi),20*log10(F_z_mag(:)),'--b', 'Linewidth',1.5);
grid on
title('Respuesta en frecuencia PLL custom','interpreter','latex')
xlabel('Frecuencia [Hz]','interpreter','latex')
ylabel('Magnitud [dB]','interpreter','latex')
hold on
plot([1 1]*fs/2,ylim,'--k','LineWidth', 1.5)
legend({'Bode manual','Bode Matlab','fs/2'},'interpreter','latex')
xlim([1e7,1e10])
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
%saveas(gcf,'img/Ej6_BodeManual.svg', 'svg')
