%---------------------------------------------------------------------%
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Programmers: Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
%---------------------------------------------------------------------%

% Ejercicio 8 - PLL en recuperacion de portadora: bode-estimacion espectral

clear; clc; close all;

% Parametros de modulacion QAM
BR = 32e9   ;    % Baud Rate [Bd]
L = 1e4     ;    % Longitud de simulacion
fs = BR     ;    % Frecuencia de muestreo
T = 1/BR    ;    % Tiempo entre simbolos
Ts = 1/fs   ;    % Tiempo entre muestras
M=16        ;    % Orden de modulacion
EbNo_dB = 60;    % EbNO en dB

vector_freq = [1e6:10e6:100e6,100e6:100e6:1e9,1e9:1e9:(fs/2-1e9)];
P_out = zeros(length(vector_freq),1);

for j = 1:length(vector_freq)

    % Parametros de error en la portadora del LO en Rx
    delta_freq  = 0e6        ; % Offset del LO   [Hz]
    LW          = 000e3      ; % Ancho de linea  [Hz]
    theta0      = 00/180*pi  ; % Fase inicial    [deg]

    sineFrec    = vector_freq(j) ; % Frecuencia de senoidal [Hz]
    sineAmp     = 30/180*pi      ; % Amplitud de senoidal   [deg]

    %% Calculo de SNR
    EbNo = 10^(EbNo_dB/10)  ;
    SNR_Slc = EbNo * log2(M);
    SNR_canal = SNR_Slc     ;

    %% Generacion de simbolos con ruido

    % Generacion de simbolos QAM-M
    xsymb = qammod(randi([0 M-1], L,1), M);

    % Agregado de ruido
    % SNR_canal [dB] = EbNo [dB] + 10*log10(k) - 10*log10(N) --%%-- k = log2(M)
    Psignal = var(xsymb);
    Pnoise = Psignal/SNR_canal;
    No = Pnoise;                % Varianza por unidad de frecuencia
    noise_real = sqrt(No/2)*randn(size(xsymb)); % sqrt implica desv estandar
    noise_imag = sqrt(No/2)*randn(size(xsymb));
    noise = noise_real + 1j*noise_imag;

    rx_noisy = xsymb + noise;   % Signal con ruido
    clear noise noise_real noise_imag

    %% Agregado de defectos de portadora

    Ldata= length(rx_noisy);
    time = (0:Ldata-1).'.*Ts;
    lo_offset = exp(1j*2*pi*delta_freq*time);   % LO offset de frecuencia
    phase_offset = exp(1j*theta0);              % LO offset de fase
    % Para el ruido de fase:
    freq_noise = sqrt(2*pi*LW/fs).*randn(Ldata,1);
    phase_noise = cumsum(freq_noise);           % Proceso de Wiener (integral)
    osc_pn = exp(1j.*phase_noise);              % LO ruido de fase
    % Senoidal 
    senoidal = sineAmp*sin(2*pi*sineFrec.*time);   
    osc_seno = exp(1j.*senoidal);               % LO offset con senial seno

    %Signal con defectos
    rxs = rx_noisy.*lo_offset.*phase_offset.*osc_pn.*osc_seno;    

    %% PLL como carrier recovery
    Ldata           = length(rxs)   ;
    Kp              = 20e-2         ;
    Ki              = Kp/500        ;
    phase_error     = zeros(Ldata,1);
    nco_output      = zeros(Ldata,1);
    integral_branch = zeros(Ldata,1);
    pll_output      = zeros(Ldata,1);   %Salida del carrier recovery

    for m=2:Ldata
        xpll = rxs(m);
        derot_x = xpll*exp(-1j*nco_output(m-1));
        pll_output(m) = derot_x;
        
        a_hat = slicer(derot_x, M);
        phase_error(m) = angle(derot_x .* conj(a_hat));
        prop_error = phase_error(m) * Kp;
        integral_branch(m) = integral_branch(m-1) + Ki*phase_error(m);
        nco_output(m) = nco_output(m-1) + prop_error +integral_branch(m);
    end

    %% Estimacion espectral para obtener potencia del tono recuperado
    [P_ref, f_ref ] = pwelch(senoidal  ,[],0, 2^13, fs );
    [P_tono, f_ton] = pwelch(nco_output,[],0, 2^13, fs );

    [~, idx] = max(P_ref);

    P_out(j) = 10*log10(P_tono(idx)/P_ref(idx));
end

%% Plots

figure
semilogx(vector_freq,P_out,'--r', 'Linewidth',1.5);
hold on
grid on
title('Jitter Transfer','interpreter','latex')
xlabel('Frecuencia [Hz]','interpreter','latex')
ylabel('Magnitud [dB]','interpreter','latex')
hold on
plot([1 1]*fs/2,ylim,'--k','LineWidth', 1.5)
legend({'Jitter Transfer','fs/2'},'interpreter','latex')
xlim([1e7,2e10])
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
%saveas(gcf,'img/Ej8_JitterTransfer.svg', 'svg')