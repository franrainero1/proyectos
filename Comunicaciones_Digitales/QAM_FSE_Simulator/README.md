# Simulador de comunicaciones digitales

FIXME: agregar todas los metodos nuevos del simulador (TP4)

El simulador cuenta con cuatro clases de Objetive C (MATLAB) que se encuentran en `src/`:

```mermaid
    classDiagram
        class Simulator{
            + tx_ak 
            + tx_out 
            + ch_out 
            + rx_ak 
            + rx_match_filter_out
            + rx_correlator_out
            
            - simulationIterations
            - simulationLength

            - showPlots
            - exportPlots
            - numberOfSymbolsToShow
            - transmittedSymbols
            - transmittedSignals
            - transmittedConstellation
            - transmittedEyeDiagram
            - TxRxSpectrum
            - receivedSignals
            - receivedEyeDiagram
            - receivedConstellation
            - correlatorOutVsSlicerOut
            - slicerInputHistogram
            
            - exportLogs
            - exportSER
            - exportBER

            - modulationType
            - symbolRate
            - mapperLevels
            - useMatchedFilter
            - oversamplingFactor

            - tx
            - rx
            - ch
                        
            - simConfigArrays
            - sysConfigArrays
            - txConfigArrays
            - chConfigArrays
            - rxConfigArrays

            
            + simulator()
            + setNewConfiguration()
            + run()
            - runIteration()
            - systemPlots()
            - plotTransmittedSymbols()
            - plotTransmittedSignals()
            - plotTransmittedConstellation()
            - plotTransmittedEyeDiagram()
            - plotTxRxSpectrum()
            - plotTxRxSpectrum()
            - plotReceivedSignals()
            - plotReceivedEyeDiagram()
            - plotReceivedConstellation()
            - plotCorrelatorOutVsSlicerOut()
            - plotSlicerInputHistogram()
            - systemErrors()
            - simulationVectorCreator()
            - createIterationDirectory()
            
        }

        class Transmitter{
            + filterTaps
            + filterSamplingFreq
            + configStruct

            - modulationType
            - symbolRate
            - mapperLevels
            - filterType
            - filterLength
            - filterRolloff
            - oversamplingFactor

            + transmitter()
            + get.filterTaps()
            + get.filterSamplingFreq()
            + get.configStruct()
            + get.modulationType()
            + get.mapperLevels()
            + transmit()
            
        }

        class Channel{
            + complexNoiseVariance

            + channel()
            + set.complexNoiseVariance()
            + addNoise()
        }

        class Receiver{
            + filterTaps
            + filterSamplingFreq
            + filterPower

            - modulationType
            - symbolRate
            - mapperLevels
            - filterType
            - filterLength
            - filterRolloff
            - oversamplingFactor
            - correlatorPhase
            - dwConvPhaseError
            - dwConvfrequencyOffset

            + receiver()
            + get.filterSamplingFreq()
            + get.filterTaps()
            + get.filterPower()
            + getConfigStruct()
            + receive()
        }

```

La estructura del simulador es:

```mermaid
    classDiagram
    Simulator .. Transmitter    : Object tx
    Simulator .. Receiver       : Object rx
    Simulator .. Channel        : Object ch
```
