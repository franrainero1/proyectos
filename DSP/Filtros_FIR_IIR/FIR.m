%% TP1 Comunicaciones Digitales - Ejercicio 3%%

clc;
clear; 
close all;

% Ejercicio 3 punto (c)

w = 0:(2*3.14/(8*1024)):(2*3.14-(2*3.14/(8*1024)));    % w va de 0 a casi 2pi en pasos de 2pi/(8*1024)
%Respuesta en frecuencia calculada en papel:
H_c = 1/5*(1+cos(w)+cos(2*w)+cos(3*w)+cos(4*w)-1i*(sin(w)+sin(2*w)+sin(3*w)+sin(4*w)));

figure;  
subplot(1,2,1);
plot(w, 20*log10(abs(H_c)), 'LineWidth', 2);  % M�dulo de H_c en dB
grid on;
title(" M�dulo de Rta en frecuencia calculada |H_c(e^{jw})|")
xlabel("Frecuencia discreta w");
ylabel("Magnitud en [dB]");
hold on

subplot(1,2,2);
plot(w, unwrap(angle(H_c)), 'LineWidth', 2);  % Fase de H_c en rad
grid on;
title(" Fase de Rta en frecuencia calculada < H_c(e^{jw})")
xlabel("Frecuencia discreta w");
ylabel("Fase en [rad]");
hold on

%Es un filtro pasa bajos FIR, ya que aten�a las frecuencias mayor a 0.

% Ejercicio 3 punto (d)

h = [1,1,1,1,1];        
h = h ./5;              % h(n). (Filtro FIR)

Tam = 8*1024;           % Puntos de la fft 
H = fft(h,Tam);         % Respuesta en frecuencia de h

subplot(1,2,1);
plot(w,20*log10(abs(H)), 'LineWidth', 1.5);  % M�dulo de H en dB
grid on;
title(" M�dulo de Rta en frecuencia |H(e^{jw})|")
xlabel("Frecuencia discreta w");
ylabel("Magnitud en [dB]");
legend("H calculada anal�ticamente","H calculada con fft");

subplot(1,2,2);
plot(w, unwrap(angle(H)), 'LineWidth', 1.5);  % Fase de H en rad
grid on;
title(" Fase de Rta en frecuencia < H(e^{jw})")
xlabel("Frecuencia discreta w");
ylabel("Fase en [rad]");
legend("H calculada anal�ticamente","H calculada con fft");

% Ejercicio 3 punto f

M2_list = [4,8,16,32];

figure
labels = {}; 

for idx = 1:length(M2_list)
    
    M2 = M2_list(idx);
    h = 1/(M2+1) .* ones(M2+1,1);
    
    H = fft(h, Tam);
    plot(w, 20*log10(abs(H)), 'LineWidth', 2);
    labels{end+1} = sprintf("M2=%d", M2);
    hold all;
end
              
grid on;
title("Barrido de M2. Rta en frec de |H(e^{jw})|")
xlabel("Frecuencia discreta w");
ylabel("Magnitud en [dB]");

plot(w, -3*ones(size(w)), '--k');
labels{end+1} = sprintf("-3 dB");

legend(labels)                    
xlim([0,pi])

% Graficador de salidas

x = randn(10e3,1) + 1;            % Entrada random con valor medio 0.7.

M2 = 16;
h = 1/(M2+1) .* ones(M2+1,1);
y = filter(h, 1, x);

figure
hold all
grid on
xlabel("Tiempo discreto");
ylabel("Amplitud");
title("Filtrado de una se�al para distintos M2")
plot(x, '--k')
plot(y, '-r', 'LineWidth', 2);

M2 = 128;
h = 1/(M2+1) .* ones(M2+1,1);
y = filter(h, 1, x);

plot(y, '-b', 'LineWidth', 2);
legend(["Entrada", "Salida para M2 = 16", "Salida para M2 = 128"])
