%% TP1 Comunicaciones Digitales - Ejercicio 4%%

clc;
clear; 
close all;

% Ejercicio 4 punto (d)

NFFT = 8*1024;
alpha = 0.999;

n = 0:1:100*1024-1;
omega = 0:2*pi/NFFT:2*pi-2*pi/NFFT;

h = (1-alpha).*alpha.^n;    %Rta al impulso

H_fft = fft(h,NFFT);        %Rta en frecuencia con fft

H_a = (1-alpha)./(1-alpha.*exp(-1i.*omega));  %Rta en frecuencia anal�tica

figure();
subplot(1,2,1);
plot(omega,20*log10(abs(H_fft)), 'LineWidth', 2);  % M�dulo de H en dB
grid on;
hold on;

subplot(1,2,1);
plot(omega,20*log10(abs(H_a)), 'LineWidth', 1.2);  % M�dulo de H en dB
title(" M�dulo de Rta en frecuencia |H(e^{jw})|")
xlabel("Frecuencia discreta w");
ylabel("Magnitud en [dB]");
legend("H calculada con fft","H calculada analiticamente");

subplot(1,2,2);
plot(omega, angle(H_fft), 'LineWidth', 2);  % Fase de H en rad
hold on;

subplot(1,2,2);
plot(omega, angle(H_a), 'LineWidth', 1.2);  % Fase de H en rad
grid on;
title(" Fase de Rta en frecuencia < H(e^{jw})")
xlabel("Frecuencia discreta w");
ylabel("Fase en [rad]");
legend("H calculada con fft","H calculada analiticamente");

% Ejercicio 4 punto (e)

figure();
plot(h);

% Ejercicio 4 punto (f)

alpha_list = 0.5:0.07:0.99;

figure
labels = {}; 

for idx = 1:length(alpha_list)
    
    alpha = alpha_list(idx);
    h = (1-alpha).*alpha.^n;
    
    H = fft(h, NFFT);
    plot(omega, 20*log10(abs(H)), 'LineWidth', 2);
    labels{end+1} = sprintf("alpha=%.2f", alpha);
    hold all;
end
              
grid on;
title("Barrido de alpha. Rta en frec de |H(e^{jw})|")
xlabel("Frecuencia discreta w");
ylabel("Magnitud en [dB]");

plot(omega, -3*ones(size(omega)), '--k');
labels{end+1} = sprintf("-3 dB");

legend(labels)                    
xlim([0,pi])

% Ejercicio 4 punto (g)

alpha = 0.95;
h = (1-alpha).*alpha.^n;
    
H = fft(h, NFFT);

labels = {};
labels{end+1} = sprintf("Filtro IIR");

figure();
plot(omega, 20*log10(abs(H)), 'LineWidth', 2);
hold on;
grid on;
title(" Rta en frec de |H(e^{jw})|. alpha = 0.95")
xlabel("Frecuencia discreta w");
ylabel("Magnitud en [dB]");


xlim([0,0.1])
ylim([-6,0])

% FIR:

M2_list = 50:1:60;

for idx = 1:length(M2_list)
    
    M2 = M2_list(idx);
    h = 1/(M2+1) .* ones(M2+1,1);
    
    H = fft(h, NFFT);
    plot(omega, 20*log10(abs(H)), 'LineWidth', 2);
    labels{end+1} = sprintf("FIR M2=%d", M2);
    hold all;
end

plot(omega, -3*ones(size(omega)), '--k');
labels{end+1} = sprintf("-3 dB");

legend(labels)


              

                  

