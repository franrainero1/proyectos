/*
 * menu.c
 *
 *  Created on: Apr 23, 2021
 *      Author: franr
 *
 * Este archivo contiene una función que se encarga de comunicarse vía USB con la computadora.
 * Envía instrucciones para operar con el programa y recibe los parámetros para controlar
 * las secuencias.
 */


#include "menu.h"
#include "main.h"
#include "usbd_cdc_if.h"
#include "string.h"
#include "sec1.h"				//Header secuencia 1
#include "sec2.h"				//Header secuencia 2
#include "sec3.h"				//Header secuencia 3

extern uint8_t *secuencia;	//Variable global que indica la secuencia activa
							//Esta variable apunta al Bufer de recepción USB
							//Esto puede verse en usbd_cdc_if.c

void menu (void){

	char *mensaje = "\n\n\rSelección de secuencias.\n\r Ingrese: \n\r -1 p/ sec1 \n\r -2 p/ sec2 \n\r -3 p/ sec3";
	char *estado = "\n\r La secuencia actual es ";

	/* Transmisión del menú de opciones, se utiliza un while porque de otra manera
	 * no se transmiten todas las cadenas deseadas.
	*/

	while(CDC_Transmit_FS((uint8_t *) mensaje, strlen(mensaje)) != USBD_OK);

	while(CDC_Transmit_FS((uint8_t *) estado, strlen(estado)) != USBD_OK);

	while(CDC_Transmit_FS((uint8_t *) secuencia, 1) != USBD_OK);

	switch(*secuencia){				//Selección de secuencia según lo recibido por USB

		case '1':					//Si recibo 1 se ejecuta la secuencia 1
			sec1();
			break;

		case '2':					//Si recibo 2 se ejecuta la secuencia 2
			sec2();
			break;

		case '3':					//Si recibo 3 se ejecuta la secuencia 3
			sec3();
			break;

		default:					//Si recibo otro caracter no hace nada
			HAL_Delay(1000);		//El delay es para que el menu no se imprima tan rápido
			break;					//en la pantalla del puerto serie.
		  }
}
