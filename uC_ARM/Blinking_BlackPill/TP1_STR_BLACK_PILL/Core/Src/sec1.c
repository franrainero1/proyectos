/*
 * sec1.c
 *
 *  Created on: Apr 23, 2021
 *      Author: franr
 *
 *  Este archivo contiene la función para realizar la secuencia 1.
 *  Esta cuenta de dos señales cuadradas, B0 (simétrica con un período de 200ms)
 *  y B1 (simétrica con un período de 400 ms)
 */

#include "sec1.h"
#include "main.h"

extern uint16_t B0;						//Variable global para monitorear B0
extern uint16_t B1;						//Variable global para monitorear B1
extern uint16_t B2;						//Variable global para monitorear B2

void sec1 (void){								// B1 toglea cada dos ciclos de 100ms
												// B0 lo hace en todos los ciclos

	int cont1 = 2;								// Contador para secuencia B1

	int i = 0;

	for(i=0;i<4;i++){							// Cuando se llama a la función esta realiza
												// 2 ciclos de B0 y 1 ciclo de B1 (4 ciclos for)

		HAL_GPIO_TogglePin(GPIOB, B0_Pin);		// Maneja B0

		if(cont1 == 2){
			HAL_GPIO_TogglePin(GPIOB, B1_Pin);	// Maneja B1
			cont1 = 0;
		}

		HAL_Delay(100);							// Delay de 100ms

		cont1++;

		B0 = HAL_GPIO_ReadPin(GPIOB, B0_Pin);	// Monitoreo de B0
		B1 = HAL_GPIO_ReadPin(GPIOB, B1_Pin);	// Monitoreo de B1
		B2 = HAL_GPIO_ReadPin(GPIOB, B2_Pin);	// Monitoreo de B2
		}
}
