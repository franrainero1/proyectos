/*
 * sec3.c
 *
 *  Created on: Apr 23, 2021
 *      Author: franr
 *
 *  Este archivo contiene la función para realizar la secuencia 3.
 *  Esta cuenta de tres señales cuadradas:
 *  B0 (simétrica con un período de 200 ms)
 *  B1 (simétrica con un período de 300 ms)
 *  B2 (asimétrica con un semiperíodo + de 200ms y uno - de 150ms)
 */


#include "sec3.h"
#include "main.h"

extern uint16_t B0;						//Variable global para monitorear B0
extern uint16_t B1;						//Variable global para monitorear B1
extern uint16_t B2;						//Variable global para monitorear B2

void sec3 (void){								// A diferencia de las otras secuencias,
												// esta no utiliza un for, sino que trabaja
												// con variables estáticas (conservan su valor).

	static int cont0 = 0;						// Contador para secuencia B0
	static int cont1 = 0;						// Contador para secuencia B1
	static int cont2 = 0;						// Contador para secuencia B2

	if(cont0 == 2){
		HAL_GPIO_TogglePin(GPIOB, B0_Pin);		// Maneja B0
		cont0 = 0;
	}

	if(cont1 == 3){
		HAL_GPIO_TogglePin(GPIOB, B1_Pin);		// Maneja B1
		cont1 = 0;
	}

	if(cont2 == 4){
		HAL_GPIO_WritePin(GPIOB, B2_Pin, GPIO_PIN_RESET);		// Maneja B2 (ciclo +)
	}

	if(cont2 == 7){
		HAL_GPIO_WritePin(GPIOB, B2_Pin, GPIO_PIN_SET);	// Maneja B2 (ciclo -)
		cont2 = 0;
	}

	HAL_Delay(50);								//Delay de 50ms

	cont0++;
	cont1++;
	cont2++;

	B0 = HAL_GPIO_ReadPin(GPIOB, B0_Pin);	// Monitoreo de B0
	B1 = HAL_GPIO_ReadPin(GPIOB, B1_Pin);	// Monitoreo de B1
	B2 = HAL_GPIO_ReadPin(GPIOB, B2_Pin);	// Monitoreo de B2
}
