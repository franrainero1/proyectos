/*
 * sec2.c
 *
 *  Created on: Apr 23, 2021
 *      Author: franr
 *
 *  Este archivo contiene la función para realizar la secuencia 2.
 *  Esta cuenta de tres señales cuadradas:
 *  B0 (asimétrica con un semiperíodo + de 100ms y uno - de 300ms)
 *  B1 (simétrica con un período de 400 ms)
 *  B2 (asimétrica con un semiperíodo + de 300ms y uno - de 100ms)
 */

#include "sec2.h"
#include "main.h"

extern uint16_t B0;						//Variable global para monitorear B0
extern uint16_t B1;						//Variable global para monitorear B1
extern uint16_t B2;						//Variable global para monitorear B2

void sec2 (void){

	int cont0 = 0;									// Contador para secuencia B0
	int cont1 = 0;									// Contador para secuencia B1
	int cont2 = 0;									// Contador para secuencia B2

	int i = 0;

	HAL_GPIO_WritePin(GPIOB, B0_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, B1_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, B2_Pin, GPIO_PIN_SET);



	for(i=0;i<4;i++){								// Cuando se llama a la función esta realiza
													// 1 ciclo de B0,B1 y B2 (4 ciclos for)

		if(cont0 == 1){
			HAL_GPIO_TogglePin(GPIOB, B0_Pin);		// Maneja B0 (ciclo -)
		}


		if(cont1 == 2){
			HAL_GPIO_TogglePin(GPIOB, B1_Pin);		// Maneja B1
		}

		if(cont1 == 3){
			HAL_GPIO_TogglePin(GPIOB, B2_Pin);		// Maneja B2
		}

		HAL_Delay(100);								// Delay de 100ms

		cont0++;
		cont1++;
		cont2++;

		B0 = HAL_GPIO_ReadPin(GPIOB, B0_Pin);	// Monitoreo de B0
		B1 = HAL_GPIO_ReadPin(GPIOB, B1_Pin);	// Monitoreo de B1
		B2 = HAL_GPIO_ReadPin(GPIOB, B2_Pin);	// Monitoreo de B2
		}
}
