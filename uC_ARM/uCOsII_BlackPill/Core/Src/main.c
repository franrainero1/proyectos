/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <app_cfg.h>
#include <cpu_core.h>
#include <os.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#warning "Modify this value to match the number of external interrupts in your MCU"
#define EXT_INT_MAX_NBR 21u
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
static void StartupTask (void *p_arg);
static void Task1       (void *p_arg);
static void Task2       (void *p_arg);
static void Task3       (void *p_arg);
static void Task4       (void *p_arg);

static OS_STK StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE];
static OS_STK Task1Stk[APP_CFG_TASK1_STK_SIZE];
static OS_STK Task2Stk[APP_CFG_TASK1_STK_SIZE];
static OS_STK Task3Stk[APP_CFG_TASK1_STK_SIZE];
static OS_STK Task4Stk[APP_CFG_TASK1_STK_SIZE];

static void App_TaskCreate (void);

int boton_start;
int boton_stop;
int boton_reset;
int boton_up;
int boton_down;

int enable;
int modo_ascendente;
int modo_descendente;

int min;
int seg;
int dec;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
#if (OS_TASK_NAME_EN > 0u)
CPU_INT08U os_err;
#endif
CPU_INT16U int_id;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  CPU_IntDis();
  for (int_id = CPU_INT_EXT0; int_id <= (EXT_INT_MAX_NBR - 1u); int_id++)
   {
    /* Set all external intr. to KA interrupt priority boundary */
	 CPU_IntSrcPrioSet(int_id, CPU_CFG_KA_IPL_BOUNDARY, CPU_INT_KA);
   }
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */
  OSInit();
  OSTaskCreateExt(StartupTask,
 		  	  	  0,
 				  &StartupTaskStk[APP_CFG_STARTUP_TASK_STK_SIZE - 1],
 				  APP_CFG_STARTUP_TASK_PRIO,
 				  APP_CFG_STARTUP_TASK_PRIO,
 				  &StartupTaskStk[0],
 				  APP_CFG_STARTUP_TASK_STK_SIZE,
 				  0,
  			      (OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR));

   #if (OS_TASK_NAME_EN > 0u)
    OSTaskNameSet(APP_CFG_STARTUP_TASK_PRIO,(INT8U *)"Startup task", &os_err);
   #endif

  OSStart();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin : LED_Pin */
  GPIO_InitStruct.Pin = LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/*
*********************************************************************************************************
*                                          HAL_InitTick()
*
* Description :
* Argument(s) : none.
* Return(s)   : os_tick_ctr.
* Caller(s)   :
* Note(s)     : none.
*********************************************************************************************************
*/
HAL_StatusTypeDef HAL_InitTick(uint32_t TickPriority)
{
/* define as empty to prevent the system tick being initialized before
    the OS starts */
return (HAL_OK);
}
/*
*********************************************************************************************************
*                                          HAL_GetTick()
*
* Description :
* Argument(s) : none.
* Return(s)   : os_tick_ctr.
* Caller(s)   :
* Note(s)     : none.
*********************************************************************************************************
*/
uint32_t HAL_GetTick(void)
{
CPU_INT32U os_tick_ctr;

#if (OS_VERSION >= 30000u)
  OS_ERR os_err;
  os_tick_ctr = OSTimeGet(&os_err);
#else
  os_tick_ctr = OSTimeGet();
#endif

return os_tick_ctr;
}
/*
*********************************************************************************************************
*                                          App_TaskStart()
*
* Description : The startup task.  The uC/OS-II ticker should only be initialize once multitasking starts.
*
* Argument(s) : p_arg       Argument passed to 'App_TaskStart()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/
static void StartupTask (void *p_arg)
{
 CPU_INT32U cpu_clk;
 (void)p_arg;
 cpu_clk = HAL_RCC_GetHCLKFreq();
 /* Initialize and enable System Tick timer */
 OS_CPU_SysTickInitFreq(cpu_clk);

 #if (OS_TASK_STAT_EN > 0)
  OSStatInit();                                               /* Determine CPU capacity.                              */
 #endif

 //App_EventCreate();										/* Create application events*/
  App_TaskCreate();											/* Create application tasks*/

  //LCD_I2C_Init(&hi2c1);
  //LCD_I2C_Clear();
  //LCD_I2C_Printf("Hola mundo :)");

 while (DEF_TRUE)
 {
  OSTimeDlyHMSM(0u, 0u, 0u, 500u);
 }

}

/*
*********************************************************************************************************
*                                          App_TaskCreate()
*
* Description :
*
* Argument(s) : p_arg       Argument passed to 'App_TaskCreate()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static void App_TaskCreate (void)
{
	CPU_INT08U os_err;

	os_err = OSTaskCreateExt((void (*)(void *)) Task1,
							(void			*) 0,
							(OS_STK			*)&Task1Stk[APP_CFG_TASK1_STK_SIZE - 1],
							(INT8U			 ) APP_CFG_TASK1_PRIO,
							(INT16U			 ) APP_CFG_TASK1_PRIO,
							(OS_STK			*)&Task1Stk[0],
							(INT32U			 ) APP_CFG_TASK1_STK_SIZE,
							(void			*) 0,
							(INT16U			 ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_TASK1_PRIO,(INT8U *) "Task 1", &os_err);

	os_err = OSTaskCreateExt((void (*)(void *)) Task2,
								(void			*) 0,
								(OS_STK			*)&Task2Stk[APP_CFG_TASK2_STK_SIZE - 1],
								(INT8U			 ) APP_CFG_TASK2_PRIO,
								(INT16U			 ) APP_CFG_TASK2_PRIO,
								(OS_STK			*)&Task2Stk[0],
								(INT32U			 ) APP_CFG_TASK2_STK_SIZE,
								(void			*) 0,
								(INT16U			 ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_TASK1_PRIO,(INT8U *) "Task 2", &os_err);

	os_err = OSTaskCreateExt((void (*)(void *)) Task3,
									(void			*) 0,
									(OS_STK			*)&Task3Stk[APP_CFG_TASK3_STK_SIZE - 1],
									(INT8U			 ) APP_CFG_TASK3_PRIO,
									(INT16U			 ) APP_CFG_TASK3_PRIO,
									(OS_STK			*)&Task3Stk[0],
									(INT32U			 ) APP_CFG_TASK3_STK_SIZE,
									(void			*) 0,
									(INT16U			 ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

	OSTaskNameSet(APP_CFG_TASK1_PRIO,(INT8U *) "Task 3", &os_err);

	os_err = OSTaskCreateExt((void (*)(void *)) Task4,
										(void			*) 0,
										(OS_STK			*)&Task4Stk[APP_CFG_TASK4_STK_SIZE - 1],
										(INT8U			 ) APP_CFG_TASK4_PRIO,
										(INT16U			 ) APP_CFG_TASK4_PRIO,
										(OS_STK			*)&Task4Stk[0],
										(INT32U			 ) APP_CFG_TASK4_STK_SIZE,
										(void			*) 0,
										(INT16U			 ) (OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));

		OSTaskNameSet(APP_CFG_TASK1_PRIO,(INT8U *) "Task 4", &os_err);
}

/*
*********************************************************************************************************
*                                          Task1()
*
* Description : Tarea Led indicador de Reloj activo
*
* Argument(s) : p_arg       Argument passed to 'Task1()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/
static void Task1 (void *p_arg)
{
	(void) p_arg;

	while (DEF_TRUE){
		if(enable)
			HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
		else
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
		OSTimeDlyHMSM(0u, 0u, 0u, 500u);
	}
}

/*
*********************************************************************************************************
*                                          Task2()
*
* Description : Verificación de teclas presionadas
*
* Argument(s) : p_arg       Argument passed to 'Task2()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/
static void Task2 (void *p_arg)
{
	(void) p_arg;

	while (DEF_TRUE){
		if(boton_start==1){
			enable = 1;
			boton_start = 0;
		}
		if(boton_stop==1){
			enable = 0;
			boton_stop = 0;
		}
		if(boton_reset==1){
			dec = 0;
			seg = 0;
			if(modo_descendente) min = 12;
			else min=0;
			boton_reset = 0;
		}
		if(boton_up==1){
			modo_ascendente = 1;
			modo_descendente = 0;
			boton_up = 0;
		}
		if(boton_down==1){
			modo_ascendente = 0;
			modo_descendente = 1;
			boton_down = 0;
		}
		OSTimeDlyHMSM(0u, 0u, 0u, 100u);
	}
}

/*
*********************************************************************************************************
*                                          Task3()
*
* Description :	Incrementar el reloj cada 100ms
*
* Argument(s) : p_arg       Argument passed to 'Task3()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/
static void Task3 (void *p_arg)
{
	(void) p_arg;

	dec=0;
	seg=0;
	min=0;
	modo_ascendente=1;
	modo_descendente=0;

	while (DEF_TRUE){

		if(enable && modo_ascendente){
			dec++;
			if(dec==10){
				dec=0;
				seg++;
				if(seg==60){
					seg=0;
					min++;
					if(min==60){
						min=0;
					}
				}
			}
		}

		if(enable && modo_descendente){
			if(dec>0) dec--;
			if(dec==0){
				dec=9;
				if(seg>0) seg--;
				if(seg==0){
					seg=59;
					if(min>0) min--;
					if(min==0){
						min=11;
					}
				}
			}
		}
		OSTimeDlyHMSM(0u, 0u, 0u, 100u);
	}
}

/*
*********************************************************************************************************
*                                          Task4()
*
* Description :	Manejo de lcd (Implementado por CubeMonitor)
*
* Argument(s) : p_arg       Argument passed to 'Task4()' by 'OSTaskCreate()'.
*
* Return(s)   : none.
*
* Caller(s)   : This is a task.
*
* Note(s)     : none.
*********************************************************************************************************
*/
static void Task4 (void *p_arg)
{
	(void) p_arg;

	while (DEF_TRUE){
		OSTimeDlyHMSM(0u, 0u, 0u, 500u);
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
